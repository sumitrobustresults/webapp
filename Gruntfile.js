module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
        	bower: ['public/vendor'],
        	nm: ['node_modules']
        },
        uglify: {
            options: {
            	mangle: false,
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                preserveComments: false
            },
            app: {
                src: 'app_modules/*.js',
                dest: 'app_modules/',
                expand: true,
                flatten: true,
                ext: '.js'
            },
            bootstrap: {
            	src: 'bootstrap/*.js',
                dest: 'bootstrap/',
                expand: true,
                flatten: true,
                ext: '.js'
            },
            routes: {
            	files: grunt.file.expandMapping(
        			['routes/**/*.js'], '', {
        				rename: function(base, path) {
        					return base + path;
        				}
        			}
    			),
            	expand: true,
                flatten: true
            },
            client: {
            	files: grunt.file.expandMapping(
        			['public/scripts/**/*.js'], '', {
        				rename: function(base, path) {
        					return base + path;
        				}
        			}
    			),
            	expand: true,
                flatten: true
            }
        },
        jsdoc: {
        	dist: {
        		src: ['app_modules/*.js', 'bootstrap/*.js', 'routes/**/*.js'],
        		options: {
        			destination: 'docs'
        		}
        	}
        }
    });

    // Load the plugins.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-jsdoc');

    // Default task(s).
    grunt.registerTask('default', ['uglify', 'clean']);
};