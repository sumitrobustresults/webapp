{
    "accounts.js": [
        {
            "method": "get",
            "path": "/"
        },
        {
            "method": "post",
            "path": "/create"
        },
        {
            "method": "post",
            "path": "/delete"
        },
        {
            "method": "post",
            "path": "/modify"
        },
        {
            "method": "post",
            "path": "/password"
        },
        {
            "method": "post",
            "path": "/name"
        },
        {
            "method": "post",
            "path": "/student/update"
        },
        {
            "method": "post",
            "path": "/student/create"
        },
        {
            "method": "post",
            "path": "/student/delete"
        }
    ],
    "control.js": [
        {
            "method": "get",
            "path": "/"
        },
        {
            "method": "post",
            "path": "/"
        }
    ],
    "data.js": [
        {
            "method": "get",
            "path": "/errors"
        },
        {
            "method": "get",
            "path": "/events"
        },
        {
            "method": "get",
            "path": "/submissions"
        },
        {
            "method": "post",
            "path": "/grade"
        },
        {
            "method": "post",
            "path": "/code/delete"
        },
        {
            "method": "get",
            "path": "/assignments"
        },
        {
            "method": "get",
            "path": "/analytics/:id"
        },
        {
            "method": "get",
            "path": "/quality"
        }
    ],
    "dataviz.js": [
        {
            "method": "get",
            "path": "/dash/:id"
        },
        {
            "method": "get",
            "path": "/progress/stats"
        },
        {
            "method": "get",
            "path": "/clusters"
        },
        {
            "method": "get",
            "path": "/clusters/instances"
        },
        {
            "method": "get",
            "path": "/clusters/code"
        },
        {
            "method": "get",
            "path": "/clusters/of"
        },
        {
            "method": "get",
            "path": "/syntactic"
        },
        {
            "method": "get",
            "path": "/syntactic/instances"
        },
        {
            "method": "post",
            "path": "/syntactic/feedback"
        },
        {
            "method": "get",
            "path": "/syntactic/code"
        },
        {
            "method": "post",
            "path": "/error/rule"
        },
        {
            "method": "post",
            "path": "/error/update"
        },
        {
            "method": "post",
            "path": "/error/code"
        }
    ],
    "default.js": [
        {
            "method": "get",
            "path": "/codeview/:id"
        },
        {
            "method": "get",
            "path": "/editor"
        },
        {
            "method": "get",
            "path": "/submissions"
        },
        {
            "method": "get",
            "path": "/viewer/:id"
        },
        {
            "method": "get",
            "path": "/pager"
        },
        {
            "method": "get",
            "path": "/jobs"
        },
        {
            "method": "get",
            "path": "/job/:id"
        }
    ],
    "editor.js": [
        {
            "method": "get",
            "path": "/"
        },
        {
            "method": "get",
            "path": "/:id"
        },
        {
            "method": "post",
            "path": "/save"
        },
        {
            "method": "post",
            "path": "/create"
        },
        {
            "method": "post",
            "path": "/delete"
        },
        {
            "method": "post",
            "path": "/file"
        },
        {
            "method": "post",
            "path": "/savefile"
        },
        {
            "method": "get",
            "path": "/downgrade"
        },
        {
            "method": "get",
            "path": "/upgrade"
        }
    ],
    "pager.js": [
        {
            "method": "get",
            "path": "/"
        },
        {
            "method": "post",
            "path": "/create"
        },
        {
            "method": "post",
            "path": "/respond"
        },
        {
            "method": "post",
            "path": "/remove"
        },
        {
            "method": "get",
            "path": "/messages"
        }
    ],
    "events.js": [
        {
            "method": "get",
            "path": "/"
        },
        {
            "method": "get",
            "path": "/assign"
        },
        {
            "method": "get",
            "path": "/all"
        },
        {
            "method": "get",
            "path": "/status"
        },
        {
            "method": "post",
            "path": "/assign"
        },
        {
            "method": "post",
            "path": "/create"
        },
        {
            "method": "post",
            "path": "/schedule"
        },
        {
            "method": "post",
            "path": "/deleteschedule"
        },
        {
            "method": "post",
            "path": "/slot"
        },
        {
            "method": "post",
            "path": "/assign/random"
        },
        {
            "method": "post",
            "path": "/assign/manual"
        }
    ],
    "index.js": [
        {
            "method": "get",
            "path": "/"
        },
        {
            "method": "get",
            "path": "/admin"
        },
        {
            "method": "get",
            "path": "/home"
        },
        {
            "method": "get",
            "path": "/practice"
        },
        {
            "method": "get",
            "path": "/practice/:id"
        },
        {
            "method": "get",
            "path": "/codebook"
        },
        {
            "method": "get",
            "path": "/codebook/page"
        },
        {
            "method": "get",
            "path": "/code/:id"
        },
        {
            "method": "post",
            "path": "/support"
        },
        {
            "method": "post",
            "path": "/activity"
        },
        {
            "method": "post",
            "path": "/bug"
        },
        {
            "method": "get",
            "path": "/ping"
        },
        {
            "method": "get",
            "path": "/settings"
        }
    ],
    "problems.js": [
        {
            "method": "get",
            "path": "/statement"
        },
        {
            "method": "get",
            "path": "/manage"
        },
        {
            "method": "get",
            "path": "/upload"
        },
        {
            "method": "get",
            "path": "/:id"
        },
        {
            "method": "get",
            "path": "/categories"
        },
        {
            "method": "get",
            "path": "/list"
        },
        {
            "method": "post",
            "path": "/update/statement"
        },
        {
            "method": "post",
            "path": "/update/template"
        },
        {
            "method": "post",
            "path": "/update/solution"
        },
        {
            "method": "post",
            "path": "/update/title"
        },
        {
            "method": "post",
            "path": "/addspec"
        },
        {
            "method": "post",
            "path": "/removespec"
        },
        {
            "method": "post",
            "path": "/togglespec"
        },
        {
            "method": "post",
            "path": "/spec/update/code"
        },
        {
            "method": "post",
            "path": "/spec/update/inputs"
        },
        {
            "method": "post",
            "path": "/addtest"
        },
        {
            "method": "post",
            "path": "/addtestauto"
        },
        {
            "method": "post",
            "path": "/removetest"
        },
        {
            "method": "post",
            "path": "/"
        },
        {
            "method": "post",
            "path": "/practice"
        },
        {
            "method": "post",
            "path": "/remove"
        }
    ],
    "tasks.js": [
        {
            "method": "get",
            "path": "/"
        },
        {
            "method": "get",
            "path": "/status"
        },
        {
            "method": "post",
            "path": "/task/grading"
        }
    ],
    "users.js": [
        {
            "method": "post",
            "path": "/login"
        },
        {
            "method": "get",
            "path": "/logout"
        }
    ]
}