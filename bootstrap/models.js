module.exports = function(nosql) {
	
	// Telemetry model.
	nosql.model('Telemetry', new nosql.Schema({
		userId: String,
		context: String,
		details: String,
		reference: String,
		activityTime: { type: Date, default: Date.now }
	}));
	
	// Notifications model.
	nosql.model('Notifications', new nosql.Schema({
		context: String,
		source: String,
		target: String,
		message: String,
		seen: { type: Boolean, default: false},
		createTime: { type: Date, default: Date.now }
	}));
	
	// Environments model.
	nosql.model('Environments', new nosql.Schema({
		name: String,
		editor_mode: String,
		compile: Boolean,
		output_format: String,
		source_ext: String,
		binary_ext: String,
		cmd_compile: String,
		cmd_execute: String,
		display: String,
		link_template: String
	}));
};