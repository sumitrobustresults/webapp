/**
 * Execute SQL queries on MySQL database and fetch results.
 * @module 		bootstrap/database
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:mysql
 */

var mysql = require('mysql');

var database = {};

var pool = mysql.createPool({
	connectionLimit : 70,
	host : process.env.DB_HOST,
	port: process.env.DB_PORT,
	user : process.env.DB_USER,
	password : process.env.DB_PASS,
	database : process.env.DB_NAME
});

console.log('CREATED DATABASE POOL.');

function query(query, params, callback) {
	pool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			con.query(query, params, callback);
			con.release();
		}
	});
}

database.pool = pool;
database.query = query;

module.exports = database;
