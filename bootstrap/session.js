/**
 * Manages the session store for the application.
 * @module 		bootstrap/session
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express-session
 * @requires module:connect-redis
 * @requires module:connect-memcached
 */

var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var MemcachedStore = require('connect-memcached')(session);

var secret = 'fsajfb3f934hg93hg';

var cacheType = process.env.CACHE_TYPE;
var cachePort = process.env.CACHE_PORT;
var cacheAddr = process.env.CACHE_ADDR;

try {
	cacheAddr = eval('(' + cacheAddr + ')');
} catch(err) {}

// TODO clustering support.
if(cacheType === 'REDIS') {
	console.log('REDIS STORE DETECTED.');
	var store = {
		secret: secret,
		store: new RedisStore({ 
			host: cacheAddr, 
			port: cachePort 
		}),
		saveUninitialized: true, 
		resave: true 
	}; 
} else {
	console.log('MEMCACHED STORE DETECTED.');
	var servers = [];
	if(typeof cacheAddr === 'string') servers.push(cacheAddr + ':' + cachePort);
	else {
		for(var i = 0; i < cacheAddr.length; i++)
			servers.push(cacheAddr[i] + ':' + cachePort);
	}
	var store = {
	    secret  : secret,
	    key     : 'its',
	    proxy   : 'true',
	    store   : new MemcachedStore({
	    	hosts: servers,
	    	prefix: 'sess:'
	    }),
		saveUninitialized: true,
		resave: true,
		unset: 'destroy'
	};
}

var sessionStore = session(store);

console.log('INITIALIZED SESSION STORE.');

module.exports = sessionStore;