/**
 * Caches data from the database.
 * @module 		bootstrap/cache
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:memcached
 * @requires module:redis
 */

var Memcached = require('memcached');
var Redis = require('redis');

var db = null;

/**
 * Caches data after fetching from the database or returns it from the cache if already cached.
 * 
 * @param {string} query The database query in SQL.
 * @param {object} params Set of parameters to be used in the query.
 * @param {cacheCallback} callback The callback function that is executed after caching/retrieval completes.
 * @param {int} ttl The time-to-live value for the cache.
 */
function cache(query, params, callback, ttl) {
    
	var cacheType = process.env.CACHE_TYPE;
	var cachePort = process.env.CACHE_PORT;
	var cacheAddr = process.env.CACHE_ADDR;
	
	try {
		cacheAddr = eval('(' + cacheAddr + ')');
	} catch(err) {}
	
	var hash = require('MD5')( query + JSON.stringify(params) );
	if(typeof ttl === 'undefined') ttl = 1800;
	
	// TODO clustering support.
	if(cacheType === 'REDIS') {
		var redis = Redis.createClient(cachePort, cacheAddr, {});
		redis.get(hash, function(err, value) {
			if(value) {
				value = eval('(' + value + ')');
				callback(null, value);
			} else {
				db.getConnection(function(err, con) {
					if(err) {
						console.log(err);
						callback(err, null);
					} else {
						con.query(query, params, function(err, rows) {
							if(err) {
								callback(err, null);
							} else {
								redis.setex(hash, ttl, JSON.stringify(rows));
								callback(null, rows);
							}
						});
						con.release();
					}
				});
			}
		});
	} else {
		var servers = [];
		if(typeof cacheAddr === 'string') servers.push(cacheAddr + ':' + cachePort);
		else {
			for(var i = 0; i < cacheAddr.length; i++)
				servers.push(cacheAddr[i] + ':' + cachePort);
		}
		var memcached = new Memcached(servers, {});
    	memcached.get(hash, function(err, value) {
    		if(value) {
				callback(null, value);
			} else {
				db.getConnection(function(err, con) {
					if(err) {
						console.log(err);
						callback(err, null);
					} else {
						con.query(query, params, function(err, rows) {
							if(err) {
								callback(err, null);
							} else {
								memcached.set(hash, JSON.stringify(rows), ttl);
								callback(null, rows);
							}
						});
						con.release();
					}
				});
			}
    	});
	}
};

module.exports = function(pool) {
	
	db = pool;
	
	return cache;
};