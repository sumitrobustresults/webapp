/**
 * Controls access to resources.
 * @module 		bootstrap/acl
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */

var acl = null;

var rules = [
{
   roles: ['instructor', 'tutor', 'ta', 'student'],
   allows: [
    {
 	   resources: ['/', '/tests/db', '/home', '/practice', '/bug', '/ping', '/accounts/login' , '/accounts/logout', '/editor/rules', '/editor', '/editor/:id', '/editor/practice/:id', '/editor/save', '/editor/create', '/editor/delete', '/editor/file', '/editor/savefile', '/pager', '/pager/create', '/pager/respond', '/pager/remove', '/pager/messages', '/codebook', '/codebook/page', '/codebook/regrade', '/problems/statement', '/notifications/read', '/editor/download/:id', '/editor/download/scratch/:id', '/editor/upload'],
 	   permissions: ['GET', 'POST']
    }
    ]
},
{
   roles: ['instructor', 'tutor', 'ta'],
   allows: [
    {
 	   resources: ['/tasks', '/tasks/status', '/code/:id', '/settings', '/problems/tests/add/manual', '/problems/tests/remove', '/problems/manage', '/problems/:id', '/problems/categories', '/problems/list', '/admin', '/admin/editor', '/admin/submissions', '/admin/viewer/:id', '/admin/pager', '/dataviz/submissions', '/dataviz/grade', '/dataviz/code/delete', '/dataviz/assignments', '/dataviz/analytics/:id', '/dataviz/events', '/accounts/password', '/accounts/name', '/notifications'],
 	   permissions: ['GET', 'POST']
    }
    ]
},
{
   roles: ['instructor', 'tutor'],
   allows: [
    {
 	   resources: ['/problems/tests/add/auto', '/problems/upload', '/problems', '/problems/practice', '/problems/remove', '/problems/update/environment', '/problems/update/statement', '/problems/update/template', '/problems/update/solution', '/problems/update/title'],
 	   permissions: ['GET', 'POST']
    }
    ]
},
{
   roles: ['instructor'],
   allows: [
    {
 	   resources: ['/events', '/events/assign', '/events/all', '/events/status', '/events/assign', '/events/create', '/events/schedule', '/events/deleteschedule', '/events/slot', '/accounts', '/accounts/initresetpwd', '/accounts/student', '/accounts/create', '/accounts/delete', '/accounts/modify', '/accounts/student/update', '/accounts/student/create', '/accounts/student/delete', '/admin/codeview/:id', '/control', '/dataviz/error/rule', '/dataviz/error/update', '/dataviz/error/code', '/dataviz/error/description', '/dataviz/syntactic', '/dataviz/syntactic/instances', '/dataviz/syntactic/feedback', '/dataviz/syntactic/code', '/dataviz/clusters', '/dataviz/clusters/instances', '/dataviz/clusters/code', '/dataviz/clusters/of', '/dataviz/clusters/star', '/dataviz/clusters/unstar', '/dataviz/clusters/seen', '/dataviz/errors', '/dataviz/dash/:id', '/dataviz/progress/stats', '/dataviz/compiler_messages'],
 	   permissions: ['GET', 'POST']
    }
    ]
}
];

var allowedServices = ['setnewpassword'];

var configure = function() {
	acl.allow(rules);
};


var controller = function(req, res, next) {
	// Filter requests.
	if(req.session.user) {
		// Authenticate user.
		var userID = req.session.user.id;
		var method = req.method;
		var resource = req._parsedUrl.pathname.replace(/[0-9]+/g, ':id');
		acl.isAllowed(userID, resource, method, function(err, allowed) {
	        if(err) {
	            console.log(err);
	            res.status(500);
	        } else {
	            if(allowed)
	                next();
	            else {
	                res.status(403);
	                res.render('errors/403', {title: 'Intelligent Tutoring System'});
	            }
	        }
	    });
	} else {
		var parts = req._parsedUrl.pathname.split("/");
		// Public user.
		if(req.url === '/' || req.url === '/accounts/login' || req.url === '/accounts/setnewpassword' || allowedServices.indexOf(parts[1]) >= 0)
			next();
		else
			res.redirect('/');
	}
};

module.exports = function(obj) {
	
	acl = obj;
	
	return  {
		configure: configure,
		controller: controller
	};
};