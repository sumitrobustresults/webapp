/**
 * Copyright (c) 2015 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	load_css('/styles/theme');
	
	$(document).ready(function() {
		$('#btn-pass').click(setPassword);
	});
});

function setPassword() {
	var pass1 = $('#pass1').val().trim();
	var pass2 = $('#pass2').val().trim();
	var path = window.location.pathname.split("/");
	var hash = path[path.length - 1];
	if(pass1 === '') {
		toastr.warning('Password cannot be empty!');
		return;
	} else if(pass1 !== pass2) {
		toastr.warning('Entered passwords do not match!');
		return;
	}
	$.post('/accounts/setnewpassword', {hash: hash, password: pass1}, function(response) {
		if(response === true) {
			toastr.success('Password has been set! Login to the ITS.');
			window.location.href = '/';
		} else
			toastr.error('An error occurred! Password could not be set.');
	});
}