/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	load_css('/styles/theme');
	load_css('/styles/home');
	
	$(document).ready(function() {
		stylize();
		bindhandlers();
	});
});

function stylize() {}

function bindhandlers() {
	$('#viewgrades').click(function(e) {
		e.preventDefault();
		var icon = $(this).find('.glyphicon');
		if(icon.hasClass('glyphicon-collapse-down'))
			icon.removeClass('glyphicon-collapse-down').addClass('glyphicon-collapse-up');
		else
			icon.removeClass('glyphicon-collapse-up').addClass('glyphicon-collapse-down');
		$('#gradecard').toggle(500);
	});
	
	$('#contactus .send').click(function() {
		$('#contactus').modal('hide');
		var msg = $('#contactus textarea').val().trim(); 
		$.post('/pager/create', {context: 'HOME', reference: 0, visibility: 0, message: msg}, function(response) {
			try {
				var result = eval('(' + response + ')');
				$('#contactus textarea').val('');
				toastr.info('Your message has been sent. We will try to resolve your issue as soon as possible.');
			} catch(err) {
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/pager/create',
					req_params: JSON.stringify({code_id: codeID, message: msg}),
					response: res
				}, function(response) {});
				toastr.error('An error occurred while trying to send your message. Please ask for assistance.');
			}
		});
	});
	
	$('#help a').click(function(e) {
		e.preventDefault();
		$('#contactus').modal('show');
	});
}