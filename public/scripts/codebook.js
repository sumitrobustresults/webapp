/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var editor = null;
var Range = null;

define(['bootstrap', 'ace', 'mathjax', 'base64', 'jstree'], function() {
	
	load_css('jstree');
	load_css('/styles/editor');

	$(document).ready(function() {
		stylize();
		initEditor();
		renderTrees();
		window.onresize = function() {
			stylize();
		};
	});
	
	MathJax.Hub.Config({
		tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] }
	});
});

function stylize() {
	$('#main').css({
		'min-height': window.innerHeight - 50,
		'background-color': '#FFFFFF'
	});
	$('#contents').css({
		'min-height': window.innerHeight - 180
	});
}

function renderTrees() {
	
	$('.tree-practice').jstree({
		core: {
			themes: {
				stripes: false
			}
		},
		plugins: ['wholerow', 'sort']
	}).on('select_node.jstree', function(e, sel) {
		var data = sel.node.data.jstree.data;
		if(typeof data === 'undefined') return;
		$.getJSON('/codebook/page', {id: data.id}, function(page) {
			// console.log(page);
			var title = page.title ? page.title : 'Untitled';
			$('#title').text(page.title);
			$('#category').text(page.category);
			$('#statement').html(page.statement);
			$('#link a').attr('href', '/editor/practice/' + page.problem_id);
			editor.setValue(Base64.decode(page.code));
			setEditorMode(page.env.editor_mode);
			$('#grade').hide();
			$('#link').show();
			$('#page').show();
			MathJax.Hub.Queue(["Typeset",MathJax.Hub,"statement"]);
		});
	});
	
	$('.tree-events').jstree({
		core: {
			themes: {
				stripes: false
			}
		},
		plugins: ['wholerow', 'sort']
	}).on('select_node.jstree', function(e, sel) {
		var data = sel.node.data.jstree.data;
		if(typeof data === 'undefined') return;
		loadPage(data);
	});
}

function loadPage(data) {
	$.getJSON('/codebook/page', {id: data.id}, function(page) {
		// console.log(page);
		var title = page.title ? page.title : 'Untitled';
		var status = (page.score !== null) ? 'This assignment has been awarded <b>' + page.score + '</b> marks out of <b>' + page.max_marks + '</b>.' : 'This assignment has not been graded yet.';
		$('#aid').text('#' + data.id);
		$('#title').text(page.title ? page.title : 'Untitled');
		$('#category').text(page.category);
		$('#statement').html(page.statement);
		$('#grad-status').html(status);
		$('#comments').text('"' + page.comments + '"');
		if(page.score == null){
			$('#comments').hide();
			$('#regrade').hide();
		} else {
			$('#comments').show();
			if(page.regrade_requested == 0) {
				$('#regrade-req').off().click(function() {
					var reason = $('#regrade-reason').val();
					$.post('/codebook/regrade', {id: data.id, reason: reason}, function(success) {
						if(success) {
							toastr.success('Regrade request submitted!');
							loadPage(data);
						} else {
							toastr.error('Unable to submit regrade request!');
						}
					});
				});
				$('#regrade').show();
				$('#regrade-status').hide();
				if(page.regrade_reqtime) {
					$('#regrade-status')
						.text('Assignment has be re-graded.')
						.show();
				}
			} else {
				$('#regrade').hide();
				$('#regrade-status').text('You have requested regrading of this assignment.');
			}
		}
		editor.setValue(Base64.decode(page.code));
		setEditorMode(page.env.editor_mode);
		$('#page').show();
		$('#link').hide();
		$('#grade').show();
		MathJax.Hub.Queue(["Typeset",MathJax.Hub,"statement"]);
	});
}

function setEditorMode(mode) {
	console.log('Setting mode: ' + mode);
	editor.getSession().setMode("ace/mode/" + mode);
}

function initEditor() {
	Range = ace.require("ace/range").Range;
	editor = ace.edit("editor");
    editor.setTheme("ace/theme/eclipse");
    editor.setFontSize(16);
    editor.setReadOnly(true);
    editor.getSession().setUseWrapMode(true);
}