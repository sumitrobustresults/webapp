/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var editor = null;
var Range = null;
var executable = null;

var THRESHOLD_CHAR = 10;
var THRESHOLD_TIME = 60000;

var autosaveEnabled = false;
var lastSaved = 0;

var localCode = null;

var saveLock = false;

var code = null;

//var assignmentID = null;
var codeID = null;
var unseen = 0;

var editorHeight = 0;
var reduction = 0;

var perspective = null;
var engineLock = false;

define(['moment', 'bootstrap', 'ace', 'base64', 'jstorage', 'scrollbar'], function() {
	
	// Initialize perspective
	var location = window.location.pathname;
	var path = location.split("/");
	
	load_css('scrollbar');
	load_css('/styles/loader');
	
	$(document).ready(function() {
		
		if(typeof env !== 'undefined') {
			env = eval('(' + env + ')');
		}
		
		adapt();
		
		load_css('/styles/editor');
		
		stylize();
		bindHandlers();
		
		initEditor();
		hotKeys();
		//initCodeStore();
		
		if(isNaN(path[path.length - 1])) {
			// Scratchpad
			perspective = 'scratch';
			scratchPerspective();
		} else {
			// Event
			//assignmentID = path[path.length - 1];
			perspective = 'event';
			eventPerspective();
		}
		
		require(['/scripts/editor/debug.js'], function() {});
		
		if(!isMobile)
			window.onresize = function() {
				onResize();
			};
		
		// Check connectivity.
		$.get('/ping', {}, function(res) {
			console.log(res);
		});
});});

function adapt() {
	//isMobile = !isMobile;
	if(!isMobile)
		return;
	$('.panels').removeClass('panels');
	$('#spare').html($('#tutor-area').html());
	$('#tutor-area').remove();
	$('#panel-tabs li').removeClass('active');
	$('#panel-tabs').prepend(
		$('<li>').addClass('active').append($('<a>').text('Editor').attr({
			'href': '#editor-view',
			'data-toggle': 'tab',
			'id': 'link-editor'
		}))
	);
	$('#panel-tabs').addClass('nav-justified');
	$('.tab-pane').removeClass('active');
	$('#panel-tabs-content').prepend(
		$('<div>')
			.css({'padding': '0px'})
			.attr('id', 'editor-view')
			.addClass('tab-pane active')
			.append($('#pad-area').html())
	);
	$('#pad-area').remove();
}

// Initialize the editor.
function initEditor() {
	var mode = env.editor_mode;
	Range = ace.require("ace/range").Range;
	editor = ace.edit("editor");
	editor.setTheme("ace/theme/eclipse");
	editor.setFontSize(16);
	editor.getSession().setMode("ace/mode/" + mode);
	editor.getSession().setUseWrapMode(true);
	editor.getSession().on('change', function(event) {
			executable = null;
			$('#btn-execute').attr('disabled', 'disabled');
			});
}

// Stylize the UI components of the IDE.
function stylize() {
	
	var viewArea = window.innerHeight - 54;
	
	editorHeight = parseInt(viewArea * 0.70);
	
	$('#editor').css('height', editorHeight);
	$('#console').css('height', (viewArea - editorHeight));
	$('.panels, .panels .tab-content').css('height', viewArea);
	$('#console .tab-content, #tab-input textarea, #tab-output textarea').css('height', (viewArea - 32 - editorHeight));
	
	var html = $('<div>')
		.css({
			'padding': '5px'
		})
		.append($('<h4>').text('Experiencing Problems?'))
		.append($('<div>').html('Send us a message regarding the problems that you are facing.' 
				+ ' Click here to send a message.'));
	$('#help').show();
	$('#help').popover({
		content: html,
		html: true,
		placement: 'auto',
		trigger: 'hover',
		viewport: 'body'
	});
	
	$('.panels .tab-content').perfectScrollbar({
		//suppressScrollX: true
	});
	
	if(isMobile) {
		$('#pad-area').css({
			'border-top': 'solid 1px #999999',
			'border-left': 'none'
		});
		$('#tutor-area').css({
			'border-bottom': 'solid 1px #999999'
		});
	}
}

// UI to be rendered on window resize.
function onResize() {
	
	console.log('Resizing UI...');
	
	var viewArea = window.innerHeight - 54 - reduction;
	
	editorHeight = parseInt(viewArea * 0.70);
	
	$('#editor').css('height', editorHeight);
	$('#console').css('height', (viewArea - editorHeight));
	$('#console .tab-content, #tab-input textarea, #tab-output textarea').css('height', (viewArea - 32 - editorHeight));
	
	$('.panels, .panels .tab-content').css('height', (window.innerHeight - 54) + 'px');
	
	editor.resize();
}

// Bind hotkeys to commands.
function hotKeys() {
	require(['keypress'], function(keypress) {
		var listener = new keypress.Listener();
		if(perspective === 'scratch') {
			// New File
			listener.simple_combo("ctrl n", function() {
				$('#file-newfile').click();
				return false;
			});
			//New Folder
			listener.simple_combo("ctrl d", function() {
				$('#file-newdir').click();
				return false;
			});
		} else if(perspective === 'event') {
			// Evaluate
			listener.simple_combo("ctrl shift e", function() {
				$('#run-evaluate').click();
				return false;
			});
		}
		// Save
		listener.simple_combo("ctrl s", function() {
			$('#file-save').click();
			return false;
		});
		// Check code
		listener.simple_combo("ctrl shift z", function() {
			$('#run-check').click();
			return false;
		});
		// Compile
		listener.simple_combo("ctrl shift c", function() {
			$('#run-compile').click();
			return false;
		});
		// Execute
		listener.simple_combo("ctrl shift x", function() {
			$('#run-execute').click();
			return false;
		});
		// Support
		listener.simple_combo("ctrl shift s", function() {
			$('#help-support').click();
			return false;
		});
	});
}

// Bind event handlers to various controls.
function bindHandlers() {
	
	$('#contactus .send').click(function() {
		$('#contactus').modal('hide');
		var msg = $('#contactus textarea').val().trim(); 
		$.post('/pager/create', {context: perspective.toUpperCase(), reference: codeID, visibility: 0, message: msg}, function(response) {
			try {
				var result = eval('(' + response + ')');
				$('#contactus textarea').val('');
				toastr.info('Your message has been sent. We will try to resolve your issue as soon as possible.');
			} catch(err) {
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/pager/create',
					req_params: JSON.stringify({code_id: codeID, message: msg}),
					response: res
				}, function(response) {});
				toastr.error('An error occurred while trying to send your message. Please ask for assistance.');
			}
		});
	});
	
	$('#help a').click(function(e) {
		e.preventDefault();
		$('#contactus').modal('show');
	});
	
	$('#accessibility a').click(function(e) {
		e.preventDefault();
		$('#shortcuts').modal('show');
	});
	
	/*$('#link-notify').click(function() {
		$('#notify').popover('destroy');
		$('#notify .badge').css('background-color', '#777777')
		unseen = 0;
		$('#notify .badge').text(unseen);
		$.post('/activity', {
			context: 'FEEDBACK',
			type: 'CLICK',
			details: codeID
		}, function(response) {
			console.log(response);
		});
	});*/
}

// Builds a loader in DOM and return it.
function getLoader(text) {

	var child = $('<div>').addClass('gmb-loader');
	child.append($('<div>'));
	child.append($('<div>'));
	child.append($('<div>'));
	
	var loader = $('<h2>').attr('align', 'center').addClass('loader');
	loader.append(child);
	loader.append($('<br>'));
	loader.append($('<span>').text(text));
	
	return loader;
}

// Initialize the IDE from event perspective.
function eventPerspective() {
	var event = '/scripts/editor/event.js';
	require([event], function() {
		
		if(initCode) {
			$('#sid').attr('title', 'ID: ' + initCode.id);
			$('#save-status').html($('<span>').addClass('glyphicon glyphicon-floppy-saved'));
			$('#save-status').append(' Last saved at ' + new Date(initCode.save_time).toLocaleTimeString().toUpperCase() + ' ');
			
			codeID = initCode.id;
			code = Base64.decode(initCode.contents);
			localCode = code;
			editor.setValue(code);
		}
		
		if(typeof expiry !== 'undefined') {
			updateTimer();
		}
		
		autosaveEnabled = true;
		
		MathJax.Hub.Config({
			tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] }
		});
	});
}

// Initialize the IDE from scratchpad perspective.
function scratchPerspective() {
	var scratch = '/scripts/editor/scratch.js';
	require([scratch], function() {});
}

/*function initCodeStore() {

if($.jStorage.get('codestore',null) == null || $.jStorage.get('codestore').total == 0) 
	storeLocally(Base64.decode(code));

if($.jStorage.get('codestore', null)) {
	console.log($.jStorage.get('codestore'));
	var totalVersions = $.jStorage.get('codestore').total;
	for(var i = 1; i <= totalVersions; i++) {
		var version = $.jStorage.get('codestore')['v' + i];
		var timeSnap = new Date(version.timestamp).toLocaleTimeString();
		$('#revert').prepend($('<li>').append(
			$('<a>')
				.attr('href', '#')
				.addClass('snapshot')
				.attr('value', 'v' + i)
				.append($('<span>').addClass('glyphicon glyphicon-time'))
				.append($('<span>').text(' ' + timeSnap))
		));
	}
}

$('.snapshot').click(function() {
	var version = $(this).attr('value');
	autosaveEnabled = false;
	require(['base64'], function() {
		var currentCode = editor.getValue().trim();
		var store = $.jStorage.get('codestore', null);
		if(!store) return;
		editor.setValue(Base64.decode(store[version].code));
		alertify.confirm('Are you sure you want to revert to this version? You will lose the current content of the editor if you do so.', function(evt) {
			if(evt) {
				localCode = editor.getValue().trim();
				save('revert');
				version = parseInt(version.substring(1));
				console.log(version);
				for(i = version + 1; i <= store.total; i++) {
					delete store['v' + i];
					$('a[value=v' + i +']').remove();
				}
				store.total = version;
				$.jStorage.set('codestore', store);
			} else {
				editor.setValue(currentCode);
			}
			autosaveEnabled = true;
		});
	});
});
}

function storeLocally(code) {
require(['base64'], function() {
	base64Code = Base64.encode(code);
	var codeStore = $.jStorage.get('codestore', null);
	if(!codeStore) codeStore = {total:0};
	codeStore['v' + (codeStore.total + 1)] = {code: base64Code, timestamp: new Date()};
	codeStore.total = codeStore.total + 1;
	$.jStorage.set('codestore', codeStore);
	console.log($.jStorage.get('codestore'));
	localCode = code;
	var timeSnap = codeStore['v' + codeStore.total].timestamp.toLocaleTimeString();
	$('#revert').prepend($('<li>').append(
		$('<a>')
			.attr('href', '#')
			.addClass('snapshot')
			.attr('value', 'v' + codeStore.total)
			.append($('<span>').addClass('glyphicon glyphicon-time'))
			.append($('<span>').text(' ' + timeSnap))
	));
});
}*/ 
