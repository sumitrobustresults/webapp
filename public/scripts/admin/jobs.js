/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	load_css('/styles/loader');
	
	$(document).ready(function() {
		require(['../scripts/admin/styles/default'], function() {
			$('#jobs').addClass('active');
			getEventStatus();
		});
		/*$('.consistency').click(function(e) {
			e.stopPropagation();
			e.preventDefault();
			$(this).html('Checking...').removeAttr('href').off();
			var eventID = $(this).attr('event-id');
			$.post('/job', {type: 'check_consistency', event_id: eventID}, function(response) {
				try {
					response = eval('(' + response + ')');
					var pending = response.pending.length;
					var complete = response.complete.length;
					$('[event-id=' + response.event_id + ']').hide();
					$('[event-id=' + response.event_id + ']').parent().append(
						$('<b>').text('(' + complete + '/' + (pending + complete) + ' evaluation results memoized)')
					);
				} catch(err) {
					toastr.error('Something went wrong! Could not check for consistency.');
				}
			});
		});*/
	});
});

function getEventStatus() {
	
	$('.event .panel-body').each(function() {
		var eventID = $(this).attr('data-event');
		$.post('/job', {type: 'event_status', event_id: eventID}, function(response) {
			try {
				var eventStatus = eval('(' + response + ')');
				var pending = eventStatus.pending;
				var complete = eventStatus.complete;
				var total = eventStatus.total;
				var percentComplete = parseInt(complete * 100 / (pending + complete));
				var status = '';
				if(!eventStatus.initiated) status = 'not started';
				if(eventStatus.initiated && eventStatus.running) status = 'running';
				if(eventStatus.initiated && !eventStatus.running) status = 'paused';
				if(!eventStatus.initiated && complete > 0) status = 'stopped';
				if(pending == 0) status = 'complete';
				if(!eventStatus.running) {
					$('[data-event=' + eventStatus.event_id + '] .tstat').text('Not Running');
					$('[data-event=' + eventStatus.event_id + '] .etr').text('');
					if(pending != 0) $('[data-event=' + eventStatus.event_id + '] .btn-run').removeAttr('disabled');
					else $('[data-event=' + eventStatus.event_id + '] .btn-run').attr('disabled', 'disabled');
					bindRunHandler(eventStatus.event_id);
				} else {
					$('[data-event=' + eventStatus.event_id + '] .btn-run').attr('disabled', 'disabled');
					$('[data-event=' + eventStatus.event_id + '] .stat-icon').addClass('icon ion-loading-b');
					setTimeout('refreshEventStatus(' + eventStatus.event_id + ')', 10000);
				}
				if(status === 'paused') {
					$('[data-event=' + eventStatus.event_id + '] .btn-run span').last().text(' Resume Job');
				} else if(status === 'not started') {
					$('[data-event=' + eventStatus.event_id + '] .progress').hide();
				}
				$('[data-event=' + eventStatus.event_id + '] .status').text(status);
				$('[data-event=' + eventStatus.event_id + '] .percent').text(percentComplete + '%');
				$('[data-event=' + eventStatus.event_id + '] .progress-bar').css({'width': percentComplete + '%'});
				$('[data-event=' + eventStatus.event_id + '] .submissions').text(total + ' submissions');
			} catch(err) {
				console.log(response);
				toastr.error('Something went wrong! Unable to get status for event');
			}
		});
	});
}

function bindRunHandler(eventID) {
	$('[data-event=' + eventID + '] .btn-run').click(function() {
		$('#progress').modal('show');
		$.post('/job', {type: 'event_status', event_id: eventID}, function(response) {
			console.log(response);
			try {
				var eventStatus = eval('(' + response + ')');
				if(eventStatus.initiated && !eventStatus.running) {
					$.post('/job', {type: 'resume_memoizing', event_id: eventID}, function(resp) {
						console.log(resp);
						try {
							resp = eval('(' + resp + ')');
							if(resp) {
								toastr.success('Job has been resumed.');
								window.location.reload();
							} else toastr.error('Something went wrong! Unable to resume job.');
						} catch(err) {
							toastr.error('Something went wrong! Unable to resume job.');
						} finally {
							$('#progress').modal('hide');
						}
					});
				} else {
					$.post('/job', {type: 'start_memoizing', event_id: eventID, admin_id: adminID}, function(resp) {
						console.log(resp);
						try {
							resp = eval('(' + resp + ')');
							if(resp) {
								toastr.success('Job has been started.');
								window.location.reload();
							} else toastr.error('Something went wrong! Unable to start job.');
						} catch(err) {
							toastr.error('Something went wrong! Unable to start job.');
						} finally {
							$('#progress').modal('hide');
						}
					});
				}
			} catch(ex) {
				console.log(response);
				toastr.error('Something went wrong! Unable to get status of current job.');
			}
		});
	});
}

function refreshEventStatus(eventID) {
	$.post('/job', {type: 'event_status', event_id: eventID}, function(response) {
		//console.log(response);
		try {
			response = eval('(' + response + ')');
			var job = response.job;
			var rate = response.rate;
			var pending = response.pending;
			var complete = response.complete;
			var percentComplete = parseInt(complete * 100 / (pending + complete));
			var etr = parseInt(pending / rate);
			var status = '';
			if(!response.initiated) status = 'not started';
			if(response.initiated && response.running) status = 'running';
			if(response.initiated && !response.running) status = 'paused';
			if(!response.initiated && complete > 0) status = 'stopped';
			if(pending == 0) status = 'complete';
			
			if(!response.running) {
				$('[data-event=' + response.event_id + '] .etr').text('');
				$('[data-event=' + response.event_id + '] .stat-icon').removeClass('icon ion-loading-b');
				$('[data-event=' + response.event_id + '] .tstat').text('Not Running');
				$('[data-event=' + response.event_id + '] .btn-run').click(function() {
					$.post('/job', {type: 'start_memoizing', event_id: response.event_id, admin_id: adminID}, function(resp) {
						try {
							resp = eval('(' + resp + ')');
							if(resp) toastr.success('Job has been started.');
							else toastr.error('Something went wrong! Unable to start job.');
						} catch(err) {
							toastr.error('Something went wrong! Unable to start job.');
						}
					});
				});
			} else {
				$('[data-event=' + response.event_id + '] .tstat').text('Estimated Time Remaining: ');
				$('[data-event=' + response.event_id + '] .stat-icon').addClass('icon ion-loading-b');
				require(['moment'], function(moment) {
					var duration = moment.duration(etr, 'seconds');
					if(status !== 'paused') $('[data-event=' + response.event_id + '] .etr').text(duration.humanize());
				});
			}
			$('[data-event=' + response.event_id + '] .status').text(status);
			$('[data-event=' + response.event_id + '] .percent').text(percentComplete + '%');
			$('[data-event=' + response.event_id + '] .progress-bar').css({'width': percentComplete + '%'});
			$('[data-event=' + response.event_id + '] .submissions').text((pending + complete) + ' submissions');
		} catch(err) {
			console.log(err);
			toastr.error('Unable to get memoization status for event.');
		}
	});
	setTimeout('refreshEventStatus(' + eventID + ')', 10000);
}