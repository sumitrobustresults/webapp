/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var Range = null;
var editor = null;
var executable = null;

var assignmentID = null;

define(['bootstrap', 'ace', 'base64', 'jstorage'], function() {
	
	load_css('/styles/admin/editor');
	load_css('/styles/loader');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
		initEditor();
		loadCode();
		window.onresize = function() {
			stylize();
		};
	});
});

function loadCode() {
	
	var code = '';
	var pid = null;
	
	if(window.location.hash) {
		var hash = window.location.hash.substring(1);
		var data = $.jStorage.get(hash, null);
		if(!data || data.code === null) {
			window.location.href = '/admin/editor';
			return;
		} else {
			code = data.code;
			var type = hash.substring(0,1);
			if(type === 'S') {
				pid = hash.substring(1);
			} else if(type === 'A') {
				assignmentID = hash.substring(1);
				$('#btn-eval').removeClass('disabled');
			}
			configureEditor(data.env);
			window.env = data.env;
		}
	} else {
		//code = $.jStorage.get('code', '');
	}
	
	editor.setValue(code);
	
	if($.jStorage.get('editable')) {
		$('#console').append(
			$('<div>').addClass('btn-group').append(
				$('<button>').addClass('btn btn-default').attr('id', 'btn-update')
					.append($('<span>').addClass('glyphicon glyphicon-refresh'))
					.append($('<span>').text(' Update Code'))
			)
		);
		$('#btn-update').click(function() {
			if(pid === null) return;
			$.post('/problems/update/solution', {pid: pid, solution: Base64.encode(editor.getValue()), host: window.location.hostname}, function(response) {
				console.log(response);
				try {
					response = eval('(' + response + ')');
					if(response) {
						// close window and notify manager
						$.jStorage.set('S' + pid, editor.getValue());
						$.jStorage.set('update', true);
						window.close();
					} else {
						$('#error').modal('show');
					}
				} catch(err) {
					$('#error').modal('show');
				}
			});
		});
	}
}

function getLoader(text) {

	var child = $('<div>').addClass('gmb-loader');
	child.append($('<div>'));
	child.append($('<div>'));
	child.append($('<div>'));
	
	var loader = $('<h2>').attr('align', 'center').addClass('loader');
	loader.append(child);
	loader.append($('<br>'));
	loader.append($('<span>').text(text));
	
	return loader;
} 

function configureEditor(env) {
	editor.getSession().setMode("ace/mode/" + env.editor_mode);
	if(env.compile)
		$('#btn-compile').show();
	else
		$('#btn-compile').hide();
}

function initEditor() {
	Range = ace.require("ace/range").Range;
	editor = ace.edit("editor");
    editor.setTheme("ace/theme/eclipse");
    editor.setFontSize(16);
    editor.getSession().setUseWrapMode(true);
    editor.on('change', function() {
    	executable = null;
		$('#btn-execute').attr('disabled', 'disabled');
		//$.jStorage.set('code', editor.getValue());
    });
}

function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		$('#workspace').css({
			'height': window.innerHeight - 58,
			'overflow-y': 'scroll'
		});
		$('textarea').css({'padding': '3px'});
	});
	$('.main').css({'padding': '0px'});
	$('#editor').css('height', (window.innerHeight - 58) + 'px');
	if(editor !== null) editor.resize();
	$('#btn-run').popover({
		placement: 'auto',
		trigger: 'hover'
	});
}

function bindHandlers() {
	
	$('#btn-compile').click(function() {
		var code = editor.getValue();
		$('#results').html(getLoader('Compiling...'));
		$.post('/compile', {
			admin: true, 
			code: Base64.encode(code),
			env: env.name
		}, function(response) {
			console.log(response);
			try {
				response = eval( '(' + response + ')' );
				showCompilationResult(response);
				if(response['result'] === 'success') {
					$('#results').prepend($('<div>')
						.css({'margin-top': '10px'})
						.addClass('alert alert-success')
						.append($('<span>').text('Compilation Succeeded'))
					);
				} else {
					$('#results').prepend($('<div>')
						.css({'margin-top': '10px'})
						.addClass('alert alert-danger')
						.append($('<span>').text('Compilation Failed'))
					);
				}
			} catch(err) {
				console.log(err);
				toastr.error('An error occurred! Could not run your code.');
			} finally{
				$('.loader').remove();
			}
		});
	});
	
	$('#btn-run').click(function() {
		var code = editor.getValue();
		if(env.compile) {
			$('#results').html(getLoader('Compiling...'));
			$.post('/compile', {
				admin: true, 
				code: Base64.encode(code),
				env: env.name
			}, function(response) {
				console.log(response);
				try {
					response = eval( '(' + response + ')' );
					showCompilationResult(response);
					if(response['result'] === 'success') {
						if(executable == null) return;
						$('.output').remove();
						$('.loader').remove();
						$('#results').append(getLoader('Executing...'));
						$.post('/execute', {
							admin: true, 
							code: Base64.encode(code), 
							executable: executable, 
							testcase: $('#testcase').val(),
							env: env.name
						}, function(response) {
							console.log(response);
							try {
								response = eval( '(' + response + ')' );
								showExecutionResult(response);
							} catch(err) {
								toastr.error('An error occurred! Could not run your code.');
							} finally {
								$('.loader').remove();
							}
						});
					} else {
						$('.loader').remove();
						$('#results').prepend($('<div>')
							.css({'margin-top': '10px'})
							.addClass('alert alert-danger')
							.append($('<span>').text('Compilation Failed'))
						);
					}
				} catch(err) {
					$('.loader').remove();
					toastr.error('An error occurred! Could not run your code.');
				}
			});
		} else {
			$('#results').append(getLoader('Executing...'));
			var code = Base64.encode(editor.getValue());
			$.post('/execute', {admin: true, code: code, testcase: $('#testcase').val()}, function(response) {
				console.log(response);
				try {
					response = eval( '(' + response + ')' );
					showExecutionResult(response);
				} catch(err) {
					toastr.error('An error occurred! Could not run your code.');
				} finally {
					$('.loader').remove();
				}
			});
		}
	});
	
	$('#btn-eval').click(function() {
		if(assignmentID === null) return;
		var code = editor.getValue();
		$('#results').html(getLoader('Evaluating...'));
		$.post('/evaluate', {admin: true, 'assignment_id': assignmentID, code: Base64.encode(code)}, function(response) {
			console.log(response);
			try {
				response = eval( '(' + response + ')' );
				showEvaluationResults(response);
				if(response.verdict === 'ACCEPTED') {
					$('#results').prepend($('<div>')
						.css({'margin-top': '10px'})
						.addClass('alert alert-success')
						.append($('<span>').text('Program Accepted'))
					);
				} else {
					$('#results').prepend($('<div>')
						.css({'margin-top': '10px'})
						.addClass('alert alert-danger')
						.append($('<span>').text('Program NOT Accepted'))
					);
				}
			} catch(err) {
				toastr.error('An error occurred! Could not run your code.');
			} finally {
				$('.loader').remove();
			}
		});
	});
}

function showExecutionResult(result) {
	var status = result['status'];
	if(status == 'ER') {
		$('#results').html('<div class="alert alert-danger">Something wicked happened. The execution went wrong.' + 
		'Try again after some time.</div>');
	} else if(status == 'OK') {
		$('.output').remove();
		$('#results').append($('<div>').addClass('header-title output').html($('<span>')
				.append($('<span>').text('Output '))
		));
		$('#results').append('<pre class="output">' + result['output'] + '</pre>');
	} else if(status == 'TL') {
		$('#results').html('<div class="alert alert-danger"><span class="glyphicon glyphicon-time"></span> ' +  
		'Time limit exceeded.</div>');
	} else if(status == 'RF') {
		$('#results').html('<div class="alert alert-danger"><span class="glyphicon glyphicon-eye-close"></span> ' + 
		'Your program misbehaved by trying to execute some prohibited system calls. This incident will be reported.</div>');
	} else if(status == 'ML') {
		$('#results').html('<div class="alert alert-danger"><span class="glyphicon glyphicon-credit-card"></span> ' + 
		'Memory limit exceeded.</div>');
	} else if(status == 'AT') {
		$('#results').html('<div class="alert alert-danger"><span class="glyphicon glyphicon-remove-circle"></span> ' + 
		'Your program terminated abnormally. Check if your main program returns a 0 or not.</div>');
	} else if(status == 'RT' || status == 'CT') {
		$('#results').html('<div class="alert alert-warning"><span class="glyphicon glyphicon-warning-sign"></span> ' + 
		'Runtime error!</div>');
		//showExecutionErrors(result['feedback']);
	} else {
		$('#results').html('<div class="alert alert-danger">Something wicked happened. The execution went wrong.' + 
		'Try again after some time.</div>');
	}
}

function showCompilationResult(result) {
	
	var status = result['result'];
	
	if(status === 'success') {
		// Compiled successfully
		executable = result['executable'];
		require(['/scripts/modules/Editor.js'], function(Editor) {
			Editor.showCompilerErrors(editor, result['messages']);
		});
		if(result.messages && result.messages.length > 0) {
			$('#results').append($('<div>').addClass('header-title').html('Compiler Output'));
			$('#results').append($('<textarea>').val(result['raw']).addClass('col-md-12').attr('readonly', 'readonly'));
		}
	} else if(status === 'failure') {
		// Compilation error
		require(['/scripts/modules/Editor.js'], function(Editor) {
			Editor.showCompilerErrors(editor, result['messages']);
		});
		$('#results').append($('<div>').addClass('header-title').html('Compiler Output'));
		$('#results').append($('<textarea>').val(result['raw']).addClass('col-md-12').attr('readonly', 'readonly'));
		executable = null;
	} else {
		// An error occurred.
		executable = null;
	}
}

function showEvaluationResults(response) {
	
	if(env.compile && response.compilation.result !== 'success') {
		// compilation failed
		toastr.warning('This submission did not compile!');
		console.log('compilation failed');
	} else {
		var table = $('<table>').addClass('table table-bordered table-hover');
		var automatic = $('<table>').addClass('table table-bordered table-hover automatic').css('display', 'none');
		table.append($('<tr>')
			.append($('<th>').text('#'))
			.append($('<th>').text('VISIBILITY'))
			.append($('<th>').text('INPUT'))
			.append($('<th>').text('EXPECTED OUTPUT'))
			.append($('<th>').text('ACTUAL OUTPUT'))
		);
		automatic.append($('<tr>')
			.append($('<th>').text('#'))
			.append($('<th>').text('INPUT'))
			.append($('<th>').text('EXPECTED OUTPUT'))
			.append($('<th>').text('ACTUAL OUTPUT'))
		);
		var results = response.evaluation;
		var passed = true;
		var numAutomatic = 0;
		var autoPassed = 0;
		for(var i = 0; i < results.length; i++) {
			
			var status = results[i].actual.status;
			var icon = 'glyphicon ';
			
			if(results[i].type == 0) {
				numAutomatic++;
				if(status == 'OK') {
					if(results[i].expected.trim() == results[i].actual.output.trim()) {
						passed = true;
						autoPassed++;
						icon = icon + 'glyphicon-ok';
					} else {
						passed = false;
						icon = icon + 'glyphicon-remove';
					}
				} else {
					passed = false;
					icon = icon + 'glyphicon-remove';
				}
				var context = 'danger';
				if(passed) context = 'success';
				automatic.append($('<tr>').addClass(context)
					.append($('<td>').text(i+1))
					.append($('<td>').addClass('pre').text(results[i].input))
					.append($('<td>').addClass('pre').text(results[i].expected))
					.append($('<td>').addClass('pre').text(results[i].actual.output))
					.append($('<td>').append($('<span>').addClass(icon)))
				); 
			} else {
				if(status == 'OK') {
					if(results[i].expected.trim() == results[i].actual.output.trim()) {
						passed = true;
						icon = icon + 'glyphicon-ok';
					} else {
						passed = false;
						icon = icon + 'glyphicon-remove';
					}
				} else if(status == 'TL') {
					passed = false;
					icon = icon + 'glyphicon-time';
				} else if(status == 'RT') {
					passed = false;
					icon = icon + 'glyphicon-warning-sign';
				} else if(status == 'AT') {
					passed = false;
					icon = icon + 'glyphicon-remove-circle';
				} else if(status == 'RF') {
					passed = false;
					icon = icon + 'glyphicon-eye-close';
				} else if(status == 'ML') {
					passed = false;
					icon = icon + 'glyphicon-credit-card';
				}
				var context = 'danger';
				if(passed) context = 'success';
				table.append($('<tr>').addClass(context)
					.append($('<td>').text(i+1))
					.append($('<td>').html($('<span>').addClass((results[i].visibility == 1) ? 'glyphicon glyphicon-eye-open' : 'glyphicon glyphicon-eye-close')))
					.append($('<td>').addClass('pre').text(results[i].input))
					.append($('<td>').addClass('pre').text(results[i].expected))
					.append($('<td>').addClass('pre').text(results[i].actual.output))
					.append($('<td>').append($('<span>').addClass(icon)))
				); 
			}
			
		}
		
		$('#results').append($('<div>').text('Evaluation Results').css({
			'background-color': '#DDDDDD',
			'color': '#428BCA',
			'font-size': '20px',
			'font-weight': 'bold',
			'padding': '10px'
		}));
		$('#results').append(table);
		
		$('#results').append($('<h3>')
			.append($('<span>').text('This assignment passed '))
			.append($('<b>').text(autoPassed))
			.append($('<span>').text(' out of '))
			.append($('<a>').css('font-weight', 'bold').attr('href', '').addClass('link-auto').text(numAutomatic))
			.append($('<span>').text(' available automated test cases.'))
		);
		
		$('#results').append(automatic);
		
		$('#results th').css('text-align', 'left');
		
		$('.link-auto').click(function(e) {
			e.preventDefault();
			$('.automatic').toggle();
		});
		
		$('#legend').show();
	}
}

function arrange_lines(text) {
	
	var words = text.split(' ');
	var arranged = '';
	var line = '';
	
	for(var index = 0; index < words.length; index++) {
		line = line + words[index] + ' ';
		if(line.length >= 50) {
			arranged = arranged + line + '\n';
			line = '';
		}
	}
	
	arranged = arranged + line;
	
	return arranged;
}