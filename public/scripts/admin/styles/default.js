/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	var stylize = function() {
		// GENERAL
		$('.nopadding').css({
			'padding': '0px'
		});
		$('body').css({
			'background-color': '#F5F5F5',
			'color': '#333333'
		});
		$('.page-header').css({
			'background-color': '#428BCA',
			'color': '#FFFFFF',
			'padding': '10px',
			'margin-top': '10px'
		});
		$('.badge').css({'background-color': '#428BCA'});
		
		// SIDEBAR
		$('#menu li .menu-main').css({
			'font-size': '30px',
			'padding': '10px',
			'margin': '0px',
			'border': 'none',
			'border-bottom': 'solid',
			'border-radius': '0px',
			'border-width': '2px',
			'border-color': '#FFFFFF'
		});
		$('#admin li a,#vizdomains li a').css({
			'padding': '10px',
			'margin': '0px',
			'border-radius': '0px',
			'border-width': '1px',
			'text-align': 'left'
		});
		$('#admin,#vizdomains').css({
			'background-color': '#1B1B1B'
		});
		$('#menu').css({
			'background-color': '#080808',
			'height': window.innerHeight - 50,
			'border': 'none'
		});
		$('#admin li.active a,#vizdomains li.active a').css({
			'background-color': '#428BCA',
			'color': '#FFFFFF',
			'border': 'none'
		});
		$('#admin li').each(function() {
			if($(this).hasClass('active')) $('#admin').slideDown(500);
		});
		$('#vizdomains li').each(function() {
			if($(this).hasClass('active')) $('#vizdomains').slideDown(500);
		});
		
		if(isMobile) {
			$('#sidebar').removeClass('affix');
			$('#sidebar').css({
				//'border-bottom': 'solid',
				'border-right': 'none'
			});
			$('#menu').css({
				'height': 'auto'
			});
		}
		
		// NAVIGATION BAR
		$('.navbar-inverse, .navbar-inverse .container-fluid').css('background-color', '#080808');
		$('.navbar a').css({'color': '#FFFFFF'});
		$('.navbar .dropdown-menu a').css({'color': '#333333'});
		$('.navbar .dropdown-menu').css({
			'border-radius': '0px',
			'box-shadow': 'none'
		});
		
		$('.short').popover({
			placement: 'auto',
			trigger: 'hover'
		});
	};
	
	var bind = function() {
		$('#menu .menu-main').each(function() {
			var content = $(this).attr('data-contents');
			$(this).tooltip({
				placement: 'right',
				trigger: 'hover',
				html: true,
				title: $('<div>').append(
					$('<div>')
						.css({'text-align': 'center', 'font-size': '15px'})
						.text(content)
				).html()
			});
		});
		
		$('#home').click(function(e) {
			e.preventDefault();
			$('#admin').slideToggle(500);
		});
		
		$('#dataviz').click(function(e) {
			e.preventDefault();
			$('#vizdomains').slideToggle(500);
		});
	};
	
	$(document).ready(function() {
		var paths = window.location.pathname;
		paths = paths.substring(1);
		paths = paths.split('/');
		var location = paths[0];
		$('#' + location).addClass('active');
		stylize();
		bind();
		window.onresize = function() { stylize(); };
	});
});