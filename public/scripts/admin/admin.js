define(['bootstrap'], function() {
	
	$(document).ready(function() {
		stylizeADMIN();
		handlersADMIN();
	});
});

function stylizeADMIN() {
	require(['../scripts/admin/styles/default']);
}

function handlersADMIN() {
	$('#notifications').click(function(e) {
		$.get('/notifications/read', {}, function(response) {
			if(response)
				$('#notification-count').hide();
			else
				console.log(response);
		});
	});
}