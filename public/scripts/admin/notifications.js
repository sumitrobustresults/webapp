define(['bootstrap', 'base64'], function() {
	
	$(document).ready(function() {
		init();
		renderNotifications();
	});
});

function init() {
	try {
		notifications = eval('(' + Base64.decode(notifications) + ')');
		console.log(notifications);
	} catch(err) {
		console.log(err);
		console.log('Something wrong with the notifications set.');
		toastr.error('Unable to load notifications!');
	}
}

function renderNotifications() {
	var now = getDate(new Date());
	for(var i = 0; i < notifications.length; i++) {
		var data = notifications[i];
		var time = getDate(new Date(data.createTime));
		var item = $('<li>')
			.addClass('notification list-group-item')
			.append(processMessage(data.message, data.context))
			.append($('<h5>').text(new Date(data.createTime).toLocaleString()));
		if(time !== now) {
			$('#not-older').append(item);
		} else {
			$('#not-today').append(item.addClass('list-group-item-info'));
		}
	}
}

function getDate(date) {
	return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
}

function processMessage(message, context) {
	var node = $('<h4>');
	if(context === 'REGRADE_REQUEST') {
		message = message.replace(/#([0-9]+)/g, '<a target="_blank" href="/admin/viewer/$1">#$1</a>');
	}
	node.append(message);
	return node;
}