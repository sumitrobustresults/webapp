/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var problemWise = {};
var userWise = {};

define(['listjs', 'bootstrap', 'base64', 'nvd3'], function(List) {
	
	organize();
	
	load_css('nvd3');
	load_css('/styles/loader');
	
	$(document).ready(function() {
		renderOverall();
		stylize();
		bindHandlers();
		var overall = new List('overall-table', {
			valueNames: ['user', 'score']
		});
		var values = ['user-grades'];
		for(var i = 0; i < performance.questions.length; i++)
			values.push(performance.questions[i]);
		/*var gradeCard = new List('grade-card', {
			valueNames: values
		});*/
	});
});

function organize() {
	
	performance = eval('(' + performance + ')');
	
	for(var i = 0; i < performance.performance.length; i++) {
		var user = performance.performance[i];
		userWise[user.id] = user;
		for(var j = 0; j < user.results.length; j++) {
			if(typeof problemWise[user.results[j].problem] === 'undefined')
				problemWise[user.results[j].problem] = [];
			problemWise[user.results[j].problem].push(user.results[j]);
		}
	}
}

function renderOverall() {
	
	var max = 0;
	var mean = 0;
	var total = 0;
	var scores = [];
	
	var bars = 10;
	
	for(var uid in userWise) {
		var row = $('<tr>');
		var problems = userWise[uid].results;
		var score = 0;
		var maxsum = 0;
		var marks = [];
		var maxMarks = [];
		row.append($('<td>').addClass('user-grades').text(userWise[uid].name));
		for(var i = 0; i < problems.length; i++) {
			score += (problems[i].score) ? problems[i].score : 0;
			maxsum += (problems[i].maxscore) ? problems[i].maxscore : 0;
			marks[parseInt(problems[i].question) - 1] = problems[i].score;
			maxMarks[parseInt(problems[i].question) - 1] = problems[i].maxscore;
		}
		for(var i = 1; i <= marks.length; i++)
			row.append($('<td>').addClass('Q' + i).text(marks[i - 1] + '/' + maxMarks[i - 1]));
		score = score / maxsum;
		scores.push(score);
		if(score > max)
			max = score;
		mean = mean + score;
		total++;
		$('.overall .list').append(
			$('<tr>')
				.append($('<td>').addClass('user').text(userWise[uid].name))
				.append($('<td>').addClass('score').text(score.toFixed(2)))
		);
		$('.overall .list-grades').append(row);
	}
	
	$('.overall td').slice(20).hide();
	$('.overall .more').click(function(e) {
		e.preventDefault();
		$(this).hide();
		$('.overall td').show();
	});
	
	mean = mean / total;
	
	$('.overall .summary').html($('<h4>')
		.text('Mean: ' + mean.toFixed(2))
	);
	
	var interval = max / bars;
	var hist = [];
	for(var i = 0; i < bars; i++)
		hist.push({ label: (i * interval + interval / 2).toFixed(2), value: 0 });
	
	for(var i = 0; i < scores.length; i++) {
		var bucket = Math.floor(scores[i] / interval); 
		if(bucket >= bars)
			bucket = bars - 1;
		hist[bucket].value++;
	}
	
	nv.addGraph(function() {
	    var chart = nv.models.discreteBarChart()
	        .x(function(d) {
	            return d.label;
	        }) 
	        .y(function(d) {
	            return parseInt(d.value);
	        })
	        .staggerLabels(false) 
	        .tooltips(true) 
	        .showValues(true) 
	        .transitionDuration(350);
	    
	    chart.yAxis.tickFormat(function(d) {
	    	return parseInt(d);
	    });

	    d3.select('#overall-graph svg')
	    	.attr('height', window.innerHeight - 300)
	    	.attr('width', '100%')
	        .datum([{
	        	key: 'Score Distribution',
	        	values: hist
	        }])
	        .call(chart);

	    nv.utils.windowResize(chart.update);

	    return chart;
	});
}

function stylize() {
	
	require(['../scripts/admin/styles/default'], function() {});
	
}

function bindHandlers() {}