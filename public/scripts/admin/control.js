/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap', 'icheck'], function() {
	
	load_css('icheck');
	
	$(document).ready(function() {
		require(['../scripts/admin/styles/default'], function() {
			$('#control').addClass('active');
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue'
			});
			$('.tool').each(function() {
				var value = parseInt($(this).attr('value'));
				if(value == 1) $(this).find('input[type=radio]').first().iCheck('check');
				else $(this).find('input[type=radio]').last().iCheck('check');
			});
			$('input').on('ifChecked', function() {
				var name = $(this).attr('name').substring(3).toUpperCase();
				var value = $(this).attr('value');
				$.post('/control', {type: 'tools', name: name, value: value}, function(response) {
					try {
						response = eval('(' + response + ')');
						if(response) toastr.success('Settings changed.');
						else toastr.error('Something went wrong! Unable to change settings.');
					} catch(err) {
						toastr.error('Something went wrong! Unable to change settings.');
					}
				});
			});
		});
		bindHandlers();
	});
});

function bindHandlers() {
	$('#btn-env').click(function() {
		var environments =$('.environment');
		var data = [];
		for(var i = 0; i < environments.length; i++) {
			var env = $(environments[i]);
			var id = env.attr('value');
			var params = {
				id: id,
				settings: {
					editor_mode: env.find('.editor_mode').val(),
					compile: env.find('.compile').val(),
					output_format: env.find('.output_format').val(),
					source_ext: env.find('.source_ext').val(),
					binary_ext: env.find('.binary_ext').val(),
					cmd_compile: env.find('.cmd_compile').val(),
					cmd_execute: env.find('.cmd_execute').val(),
					display: env.find('.display').val(),
					link_template: env.find('.link_template').val()
				}
			};
			data.push(params);
		}
		$.post('/control', {type: 'env', settings: data}, function(response) {
			if(response)
				toastr.success('Environment settings updated!');
			else
				toastr.error('Unable to update environment settings!');
		});
	});
	$('#btn-flag').click(function() {
		var flag = $('#flag').val().trim();
		$.post('/control', {type: 'flags', action: 'add', flag: flag}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) window.location.reload();
				else toastr.error('Something went wrong! Unable to save your modifications.');
			} catch(err) {
				toastr.error('Something went wrong! Unable to save your modifications.');
			}
		});
	});
	$('#btn-update').click(function() {
		var time = $('#quota-time').val();
		var memory = $('#quota-mem').val();
		$.post('/control', {type: 'quota', time: time, memory: memory}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) window.location.reload();
				else toastr.error('Something went wrong! Unable to save your modifications.');
			} catch(err) {
				toastr.error('Something went wrong! Unable to save your modifications.');
			}
		});
	});
	$('#btn-delays').click(function() {
		var compile = $('#delay-compile').val();
		var execute = $('#delay-execute').val();
		var evaluate = $('#delay-evaluate').val();
		$.post('/control', {type: 'delays', compile: compile, execute: execute, evaluate: evaluate}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) window.location.reload();
				else toastr.error('Something went wrong! Unable to save your modifications.');
			} catch(err) {
				toastr.error('Something went wrong! Unable to save your modifications.');
			}
		});
	});
	$('.remove-flag').click(function(e) {
		e.preventDefault();
		var flag = $(this).parent().parent().find('.flag').text().trim();
		$.post('/control', {type: 'flags', action: 'remove', flag: flag}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) window.location.reload();
				else toastr.error('Something went wrong! Unable to save your modifications.');
			} catch(err) {
				toastr.error('Something went wrong! Unable to save your modifications.');
			}
		});
	});
}