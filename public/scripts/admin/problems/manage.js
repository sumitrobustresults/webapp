/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var templateEditor = null;
var specEditor = null;

define(['bootstrap', 'modernizr', 'base64', 'jstorage', 'mathjax', 'jstree', 'icheck', 'ace'], function() {
	
	load_css('icheck');
	load_css('jstree');
	load_css('fontawesome');
	load_css('/styles/loader');
	load_css('/styles/admin/problems');
	
	$(document).ready(function() {
		stylize();
		loadTree();
		MathJax.Hub.Config({
			tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] }
		});
	});
});

function loadTree() {
	// Categorize
	try {
		problems = eval('(' + Base64.decode(problems) + ')');
	} catch(err) {
		console.log('Something wrong with the problem set.');
	}
	var categorized = {};
	for(var i = 0; i < problems.length; i++) {
		var cat = problems[i].category;
		if(typeof categorized[cat] === 'undefined') categorized[cat] = [];
		categorized[cat].push(problems[i]);
	}
	
	// Render tree
	var treeNodes = [];
	for(var cat in categorized) {
		var children = [];
		for(var i = 0; i < categorized[cat].length; i++) {
			var problem = categorized[cat][i];
			var title = (problem.title) ? ' (' + problem.title + ')' : '';
			children.push({
				id: 'p' + problem.id,
				icon: 'glyphicon glyphicon-question-sign',
				text: 'P' + problem.id_p + '-V' + problem.id_v + '-D' + problem.id_d + title,
				state: {
					opened: false,
					disabled: false,
					selected: false
				}
			});
		}
		var node = {
			text: cat,
			icon: 'glyphicon glyphicon-folder-close',
			state: {
				opened: false,
				disabled: false,
				selected: false
			},
			children: children
		};
		treeNodes.push(node);
	}
	var loc = window.location.href.split('/');
	$('.tree').jstree({
		core: {
			data: treeNodes,
			themes: {
				stripes: true
			}
		},
		plugins: ['wholerow', 'sort']
	}).on('select_node.jstree', function(e, sel) {
		var id = sel.node.id;
		if(id.match(/p[0-9]+/g)) {
			id = id.substring(1);
			var pid = loc[loc.length - 1];
			if(pid.indexOf('#')) {
				pid = pid.split('#');
				pid = pid[0];
			}
			if(pid !== id)
				window.location.href = '/problems/' + id;
		}
	}).on('ready.jstree', function() {
		var pid = loc[loc.length - 1];
		if(pid.indexOf('#')) {
			pid = pid.split('#');
			pid = pid[0];
		}
		if(!isNaN(pid)) {
			renderProblem(pid);
			pid = 'p' + pid;
			var path = $('.tree').jstree('get_path', pid, false, true);
			for(var i = 0; i < path.length - 1; i++) {
				$('.tree').jstree('open_node', $('.tree').jstree('get_node', path[i]));
			}
			$('.tree').jstree('select_node', pid);
		}
	});
}

function getLoader(text) {

	var child = $('<div>').addClass('gmb-loader');
	child.append($('<div>'));
	child.append($('<div>'));
	child.append($('<div>'));
	
	var loader = $('<h2>').attr('align', 'center').addClass('loader');
	loader.append(child);
	loader.append($('<br>'));
	loader.append($('<span>').text(text));
	
	return loader;
} 

function stylize() {
	
	/*$('#statement-templateEditor').css({
		'height': '300px',
		'overflow-y': 'scroll'
	});
	$('#statement-templateEditor').wysihtml5();
	$('.wysihtml5-toolbar .btn').addClass('btn-default');*/
	
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue'
	});
	$('input[type=radio]').first().iCheck('check');
	
	$('input.en-spec').iCheck({
		checkboxClass: 'icheckbox_square-red',
		increaseArea: '-20%'
	});
	
	$('.en-flag').each(function() {
		var en = $(this).val();
		if(en === '1') $(this).parent().find('.en-spec').iCheck('check');
	});
	
	require(['../scripts/admin/styles/default'], function() {
		$('#home').addClass('active');
	});
	
	$('.data-view').css({
		'background-color': '#FAFAFA',
		'padding': '10px',
		'border': 'solid',
		'border-width': '1px',
		'border-color': '#CCCCCC',
		'border-radius': '5px'
	});
	
	$('.stmt .pre').css({
		'padding': '5px',
		'color': '#333333',
		'background-color': '#F5F5F5',
		'border': 'solid',
		'border-width': '1px',
		'border-color': '#CCCCCC'
	});
	
	if($('#template-editor').length > 0) {
		$('#template-editor').css({'height': '300px'});
		templateEditor = ace.edit("template-editor");
	    templateEditor.setTheme("ace/theme/eclipse");
	    templateEditor.setFontSize(16);
	    templateEditor.getSession().setMode("ace/mode/" + env.editor_mode);
	    templateEditor.getSession().setUseWrapMode(true);
	    templateEditor.setValue($('.template .code').text());
	}
	
	$('#btn-create').click(function(e) {
		var pvd = $('#create .pvd').val().trim();
		var category = $('#create .cat').val().trim();
		var regex = /^(p|P)[0-9]+(v|V)[0-9]+(d|D)[0-9]+$/;
		if(!pvd.match(regex)) {
			toastr.error('Please enter a valid PVD identifier!');
			return;
		}
		if(category === '') {
			toastr.error('Please enter category name!');
			return;
		}
		$.post('/problems', {
			visibility: 0,
			statement: '',
			solution: '',
			template: '',
			category: category.toUpperCase(),
			'local_id': pvd.toLowerCase()
		}, function(response) {
			try {
				var result = eval('(' + response + ')');
				if(result && !isNaN(result)) {
					window.location.href = '/problems/' + result;
				} else toastr.error('Something went wrong! Unable to create problem.');
			} catch(err) {
				console.log(err);
				toastr.error('Something went wrong! Unable to create problem.');
			}
		});
	});
}

function renderProblem(problemID) {
	
	// Styling
	$('.stmt .pre').each(function() {
		$(this).html($(this).text());
		MathJax.Hub.Queue(['Typeset', MathJax.Hub, $(this).get(0)]);
	});
	
	$('.data-view').css('margin-bottom', '10px');
	$('th').css('text-align', 'left');
	
	// Cross Tab Events/Storage
	var pdata = {
		code: $('.solution').text(),
		env: env
	};
	$.jStorage.set('S' + problemID, pdata);
	if($('.ediview').length == 1) {
		$.jStorage.set('editable', true);
		$.jStorage.set('update', false);
		// Update all open problem views.
		$.jStorage.listenKeyChange('update', function(key, action) {
			var value = $.jStorage.get('update');
			if(value === true) {
				$.jStorage.stopListening('update');
				window.location.reload();
			}
		});
	} else $.jStorage.set('editable', false);

	// Event Handlers
	$('#env').change(function(e) {
		var val = $(this).val();
		$.post('/problems/update/environment', {id: problemID, env: val}, function(response) {
			if(response)
				toastr.success('Programming environment updated.');
			else
				toastr.error('Unable to update programming environment!');
		});
	});
	$('#link-edit').click(function(e) {
		e.preventDefault();
		$('.stmt').hide();
		$('.ediview').show();
	});
	$('#link-template').click(function(e) {
		e.preventDefault();
		$('.template').hide();
		$('.templateview').show();
	});
	$('#add').click(function() {
		$('#io').toggle();
	});
	$('.io-panel button').click(function() {
		var input = $('.input').val();
		$(this).attr('disabled', 'disabled');
		$(this).find('.glyphicon').removeClass('glyphicon-floppy-disk').addClass('glyphicon-transfer');
		$(this).find('.btn-title').text(' Generating output...');
		$.post('/compile', {
			code: Base64.encode($('.solution').text()),
			env: env.name
		}, function(response) {
			console.log(response);
			try {
				response = eval( '(' + response + ')' );
				if(response.result === 'success') {
					$.post('/execute', {
						code: Base64.encode($('.solution').text()), 
						executable: response.executable, 
						testcase: input,
						env: env.name
					}, function(result) {
						try {
							result = eval( '(' + result + ')' );
							var visibility = $('input[name=visibility]:checked').val();
							if(result.status === 'OK') {
								$.post('/problems/tests/add/manual', {
									problem_id: problemID,
									input: input,
									output: result.output,
									visibility: visibility
								}, function(response) {
									$('.io-panel button').removeAttr('disabled');
									window.location.reload();
								});
							} else {
								toastr.error('The program did not execute normally. The returned error code was: ' + result.status + '. Please fix the program and then try again.');
								$('.io-panel button .btn-title').text(' Output generation failed.');
								$('.io-panel button .glyphicon').removeClass('glyphicon-transfer').addClass('glyphicon-exclamation-sign');
							}
						} catch(ex) {
							toastr.error('Something went wrong! Could not add test case.');
							$('.io-panel button .btn-title').text(' Output generation failed.');
							$('.io-panel button .glyphicon').removeClass('glyphicon-transfer').addClass('glyphicon-exclamation-sign');
						}
					});
				} else {
					toastr.error('This program did not compile successfully. Please fix the program.');
					$('.io-panel button .btn-title').text(' Output generation failed.');
					$('.io-panel button .glyphicon').removeClass('glyphicon-transfer').addClass('glyphicon-exclamation-sign');
				}
			} catch(err) {
				toastr.error('Something went wrong! Could not add test case.');
				$('.io-panel button .btn-title').text(' Output generation failed.');
				$('.io-panel button .glyphicon').removeClass('glyphicon-transfer').addClass('glyphicon-exclamation-sign');
			}
		});
	});
	$('.link-remove').click(function(e) {
		e.preventDefault();
		var testID = $(this).parent().parent().attr('value');
		$.post('/problems/tests/remove', {test_id: testID}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) {
					$('#tid-' + testID).remove();
				}
			} catch(err) {
				toastr.error('Something went wrong! Unable to remove test case.');
			}
		});
	});
	$('.ediview').on('change keypress paste textInput input', function() {
		$('#save-statement').show();
	});
	templateEditor.getSession().on('change', function(event) {
		$('#template-update').show();
    });
	$('#save-statement').click(function() {
		$.post('/problems/update/statement', {pid: problemID, statement: $('.ediview').val()}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) {
					window.location.reload();
				} else {
					toastr.error('Something went wrong! Unable to save problem statement.');
				}
			} catch(err) {
				toastr.error('Something went wrong! Unable to save problem statement.');
			}
		});
	});
	$('#save-template').click(function() {
		$.post('/problems/update/template', {pid: problemID, template: Base64.encode(templateEditor.getValue().trim())}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) {
					window.location.reload();
				} else {
					toastr.error('Something went wrong! Unable to save solution template.');
				}
			} catch(err) {
				toastr.error('Something went wrong! Unable to save solution template.');
			}
		});
	});
	$('#add-auto').popover({
		placement: 'top',
		content: 'Test cases must be in a csv format having tuples of the form (input,output) and no headers. The input/output should be in base64 encoded format.',
		trigger: 'hover'
	});
	$('#file-auto').change(function() {
		var files = $(this).get(0).files;
		console.log(files);
		if(files.length == 0) return;
		var reader = new FileReader();
		reader.onload = function(e) {
			var text = e.target.result;
			$.post('/p/tests/add/auto', {pid: problemID, data: text}, function(response) {
				console.log(response);
				window.location.reload();
			});
		};
		reader.readAsText(files[0], "UTF-8");
	});
	$('#add-auto').click(function(e) {
		e.preventDefault();
		$('#file-auto').click();
	});
	$('#automated').click(function(e) {
		e.preventDefault();
		$('#auto-tests').toggle();
	});
	$('#link-title').click(function(e) {
		e.preventDefault();
		$('#title-view').toggle();
		$('#title-edit').toggle();
	});
	$('#title-save').click(function() {
		var title = $('#field-title').val().trim();
		$.post('/problems/update/title', {pid: problemID, title: title}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) {
					window.location.reload();
				} else {
					toastr.error('Something went wrong! Unable to save problem title.');
				}
			} catch(err) {
				toastr.error('Something went wrong! Unable to save problem title.');
			}
		});
	});
	$('#del').click(function(e) {
		e.preventDefault();
		alertify.confirm('Are you sure you want to delete this problem? This cannot be undone.', function(e) {
			if(e) {
				$.post('/problems/remove', {id: problemID}, function(response) {
					try {
						var result = eval('(' + response + ')');
						if(result) window.location.href = '/problems/manage';
						else toastr.error('Something went wrong! Unable to delete problem.');
					} catch(err) {
						console.log(err);
						toastr.error('Something went wrong! Unable to delete problem.');
					}
				});
			}
		});
	});
	var practice = $('#practice').val();
	if(practice == 1) {
		$('#practice').iCheck('check');
	}
	$('#practice').on('ifToggled', function(event) {
		var checked = (event.target.checked) ? 1 : 0;
		$.post('/problems/practice', {pid: problemID, practice: checked}, function(response) {
			try {
				var result = eval('(' + response + ')');
				if(result) toastr.success('Problem marked/unmarked as practice.');
				else toastr.error('Something went wrong! Unable to mark/unmark problem.');
			} catch(err) {
				console.log(err);
				toastr.error('Something went wrong! Unable to mark/unmark problem.');
			}
		});
	});
}

function showProblem(problemID) {
	window.location.href = '/problems/' + problemID;
}
