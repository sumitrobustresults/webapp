/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap', 'modernizr', 'base64', 'select'], function() {
	
	load_css('select');
	load_css('/styles/loader');
	load_css('/styles/admin/upload');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
	});
	
	var check_feature_support = function() {
	    // check drag and drop
	    if(Modernizr.draganddrop) {
	        console.log('FEATURE: drag and drop supported.');
	    } else {
	        console.log('FEATURE: drag and drop not supported.');
	    }
	};
});

function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		$('#home').addClass('active');
	});
	$('.drop').css('height', '287px');
	$('.drop .caption').addClass('vcentered');
	$('select').selectpicker();
}

function bindHandlers() {
	$('.drop').get(0).addEventListener('dragover', function(event) {
		event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
	}, false);
	
	$('.drop').get(0).addEventListener('drop', function(event) {
        event.stopPropagation();
        event.preventDefault();
        var files = event.dataTransfer.files;
        startReading(files);
    }, false);
	
	$('#btn-select').click(function() {
		$('#file').click();
	});
	
	$('#file').change(function() {
		var files = $('#file').get(0).files;
		startReading(files);
	});
}

function startReading(files) {
	if(files.length == 0) return;
	var done = false;
	$('#progress').modal('show');
	$('.upview').slideUp(500, function() {
		var problems = {};
		var valid = 0;
		var readCount = 0;
		for(var i = 0; i < files.length; i++) {
			var name = files[i].name;
			var parts = name.split('_');
			if(parts.length < 2) continue;
			var regex = /p[0-9]+v[0-9]+d[0-9]+/;
			if(!parts[0].match(regex)) continue;
			var id = parts[0];
			parts = parts[1].split('.');	
			if(parts.length < 2) continue;
			var type = parts[0];
			if(typeof problems[id] == 'undefined') problems[id] = {};
			valid = valid + 1;
			readFile(files[i], id, type, function(data, id, type) {
				problems[id][type] = data;
				readCount = readCount + 1;
				if(readCount == valid) {
					scrutinize(problems);
					$('#progress').modal('hide');
				}
			});
	    }
	});
}

function scrutinize(problems) {
	
	var validSet = {};
	for(var id in problems) {
		var components = Object.keys(problems[id]);
		var all = components[0];
		for(var i=1;i<components.length;i++) all = all + ',' + components[i]; 
		var flag = (components.length == 3) ? 'list-group-item-success' : 'list-group-item-danger';
		if(components.length == 3) {
			validSet[id] = problems[id];
		}
		$('<a>')
			.attr('href','#' + id)
			.attr('id', id)
			.addClass('list-group-item ' + flag)
			.append($('<b>').text(id))
			.append($('<i>').text(' (' + all + ')'))
		.appendTo('.preview .list-group');
	}
	
	$('.list-group-item').click(function(e) {
		e.preventDefault();
		var id = $(this).attr('id');
		var set = problems[id];
		$('.view h1').text(id.toUpperCase());
		for(var key in set) {
			$('.view h2.' + key).text(key.toUpperCase());
			$('.view pre.' + key).text(set[key]);
		}
		$('.jumbotron').hide();
		$('.view').show();
	});
	
	$('.uploader h3').text(Object.keys(validSet).length + ' valid problem(s) to upload.');
	
	$('#btn-upload').click(function() {
		var category = $('.uploader').find('.category').val().trim();
		var visibility = $('select').val();
		if(category == '') {
			$('.category').parent().addClass('has-error');
			return;
		}
		startUpload(validSet, category, visibility);
	});
	
	$('.preview').show();
}

function readFile(file, id, type, callback) {
    var reader = new FileReader();  
    reader.onload = function(e) {  
        var text = e.target.result; 
        callback(text, id, type);
    }
    reader.readAsText(file, "UTF-8");
}

function startUpload(problemSet, category, visibility) {
	var numProblems = Object.keys(problemSet).length;
	if(numProblems == 0) return;
	$('.uploader').append(
		$('<div>').addClass('progress').css('margin-top', '10px')
		.append($('<div>').addClass('progress-bar').css('width', '0%'))
	);
	var uploaded = 0;
	for(var id in problemSet) {
		var problem = problemSet[id];
		$.post('/problems', {
			local_id: id,
			visibility: visibility,
			category: category,
			statement: problem.statement,
			solution: problem.solution,
			template: problem.template
		}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response === false) {
					toastr.error('Something went wrong with the system. The problems could not be uploaded.');
					return;
				}
				uploaded = uploaded + 1;
				$('.progress-bar').css('width', (uploaded * 100 / numProblems) + '%');
				if(uploaded == numProblems) {
					$('.progress-bar').append($('<span>').text('Upload Complete'));
					window.location.href = '/problems/manage';
				}
			} catch(err) {
				console.log(err);
				toastr.error('Something went wrong with the system. The problems could not be uploaded.');
			}
		});
	}
}