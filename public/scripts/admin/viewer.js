/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var Range = null;
var editor = null;

var curPos = -1;
var paused = true;

define(['bootstrap', 'ace', 'base64', 'mathjax', 'xssfilter'], function() {
	
	load_css('/styles/loader');
	load_css('/styles/admin/viewer');
	
	$(document).ready(function() {
		try {
			compilations = eval('(' + Base64.decode(compilations) + ')');
			executions = eval('(' + Base64.decode(executions) + ')');
		} catch(err) {
			console.log(err);
		}
		stylize();
		bindHandlers();
		initEditor();
		init();
		window.onresize = function() {
			$('#editor, .detailspane, .timeline').css('height', window.innerHeight - 50);
			editor.resize();
		};
		MathJax.Hub.Config({
			tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] }
		});
		$('.prob').html($('.prob').text());
	});
});

function init() {
	
	var tmp = {};
	for(var i = 0; i < compilations.length; i++) {
		tmp[compilations[i].code_id] = compilations[i];
	}
	compilations = tmp;
	
	tmp={};
	for(var i = 0; i < executions.length; i++) {
		var codeid = executions[i].code_id;
		if(typeof tmp[codeid] === 'undefined') tmp[codeid] = [];
		tmp[codeid].push(executions[i]);
	}
	executions = tmp;
	
	//var sid = $('.thumbnail').find('div[value=2]');
	if(window.location.hash) {
		$(window.location.hash).click();
	} else if(assignment.is_submitted) {
		$('#' + assignment.submission).click();
	} else {
		$('.thumbnail').first().click();
	}
	
	$('.code').each(function() {
		var code = Base64.decode($(this).attr('value'));
		code = xssFilters.inHTMLData(code);
		$(this).html(code);
	});
}


function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		$('#viewer').css({
			'border-right': 'solid',
			'border-right-color': '#2A6496'
		});
		$('#home').addClass('active');
		$('#home .badge').text('Viewer');
		
		var width = $('.thumbnail').css('width');
		width = width.substring(0, width.length - 2);
		$('.thumbnail .code').css('height', parseInt(width / 2));
	});
	
	$('#editor, .detailspane, .timeline').css('height', window.innerHeight - 50);
	$('.container-fluid').css({'padding': '0px'});
	
	require(['toastr'], function() {
		toastr.options.positionClass = 'toast-bottom-right';
	});
}

function bindHandlers() {
	
	$('#btn-predict-grade').click(predictGrade);
	
	$('.thumbnail').click(function(e) {
		e.preventDefault();
		var code64 = $(this).find('.code').attr('value');
		editor.setValue(Base64.decode(code64));
		$('#details').html('');
		$('#details').append($('<h3>')
			.append(' ' + $(this).find('.timestamp').html())
			.append(
				$('<a>')
					.attr('href', '')
					.attr('data', $(this).attr('id'))
					.addClass('text-danger pull-right del')
					.append($('<span>').addClass('glyphicon glyphicon-trash'))
					.click(function(e) {
						e.preventDefault();
						var codeID = $(this).attr('data');
						alertify.confirm('Are you sure tou want to delete this code save? This cannot be undone.', function(e) {
							if(e) {
								$.post('/dataviz/code/delete', {id: codeID}, function(response) {
									try {
										var result = eval('(' + response + ')');
										if(result) {
											if(!rewind()) {
												if(!forward()) window.location.reload();
											}
											$('#' + codeID).remove();
										} else toastr.error('Something went wrong! Unable to delete code version.');
									} catch(err) {
										toastr.error('Something went wrong! Unable to delete code version.');
									}
								});
							}
						});
					})
			)
		);
		$('#details').append($('<div>').addClass('clearfix'));
		var trigger = $(this).find('.trigger').attr('value');
		var alertClass = 'alert-info';
		if(trigger == 3) {
			var result = compilations[$(this).attr('id')];
			if(result) {
				console.log(result);
				if(result.compiled === 1) {
					alertClass = 'alert-success';
				} else {
					alertClass = 'alert-danger';
				}
			}
		}
		var iconMap = ['glyphicon-floppy-save', 'glyphicon-floppy-disk', 'glyphicon-open', 'glyphicon-cog', 'glyphicon-file'];
		var saveMap = ['Auto Saved', 'Manually Saved', 'Submitted', 'Compiled', 'Pasted'];
		$('#details').append(
			$('<h2>').addClass('alert ' + alertClass)
				.append($('<span>').addClass('glyphicon ' + iconMap[trigger]))
				.append($('<span>').text(' ' + saveMap[trigger]))
		);
		// Compiler errors.
		var codeID = $(this).attr('id');
		if(trigger == 3) {
			$('#details').append($('<a>')
				.attr('href', '')
				.attr('data', codeID)
				.text('View Compiler Messages')
				.css({
					'text-decoration': 'none',
					'font-size': 'large'
				})
				.addClass('text-warning')
				.click(function(e) {
					e.preventDefault();
					$(this).hide();
					var target = $(this).parent();
					$.getJSON('/dataviz/errors', {id: $(this).attr('data')}, function(data) {
						console.log(data);
						var node = $('<table>').addClass('table table-condensed');
						for(var i = 0; i < data.length; i++) {
							node.append($('<tr>')
								.append($('<td>').text(data[i].message))
								.append($('<td>').addClass('text-right').text(data[i].line))
							);
						}
						target.append($('<div>').addClass('alert alert-warning')
								.append($('<h4>').text('Compiler Messages'))
							.append(node));
					});
				})
			);
		}
		// Multiple executions
		if(typeof executions[$(this).attr('id')] !== 'undefined') {
			var set = executions[$(this).attr('id')];
			$('#details').append(
				$('<h3>').addClass('alert alert-info')
					.append($('<span>').addClass('glyphicon glyphicon-play'))
					.append($('<span>').text(' Executed'))
			);
			for(var i = 0; i < set.length; i++) {
				var execution = set[i];
				$('#details').append(
					$('<table>').addClass('table table-bordered')
						.append($('<tr>').append($('<th>').text('RESULT')).append($('<td>').text(execution.result)))
						.append($('<tr>').append($('<th>').text('INPUT')).append($('<td>').addClass('pre').text(execution.input)))
						.append($('<tr>').append($('<th>').text('OUTPUT')).append($('<td>').addClass('pre').text(execution.output)))
				);
			}
		}
		$('.thumbnail').removeClass('active');
		$(this).addClass('active');
		curPos = $('.thumbnail').index($(this));
	});
	$('#edit').click(function() {
		var pdata = {
			code: editor.getValue(),
			env: env
		};
		$.jStorage.set('A' + assignmentID, pdata);
		$.jStorage.set('editable', false);
		$(this).attr('href', '/admin/editor#A' + assignmentID);
	});
	$('a[href*=#]:not([href=#])').click(function() {
		if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			var pos =  $(this).get(0).offsetParent.offsetTop;
			if (target.length) {
		        $('.timeline').animate({
		        	scrollTop: pos - 10
		        }, 500);
		        return false;
	      	}
	    }
    });
	$('#playpause').click(function() {
		 if(paused) {
			 $(this).find('span').removeClass('glyphicon-play').addClass('glyphicon-pause');
			 play();
		 } else {
			 $(this).find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
			 paused = true;
		 }
	});
	$('#rewind').click(function() {
		rewind();
	});
	$('#forward').click(function() {
		forward();
	});
	$('#begin').click(function() {
		//paused = true;
		curPos = -1;
		$('.thumbnail').last().click();
	});
	$('#end').click(function() {
		//paused = true;
		curPos = -1;
		$('.thumbnail').first().click();
	});
	$('#evaluate').click(function() {
		$(this).attr('disabled', 'disabled');
		$('#results').html(getLoader('Evaluating Submission'));
		$.post('/evaluate', {assignment_id: assignmentID, admin: true}, function(response) {
			console.log(response);
			try {
				response = eval('(' + response + ')');
				if(response === false) toastr.warning('This problem has no test cases to evaluate upon.');
				else showEvaluationResults(response);
			} catch(err) {
				toastr.error('Something went wrong! Could not evaluate assignment.');
			} finally {
				$('#evaluate').removeAttr('disabled');
				$('.loader').remove();
			}
		});
	});
	$('#submission').click(function() {
		$('#' + assignment.submission).click();
	});
	$('.pbar').click(function() {
		$('.prob').slideToggle(500);
	});
	$('#btn-grade').click(function() {
		var marks = $('#marks').val();
		var maxMarks = parseInt($('.max').text());
		if(marks === '' || isNaN(marks) || marks < 0 || marks > maxMarks) {
			toastr.error('Invalid marks! Please enter valid marks.');
			return;
		}
		var comments = $('#comment').val();
		$.post('/dataviz/grade', {assignment_id: assignmentID, marks: marks, comments: comments}, function(response) {
			//console.log(response);
			try {
				response = eval('(' + response + ')');
				toastr.success('Assignment has been graded.');
				window.location.reload();
			} catch(err) {
				toastr.error('An error occurred. Could not grade this assignment.');
				console.log(err);
			}
		});
	});
	$('#btn-memoized').click(function() {
		$('#memoized').modal('show');
	});
}

function initEditor() {
	Range = ace.require("ace/range").Range;
	editor = ace.edit("editor");
    editor.setTheme("ace/theme/eclipse");
    editor.setFontSize(16);
    editor.setReadOnly(true);
    editor.getSession().setMode("ace/mode/" + env.editor_mode);
    editor.getSession().setUseWrapMode(true);
}

function getLoader(text) {

	var child = $('<div>').addClass('gmb-loader');
	child.append($('<div>'));
	child.append($('<div>'));
	child.append($('<div>'));
	
	var loader = $('<h2>').attr('align', 'center').addClass('loader');
	loader.append(child);
	loader.append($('<br>'));
	loader.append($('<span>').text(text));
	
	return loader;
} 

function showEvaluationResults(response) {
	
	if(response.defaulter) toastr.warning('This assignment has no submission. Evaluation has been done on the last saved code version.');
	
	if(env.compile && response.compilation.result !== 'success') {
		// compilation failed
		toastr.warning('This submission did not compile!');
		console.log('compilation failed');
	} else {
		var table = $('<table>').addClass('table table-bordered table-hover');
		var automatic = $('<table>').addClass('table table-bordered table-hover automatic').css('display', 'none');
		table.append($('<tr>')
			.append($('<th>').text('#'))
			.append($('<th>').text('VISIBILITY'))
			.append($('<th>').text('INPUT'))
			.append($('<th>').text('EXPECTED OUTPUT'))
			.append($('<th>').text('ACTUAL OUTPUT'))
		);
		automatic.append($('<tr>')
			.append($('<th>').text('#'))
			.append($('<th>').text('INPUT'))
			.append($('<th>').text('EXPECTED OUTPUT'))
			.append($('<th>').text('ACTUAL OUTPUT'))
		);
		var results = response.evaluation;
		var passed = true;
		var numAutomatic = 0;
		var autoPassed = 0;
		for(var i = 0; i < results.length; i++) {
			
			var status = results[i].actual.status;
			var icon = 'glyphicon ';
			
			if(results[i].type == 0) {
				numAutomatic++;
				if(status == 'OK') {
					if(results[i].expected.trim() == results[i].actual.output.trim()) {
						passed = true;
						autoPassed++;
						icon = icon + 'glyphicon-ok';
					} else {
						passed = false;
						icon = icon + 'glyphicon-remove';
					}
				} else {
					passed = false;
					icon = icon + 'glyphicon-remove';
				}
				var context = 'danger';
				if(passed) context = 'success';
				automatic.append($('<tr>').addClass(context)
					.append($('<td>').text(i+1))
					.append($('<td>').addClass('pre').text(results[i].input))
					.append($('<td>').addClass('pre').text(results[i].expected))
					.append($('<td>').addClass('pre').text(results[i].actual.output))
					.append($('<td>').append($('<span>').addClass(icon)))
				); 
			} else {
				if(status == 'OK') {
					if(results[i].expected.trim() == results[i].actual.output.trim()) {
						passed = true;
						icon = icon + 'glyphicon-ok';
					} else {
						passed = false;
						icon = icon + 'glyphicon-remove';
					}
				} else if(status == 'TL') {
					passed = false;
					icon = icon + 'glyphicon-time';
				} else if(status == 'RT') {
					passed = false;
					icon = icon + 'glyphicon-warning-sign';
				} else if(status == 'AT') {
					passed = false;
					icon = icon + 'glyphicon-remove-circle';
				} else if(status == 'RF') {
					passed = false;
					icon = icon + 'glyphicon-eye-close';
				} else if(status == 'ML') {
					passed = false;
					icon = icon + 'glyphicon-credit-card';
				}
				var context = 'danger';
				if(passed) context = 'success';
				table.append($('<tr>').addClass(context)
					.append($('<td>').text(i+1))
					.append($('<td>').html($('<span>').addClass((results[i].visibility == 1) ? 'glyphicon glyphicon-eye-open' : 'glyphicon glyphicon-eye-close')))
					.append($('<td>').addClass('pre').text(results[i].input))
					.append($('<td>').addClass('pre').text(results[i].expected))
					.append($('<td>').addClass('pre').text(results[i].actual.output))
					.append($('<td>').append($('<span>').addClass(icon)))
				); 
			}
			
		}
		
		$('#results').html($('<hr>'));
		$('#results').append($('<div>').text('Evaluation Results').css({
			'background-color': '#DDDDDD',
			'color': '#428BCA',
			'font-size': '20px',
			'font-weight': 'bold',
			'padding': '10px'
		}));
		$('#results').append(table);
		
		$('#results').append($('<h3>')
			.append($('<span>').text('This assignment passed '))
			.append($('<b>').text(autoPassed))
			.append($('<span>').text(' out of '))
			.append($('<a>').css('font-weight', 'bold').attr('href', '').addClass('link-auto').text(numAutomatic))
			.append($('<span>').text(' available automated test cases.'))
		);
		
		$('#results').append(automatic);
		
		$('#results th').css('text-align', 'left');
		
		$('.link-auto').click(function(e) {
			e.preventDefault();
			$('.automatic').toggle();
		});
		
		$('#legend').show();
	}
}

function play() {
	paused = false;
	resume();
}

function resume() {
	if(paused) return;
	if(curPos == -1) curPos = $('.thumbnail').length - 1;
	else if(curPos == 0) return;
	curPos--;
	$('.thumbnail').get(curPos).click();
	setTimeout('resume()', 1000);
}

function rewind() {
	if(curPos == $('.thumbnail').length - 1) return false;
	curPos++;
	$('.thumbnail').get(curPos).click();
	return true;
}

function forward() {
	if(curPos == 0) return false;
	curPos--;
	$('.thumbnail').get(curPos).click();
	return true;
}

function predictGrade() {
	var aid = $('#aid-txt').text();
	$.post('/plugins', {'name': 'grade-prediction', 'aid': aid}, function(response) {
		console.log(response);
		var score = parseInt(response.score * response.max_marks);
		$('#predicted-score').text('Predicted score: ' + score);
	});
}