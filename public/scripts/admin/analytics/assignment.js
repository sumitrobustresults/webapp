/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var tinycolor = null;

define(['tinycolor', 'bootstrap', 'nvd3', 'base64', 'jstree', 'd3', 'moment'], function(tc) {
	
	tinycolor = tc;
	
	load_css('jstree');
	load_css('nvd3');
	load_css('/styles/admin');
	
	codeHistory = eval('(' + Base64.decode(codeHistory) + ')');
	compilationHistory = eval('(' + Base64.decode(compilationHistory) + ')');
	executions = eval('(' + Base64.decode(executions) + ')');
	evaluations = eval('(' + Base64.decode(evaluations) + ')');
	
	$(document).ready(function() {
		require(['../scripts/admin/styles/default'], function() {});
		$('#size .opener').click(function(e) {
			e.preventDefault();
			$(this).hide();
			$(this).parent().find('.viewer').show();
			plotSizeTimeline();
		});
		$('#save .opener').click(function(e) {
			e.preventDefault();
			$(this).hide();
			$(this).parent().find('.viewer').show();
			plotSaveSequence();
		});
		$('#syntax .opener').click(function(e) {
			e.preventDefault();
			$(this).hide();
			$(this).parent().find('.viewer').show();
			syntacticAnalysis();
		});
		$('#ctimeline .opener').click(function(e) {
			e.preventDefault();
			$(this).hide();
			$(this).parent().find('.viewer').show();
			plotCompilationTimeline();
		});
		$('#executions .opener').click(function(e) {
			e.preventDefault();
			$(this).hide();
			$(this).parent().find('.viewer').show();
			plotExecutionSequence();
		});
	});
});

function plotExecutionSequence() {
	var dataset = [
       {key: 'Execution Success', values: []},
       {key: 'Execution Error', values: []},
       {key: 'Evaluation Success', values: []},
       {key: 'Evaluation Failure', values: []}
   ];
	
	var evalStart = new Date();
	var execStart = new Date();
	
	for(var i = 0; i < evaluations.length; i++)
		if(new Date(evaluations[i].evaluate_time) < evalStart)
			evalStart = new Date(evaluations[i].evaluate_time);
	
	for(var i = 0; i < executions.length; i++)
		if(new Date(executions[i].execute_time) < execStart)
			execStart = new Date(executions[i].execute_time)
	
	for(var i = 0; i < evaluations.length; i++) {
		var e = evaluations[i];
		if(parseInt(e.passed) == 1)
			dataset[2].values.push({x: moment(new Date(e.evaluate_time)).diff(evalStart, 'seconds'), y: 1, size: 20});
		else
			dataset[3].values.push({x: moment(new Date(e.evaluate_time)).diff(evalStart, 'seconds'), y: 1, size: 20});
	}
	
	for(var i = 0; i < executions.length; i++) {
		var e = executions[i];
		if(e.result === 'OK')
			dataset[0].values.push({x: moment(new Date(e.execute_time)).diff(execStart, 'seconds'), y: 2, size: 20});
		else
			dataset[1].values.push({x: moment(new Date(e.execute_time)).diff(execStart, 'seconds'), y: 2, size: 20});
	}
	
	nv.addGraph(function() {
		var chart = nv.models.scatterChart()
            .showDistX(true)
            .showDistY(true)
            .showLegend(true)
            .transitionDuration(350)
            .color(d3.scale.category10().range());
		chart.xAxis
		    .axisLabel('Timeline')
		    .tickFormat(function(d) { return d; });
		chart.yAxis
	    	.axisLabel('');
		chart.tooltipContent(function(key) {
			return $('<div>').text(key).html();
	    });
		d3.select('#execs svg')
			.attr('height', window.innerHeight / 2 - 90 + 'px')
			.attr('width', '100%')
		    .datum(dataset)
		    .call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});
}

function plotCompilationTimeline() {
	var cMap = {};
	var cTimeline = [];
	var errorMap = {};
	var timeStamps = [];
	for(var i = 0; i < compilationHistory.length; i++) {
		var c = compilationHistory[i];
		if(typeof cMap[c.code_id] === 'undefined')
			cMap[c.code_id] = [];
		cMap[c.code_id].push(c);
	}
	for(var i = 0; i < codeHistory.length; i++) {
		var c = codeHistory[i];
		if(c.event === 3) {
			cTimeline.push({
				id: c.id,
				errors: (typeof cMap[c.id] === 'undefined') ? null : cMap[c.id]
			});
			timeStamps.push(new Date(c.timestamp));
		}
	}
	for(var i = 0; i < cTimeline.length; i++) {
		var c = cTimeline[i];
		if(c.errors === null)
			continue;
		for(var j = 0; j < c.errors.length; j++) {
			var m = Base64.encode(c.errors[j].message);
			if(typeof errorMap[m] === 'undefined')
				errorMap[m] = [];
			errorMap[m].push({
				'code_id': c.errors[j].code_id,
				'message': c.errors[j].message,
				'timestamp': new Date(c.errors[j].timestamp),
				'type': c.errors[j].type
			});
		}
	}
	var errorTime = {};
	for(var er in errorMap) {
		var time = [];
		for(var i = 0; i < cTimeline.length; i++) {
			var c = cTimeline[i];
			if(c.errors === null) {
				time.push(false);
				continue;
			}
			var found = false;
			for(var j = 0; j < c.errors.length; j++) {
				var m = Base64.encode(c.errors[j].message);
				if(m === er) {
					found = true;
					break;
				}
			}
			if(found) time.push(true);
			else time.push(false);
		}
		errorTime[er] = time;
	}
	var dataSet = [];
	for(var er in errorTime) {
		var start = null;
		var stop = null;
		for(var i = 0; i < errorTime[er].length; i++) {
			if(errorTime[er][i]) {
				if(start === null)
					start = i;
				else stop = i;
			}
		}
		if(stop === null)
			stop = start;
		dataSet.push({
			error: er,
			start: start,
			stop: stop + 1
		});
	}
	var minTime = null;
	var maxTime = null
	for(var i = 0; i < timeStamps.length; i++) {
		if(minTime === null)
			minTime = timeStamps[i];
		else if(timeStamps[i] < minTime)
			minTime = timeStamps[i];
		if(maxTime === null)
			maxTime = timeStamps[i];
		else if(timeStamps[i] > maxTime)
			maxTime = timeStamps[i];
	}
	//console.log(dataSet);
	var margin = {
		left: 50,
		bottom: 10
	}, thickness = 40, padding = 2, row = thickness + padding;
	var colors = [];
	for(var i = 0; i < dataSet.length; i++) {
		var color = tinycolor.random().desaturate().toString();
		while(colors.indexOf(color) != -1 || !tinycolor.isReadable(color, '#FFFFFF'))
			color = tinycolor.random().desaturate().toString();
		colors.push(color);
	}
	for(var i = 0; i < colors.length; i++)
		$('#legend').append(
			$('<tr>')
				.append($('<td>').css({'background-color': colors[i]}))
				.append($('<td>').text(errorMap[dataSet[i].error][0].message))
		);
	var width = $('#ctimeline').css('width'); 
	width = parseInt(width.substring(0, width.length - 2)) - 20;
	var scaleX = d3.scale.linear()
		.domain([0, moment(maxTime).diff(moment(minTime), 'seconds')])
		.range([0, width - margin.left]);
	var chart = d3.select('#ctimeline svg')
		.attr('width', '100%')
		.attr('height', dataSet.length * row + 100);
	var xAxis = d3.svg.axis()
		.scale(scaleX)
		.orient('bottom')
		.tickFormat(function(d) {
			return moment.duration(d, 'seconds').humanize();
		});
	chart.append('g')
		.attr('class', 'axis')
		.attr('transform', 'translate(' + margin.left + ',' + (dataSet.length * row + margin.bottom) + ')')
		.call(xAxis);
	var bars = chart.selectAll('.bar')
		.data(dataSet)
		.enter()
		.append('g')
		.attr('class', 'bar');
	bars.append('rect')
		.attr('y', function(d, i) {
			return row * i; 
		})
		.attr('x', function(d) {
			var start = moment(timeStamps[d.start]);
			var offset = start.diff(moment(minTime), 'seconds');
			return margin.left + scaleX(offset);
		})
		.attr('width', function(d, i) {
			var start = moment(timeStamps[d.start]);
			var stop = moment(timeStamps[d.stop]);
			var length = stop.diff(start, 'seconds');
			return scaleX(length);
		})
		.attr('height', function(d) {
			return thickness;
		})
		.attr('fill', function(d, i) {
			return colors[i];
		});
	bars.append('text')
		.attr('x', function(d) {
			var start = moment(timeStamps[d.start]);
			var offset = start.diff(moment(minTime), 'seconds');
			return margin.left + 5 + scaleX(offset);
		})
		.attr('y', function(d, i) {
			return row * i + 15;
		})
		.attr('dy', '5px')
		.attr('fill', 'white')
		.text(function(d) {
			var start = moment(timeStamps[d.start]);
			var stop = moment(timeStamps[d.stop]);
			var length = stop.diff(start, 'seconds');
			return "Fixed " + moment.duration(length, 'seconds').humanize(true);
		});
	var zones = chart.selectAll('.zone')
		.data(timeStamps)
		.enter()
		.append('g')
		.attr('class', 'zone');
	zones.append('line')
		.attr('x1', function(d) {
			var x = moment(d).diff(minTime, 'seconds');
			return margin.left + scaleX(x);
		})
		.attr('x2', function(d) {
			var x = moment(d).diff(minTime, 'seconds');
			return margin.left + scaleX(x);
		})
		.attr('y1', 0)
		.attr('y2', dataSet.length * row)
		.attr('stroke', '#CCCCCC');
	$('.axis path, .axis line').css({
		'fill': 'none',
		'stroke': '#AAAAAA',
		'shape-rendering': 'crispEdges'
	});
}

function plotSizeTimeline() {
	
	var dataSet = [];
	
	for(var i = codeHistory.length - 1; i >= 0; i--) {
		var saveEvent = codeHistory[i].event;
		var saveTime = new Date(codeHistory[i].timestamp);
		var code = codeHistory[i].code;
		if(isNaN(saveTime.getTime())) continue;
		dataSet.push({
			x: saveTime,
			y: code.length
		});
	}
	
	nv.addGraph(function() {
		var chart = nv.models.lineWithFocusChart();
		chart.xAxis
		    .axisLabel('Timeline')
		    .tickFormat(function(d) { return d3.time.format('%d/%m %H:%M')(new Date(d)) });
		chart.yAxis
	    	.axisLabel('Code Size')
	    	.tickFormat(function(d) { return d + ' B'; });
		chart.x2Axis
		    .tickFormat(function(d) { return d3.time.format('%d/%m %H:%M')(new Date(d)) });
		chart.y2Axis
	    	.axisLabel('Code Size')
	    	.tickFormat(function(d) { return d + ' B'; });
		d3.select('#progress svg')
			.attr('height', window.innerHeight - 100 + 'px')
			.attr('width', '100%')
			.datum([{values: dataSet, key: 'Code Size'}])
			.call(chart);
		nv.utils.windowResize(function() { chart.update() });
		return chart;
	});
}

function plotSaveSequence() {
	
	var dataSet = [
       {
    	   key: 'Auto Save',
    	   values: []
       },
       {
    	   key: 'Manual Save',
    	   values: []
       },
       {
    	   key: 'Submit',
    	   values: []
       },
       {
    	   key: 'Compile',
    	   values: []
       },
       {
    	   key: 'Paste',
    	   values: []
       }
    ];
	
	for(var i = codeHistory.length - 1; i >= 0; i--) {
		var saveEvent = codeHistory[i].event;
		var saveTime = new Date(codeHistory[i].timestamp);
		var code = codeHistory[i].code;
		if(isNaN(saveTime.getTime()) || saveEvent > 4) continue;
		dataSet[saveEvent].values.push({
			x: saveTime,
			y: code.length,
			size: 20
		});
	}
	
	nv.addGraph(function() {
		var chart = nv.models.scatterChart()
            .showDistX(true)
            .showDistY(true)
            .showLegend(true)
            .transitionDuration(350)
            .color(d3.scale.category10().range());
		chart.xAxis
		    .axisLabel('Timeline')
		    .tickFormat(function(d) { return d3.time.format('%d/%m %H:%M')(new Date(d)) });
		chart.yAxis
	    	.axisLabel('Code Size')
	    	.tickFormat(function(d) { return d + ' B'; });
		chart.tooltipContent(function(key) {
			return $('<div>').append($('<h4>').css('padding', '3px').text(key)).html();
	    });
		d3.select('#saves svg')
			.attr('height', window.innerHeight / 2 - 90 + 'px')
			.attr('width', '100%')
		    .datum(dataSet)
		    .call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});
}

function syntacticAnalysis() {
	
	if(compilationHistory.length == 0) {
		$('#syntax').parent().append($('<h3>').text('No Data Available.'));
		return;
	}
	
	var errors = {};
	var codeMap = {};
	
	for(var i = 0; i < compilationHistory.length; i++) {
		var hash = compilationHistory[i].hash;
		if(typeof errors[hash] === 'undefined')
			errors[hash] = {name: compilationHistory[i].error, list: []};
		errors[hash].list.push(compilationHistory[i]);
	}
	
	for(var i = 0; i < codeHistory.length; i++) {
		codeMap[codeHistory[i].id] = codeHistory[i].code;
	}
	
	var treeData = [];
	var index = 0;
	for(var hash in errors) {
		index++;
		var items = errors[hash].list;
		var nodes = [];
		for(var i = 0; i < items.length; i++) {
			nodes.push({
				icon: 'glyphicon glyphicon-record',
				text: shorten(items[i].message),
				data: items[i]
			});
		}
		treeData.push({
			icon: 'glyphicon glyphicon-warning-sign',
			text: shorten('(E' + index + ') ' + errors[hash].name),
			children: nodes
		});
	}
	
	$('.tree').jstree({
		core: {
			data: treeData,
			themes: {
				stripes: true
			}
		},
		plugins: ['wholerow', 'sort']
	}).on('select_node.jstree', function(e, sel) {
		var data = sel.node.data;
		if(data === null) return;
		var code = Base64.decode(codeMap[data.code_id]);
		var lines = code.split("\n");
		var elem = $('<ol>');
		for(var i = 0; i < lines.length; i++)
			if(data.line === i+1)
				elem.append($('<li>').addClass('bg-danger text-danger').text(lines[i]));
			else
				elem.append($('<li>').text(lines[i]));
		var context = (data.type === 'error') ? 'danger' : 'warning';
		$('#code').html(elem);
		$('#error-name').text(data.error);
		$('#error-type').text(data.message);
		$('#goto-code').attr('href', '/admin/viewer/' + assignmentID + '#' + data.code_id);
		$('#link-type').attr('href', '/dataviz#' + data.hash);
		$('#error-type')
			.removeClass('alert-danger')
			.removeClass('alert-warning')
			.addClass('alert-' + context);
		$('#code-view').show();
	});
}

function shorten(text) {
	
	var short = text.substring(0, 50);
	
	if(short.length < text.length)
		short = short + '...';
	
	return short;
}