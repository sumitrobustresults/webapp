/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var changes = false;
var allEvents = null;
var schedules = null;

define(['fullcalendar', 'bootstrap', 'select', 'datetime', 'icheck', 'jstree'], function() {
	
	load_css('fullcalendar');
	load_css('select');
	load_css('icheck');
	load_css('jstree');
	load_css('datetime');
	load_css('/styles/loader');
	load_css('/styles/admin');
	load_css('/styles/admin/events');
	
	var path = window.location.pathname.split("/");
	var loc = path[path.length - 1];
	
	$(document).ready(function() {
		if(loc === 'events')
			loadCalendar();
		require(['../scripts/admin/styles/default'], function() {
			$('#home').addClass('active');
		});
		$('.selectpicker').selectpicker();
		$('.date').datetimepicker();
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue'
		});
		$('input[name=algo]').first().iCheck('check');
		if(loc === 'events') {
			//$('#btn-assign').attr('disabled', 'disabled');
			$('.right-panel').css({
				'height': (window.innerHeight - 50) + 'px',
				'overflow-y': 'scroll',
				'border-left': 'solid',
				'border-width': '1px',
				'border-color': '#CFCECE'
			});
			bindHandlers();
		} 
		$('#assignment').css({
			'height': window.innerHeight - 60
		});
		assignPerspective();
	});
});

function assignPerspective() {
	$('#next').click(function() {
		var next = $('.slide.active').next();
		if(next.hasClass('slide')) {
			if(!next.next().hasClass('slide')){
				$('#next').hide();
				$('#finish').show();
			}
			stage(parseInt($('.slide.active').attr('id').substring(5)));
			$('.slide.active').fadeOut(function() {
				next.fadeIn();
				$('.slide').removeClass('active');
				next.addClass('active');
			});
		} 
		$('#prev').show();
	});
	$('#prev').click(function() {
		var prev = $('.slide.active').prev();
		if(prev.hasClass('slide')) {
			if(!prev.prev().hasClass('slide'))
				$('#prev').hide();
			$('.slide.active').fadeOut(function() {
				prev.fadeIn();
				$('.slide').removeClass('active');
				prev.addClass('active');
			});
		} 
		$('#finish').hide();
		$('#next').show();
	});
	$('#finish').click(function() {
		if(opt == 2) {
			var data = {};
			for(var i = 0; i < schedules.length; i++)
				data[schedules[i].id] = [];
			for(var i = 0; i < pool.length; i++) {
				var sid = $('input[name=p' + pool[i] + ']:checked').val();
				data[sid].push(pool[i]);
			}
			pool = JSON.stringify(data);
		}
		$.post('/events/assign', {algo: opt, 'event_id': eventID, problems: pool}, function(response) {
			try {
				var result = eval('(' + response + ')');
				if(result) {
					toastr.success('Problems have been assigned');
					window.location.reload();
				} else toastr.error('Something went wrong! Unable to assign problems.');
			} catch(err) {
				console.log(response);
				toastr.error('Something went wrong! Unable to assign problems.');
			}
		});
	});
	$('.tree').jstree({
		core: {
			themes: {
				stripes: true
			}
		},
		plugins: ['wholerow', 'sort', 'checkbox']
	});
	$('#set-size').change(function() {
		var val = $(this).val();
		if(val == numGroups)
			return;
		numGroups = val;
		$('.n').text(val);
		$('.group-checks').remove();
		for(var j = 0; j < val; j++)
			$('.groups tr.prob-row').each(function() {
				var pid = $(this).attr('data');
				$(this).append($('<td>')
						.addClass('group-checks')
						.append($('<input>')
							.attr('type', 'radio')
							.attr('name', 'prob-' + pid)
						)
				);
			});
		$('.groups tr.prob-row').each(function() {
			$(this).find('input').first().attr('checked', 'checked');
		});
	});
}

var numGroups = 1;
var eventID = null;
var pool = [];
var problems = [];
var nodes = [];
var opt = null;

function stage(index) {
	if(index == 1) {
		eventID = $('#sel-events').val();
	} else if(index == 2) {
		pool = [];
		var selected = $('.tree').jstree('get_selected', false);
		for(var i = 0; i < selected.length; i++) {
			var node = $('.tree').jstree('get_node', selected[i]);
			if(typeof node.data.jstree.data !== 'undefined'){
				pool.push(node.data.jstree.data.id);
				problems.push(node.data.jstree.data);
				nodes.push(selected[i]);
			}
		}
	} else if(index == 3) {
		opt = parseInt($('#slide3 input:checked').get(0).value);
		$('.opt').hide();
		$('#opt' + opt).show();
		if(opt == 1) {
			$('#opt1 h1').text($('#opt1 h1').text().replace('#p', pool.length));
		} else if(opt == 2) {
			schedules = $('#opt2').attr('data-schedules');
			var table = $('#opt2 table');
			try {
				schedules = eval('(' + schedules + ')');
				schedules = schedules[eventID];
				var tr = $('<tr>');
				tr.append($('<th>'));
				for(var i = 0; i < schedules.length; i++)
					tr.append($('<th>').addClass('text-center').text(schedules[i].reference));
				table.append(tr);
				for(var i = 0; i < pool.length; i++) {
					tr = $('<tr>');
					tr.append($('<th>').text(problems[i].category + ': P' + problems[i].id_p + 'V' + problems[i].id_v + 'D' + problems[i].id_d));
					for(var j = 0; j < schedules.length; j++)
						tr.append($('<td>').addClass('text-center asgn-prob').append($('<input>').attr({'type': 'radio', 'name': 'p' + pool[i], 'value': schedules[j].id})));
					table.append(tr);
				}
			} catch(err) {
				console.log(err);
			}
		}
		$('.groups').empty();
		/*$('.groups').append($('<tr>').addClass('head-row')
			.append($('<th>').text('Problem'))
			.append($('<th>').text('Group'))
			.append($('<th>').addClass('gid').text('G1'))
		);*/
		for(var i = 0; i < nodes.length; i++) {
			var node = $('.tree').jstree('get_node', nodes[i]);
			var parent = $('.tree').jstree('get_node', node.parent);
			var name = parent.text + ': ' + node.text;
			$('.groups').append(
				$('<tr>').attr('data', node.data.jstree.data).addClass('prob-row')
					.append($('<td>').text(name))
					.append($('<td>').addClass('group-id').text('Group 1'))
					.append($('<td>').addClass('group-checks').append($('<input>').attr('type', 'radio').attr('checked', 'checked').attr('name', 'prob-' + node.data.jstree.data)))
			);
		}
	} 
}

function summarize(events) {
	var grouped = {};
	for(var i = 0; i < events.length; i++) {
		if(typeof grouped[events[i].event_id] === 'undefined')
			grouped[events[i].event_id] = {
				name: events[i].name, 
				stopTime: new Date(events[i].time_stop),
				startTime: new Date(events[i].time_start),
				schedules: []};
		grouped[events[i].event_id].schedules.push(events[i]);
		if(grouped[events[i].event_id].stopTime < events[i].time_stop)
			grouped[events[i].event_id].stopTime = events[i].time_stop;
		if(grouped[events[i].event_id].startTime > events[i].time_start)
			grouped[events[i].event_id].startTime = events[i].time_start;
	}
	for(var eid in grouped) {
		var schedules = $('<ul>').addClass('list-group');
		for(var i = 0; i < grouped[eid].schedules.length; i++)
			schedules.append($('<li>').addClass('list-group-item')
				.append($('<h4>').text(grouped[eid].schedules[i].reference))
				.append($('<b>').text('Begins: '))
				.append($('<i>').text(new Date(grouped[eid].schedules[i].time_start).toLocaleString()))
				.append($('<br>'))
				.append($('<b>').text('Ends: '))
				.append($('<i>').text(new Date(grouped[eid].schedules[i].time_stop).toLocaleString()))
			);
		if(grouped[eid].stopTime > new Date()) {
			$('#upcoming').prepend($('<div>').addClass('panel panel-default ongoing')
				.append($('<div>').addClass('panel-body')
					.append($('<a>').addClass('pull-right btn btn-success').attr('href', '/dataviz/dash/' + eid).text('Dashboard'))
					.append($('<h3>').text(grouped[eid].name))
					.append($('<div>').addClass('clearfix'))
					.append($('<div>').addClass('asgn-stat').attr('data', eid))
					.append(schedules)
			));
		} else {
			$('#past').prepend($('<div>').addClass('panel panel-default')
				.append($('<div>').addClass('panel-body')
					.append($('<a>').addClass('pull-right btn btn-success').attr('href', '/dataviz/dash/' + eid).text('Dashboard'))
					.append($('<h3>').text(grouped[eid].name))
					.append($('<div>').addClass('clearfix'))
					.append($('<h4>').text('From: ' + grouped[eid].startTime.toLocaleDateString()))
					.append($('<h4>').text('To: ' + grouped[eid].stopTime.toLocaleDateString()))
					.append(schedules.hide())
			));
		}
	}
	$('.asgn-stat').each(function() {
		var eid = $(this).attr('data');
		var stat = $(this);
		$.getJSON('/events/status', {id: eid}, function(data) {
			var sections = '';
			for(var i = 0; i < data.length; i++) {
				sections = sections + ', ' + data[i];
			}
			stat.addClass('alert alert-info');
			if(data.length > 0) stat.text('Problems assigned to sections: ' + sections.substring(2) + '.');
			else stat.text('Problems have not been assigned for this event.');
		});
	});
}

function loadCalendar() {
	$.getJSON('/events/all', {}, function(events) {
		allEvents = events;
		summarize(events);
		var dataSet = [];
		for(var i = 0; i < events.length; i++) {
			dataSet.push({
				sid: events[i].schedule_id,
				title: events[i].name + ', ' + events[i].reference,
				start: events[i].time_start,
				end: events[i].time_stop
			});
		}
		$('#calendar').fullCalendar({
	        height: window.innerHeight - 60,
	        header: {
	        	left: 'title',
	        	center: 'basicDay,basicWeek,month',
	        	right: 'prev,next'
	        },
	        timezone: 'local',
	        selectable: true,
	        select: onSelect,
	        eventClick: onClick,
	        events: dataSet
	    });
		$('#btn-assign').removeAttr('disabled');
	});
}

function onClick(evt, js) {
	console.log(evt);
	//console.log(js);
	// Change or delete event.
	$('#modify .sid').val(evt.sid);
	$('#modify .title').text(evt.title);
	$('#modify').modal('show');
}

function onSelect(start, end) {
	console.log(start.format('MM/DD/YYYY'));
	$('.start').parent().data('DateTimePicker').setDate(start.format('MM/DD/YYYY'));
	$('.stop').parent().data('DateTimePicker').setDate(end.format('MM/DD/YYYY'));
	$('#btn-add').click();
}

function bindHandlers() {
	$('#btn-add').click(function() {
		createEvent();
	});
	$('#btn-assign').click(function() {
		$('#calendar').fadeOut(function() {
			$('#assignment').fadeIn();
		});
		$(this).hide();
		$('#btn-back').show();
	});
	$('#btn-back').click(function() {
		$('#assignment').fadeOut(function() {
			$('#calendar').fadeIn();
		});
		$(this).hide();
		$('#btn-assign').show();
	});
	$('#btn-del').click(function() {
		var sid = $('#modify .sid').val();
		$('#modify').modal('hide');
		alertify.confirm('Are you sure you want to delete this schedule? This cannot be undone. Please note that if this is the only schedule for its event,' 
				+ 'then the corresponding event will also be deleted.', function(e) {
			if(e) {
				$.post('/events/deleteschedule', {id: sid}, function(response) {
					try {
						var result = eval('(' + response + ')');
						if(result) {
							toastr.success('Schedule has been deleted.');
							window.location.reload();
						} else toastr.error('Something went wrong! Unable to delete schedule.');
					} catch(err) {
						console.log(response);
						toastr.error('Something went wrong! Unable to delete schedule.');
					}
				});
			}		
		});
	});
}

function createEvent() {
	$('#add').modal('show');
	$('#add .save').click(function() {
		
		// Schedule
		var reference = $('.sh-name').val();
		var start = new Date($('.start').val().trim()).toString();
		var stop = new Date($('.stop').val().trim()).toString();
		var sections = [];
		$('#input-sections input:checked').each(function() {
			sections.push($(this).val());
		});
		
		$('#add').modal('hide');
		$('#progress').modal('show');
		
		var evType = $('input[name=evt-type]:checked').val();
		
		if(evType === 'new') {
			// New event
			var name = $('.ev-name').val().trim();
			var type = $('#ev-type').val();
			$.post('/events/create', {
				name: name,
				type: type
			}, function(response) {
				console.log(response);
				try {
					var eid = eval('(' + response + ')');
					$.post('/events/schedule', {
						eid: eid,
						reference: reference,
						start_time: start,
						end_time: stop
					}, function(response) {
						console.log(response);
						try {
							var sid = eval('(' + response + ')');
							$.post('/events/slot', {sid: sid, sections: sections}, function(response) {
								console.log(response);
								try {
									response = eval('(' + response + ')');
									if(response) {
										toastr.success('Schedule created successfully!');
										window.location.reload();
									} else toastr.error('An error occurred! Could not assign slots.');
								} catch(err) {
									console.log(err);
									toastr.error('An error occurred! Could not assign slots.');
								}
							});
						} catch(err) {
							console.log(err);
							toastr.error('An error occurred! Could not schedule events.');
						}
					});
				} catch(err) {
					console.log(err);
					toastr.error('An error occurred! Could not create event.');
				}
			});
		} else {
			// Old event
			var eid = $('#ev-name').val();
			$.post('/events/schedule', {
				eid: eid,
				reference: reference,
				start_time: start,
				end_time: stop
			}, function(response) {
				try {
					var sid = eval('(' + response + ')');
					$.post('/events/slot', {sid: sid, sections: sections}, function(response) {
						try {
							response = eval('(' + response + ')');
							if(response) {
								toastr.success('Schedule created successfully!');
								window.location.reload();
							} else toastr.error('An error occurred! Could not assign slots.');
						} catch(err) {
							console.log(err);
							toastr.error('An error occurred! Could not assign slots.');
						}
					});
				} catch(err) {
					console.log(err);
					toastr.error('An error occurred! Could not schedule events.');
				}
			});
		}
	});
}

function addEvent(item, callback) {
	callback(null);
}

function removeEvent(item, callback) {
	// confirmation
	callback(item);
}

function moveEvent(item, callback) {
	// re-schedule
	callback(null);
}

function updateEvent(item, callback) {
	// assign problems
}