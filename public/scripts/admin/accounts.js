/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['listjs', 'bootstrap', 'icheck'], function(List) {
	
	load_css('icheck');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
		var students = new List('students', {
			valueNames: ['roll', 'email', 'name', 'section', 'auth']
		});
		require(['/vendor/jquery.dnd_page_scroll.js'], function() {
			$().dndPageScroll();
		});
	});
});

function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		$('#home').addClass('active');
		load_css('/styles/admin/accounts');
		$('.page-header').css({'margin-top': '0px'});
	});
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue'
	});
	$('.name,.email').val('');
	$('.user').css('cursor', 'move');
	var maxHeight = 0;
	var panels = $('.rolepanel');
	for(var i = 0; i < panels.length; i++) {
		if(panels.get(i).clientHeight > maxHeight) maxHeight = panels.get(i).clientHeight;
	}
}

function bindHandlers() {
	$('.add').click(function() {
		$(this).parent().find('.form').slideToggle(500);
	});
	$('.del').click(function() {
		var id = $(this).parent().parent().parent().attr('value');
		alertify.confirm('Are you sure you want to delete this admin user? This cannot be undone.', function(e) {
			if(e) {
				$.post('/accounts/delete', {id: id}, function(response) {
					window.location.reload();
				});
			}
		});
	});
	$('.form button').click(function() {
		var type = $(this).attr('value');
		var name = $(this).parent().parent().find('.name').val().trim();
		var email = $(this).parent().parent().find('.email').val().trim();
		var section = $(this).parent().parent().find('.section').val().trim();
		$.post('/accounts/create', {name: name, email: email, type: type, section: section}, function(response) {
			try {
				response = eval('(' + response + ')');
			} catch(err) {
				$('#error').modal('show');
				response = false;
			}
			if(response) window.location.reload();
		});
	});
	$('.user').each(function() {
		var user = $(this).get(0);
		user.ondragstart = function(e) {
			e.dataTransfer.setData('userid', e.target.id);
			this.style.opacity = 0.4;
		};
	});
	$('.rolepanel').each(function() {
		var panel = $(this).get(0);
		var usersection = $(this).find('.users');
		var jPanel = $(this);
		var role = $(this).attr('value');
		panel.ondragenter = function() {
			jPanel.addClass('dragenter');
		};
		panel.ondragleave = function() {
			jPanel.removeClass('dragenter');
		};
		panel.ondragend = function() {
			jPanel.removeClass('dragenter');
		};
		panel.ondragover = function(e) {
			e.preventDefault();
			e.dataTransfer.dropEffect = 'move'; 
			jPanel.addClass('dragenter');
		};
		panel.ondrop = function(e) {
			e.preventDefault();
			var user = e.dataTransfer.getData('userid');
			if($('#' + user).hasClass(role)) {
				$('#' + user).css('opacity', '1.0');
				return;
			}
			var id = $('#' + user).attr('value');
			$.post('/accounts/modify', {id: id, role: jPanel.attr('value')}, function(response) {
				try {
					response = eval('(' + response + ')');
					if(response) {
						usersection.append($('#' + user));
						$('#' + user).css('opacity', '1.0');
						for(var c = 0; c < 3; c++) $('#' + user).removeClass(c+'');
						$('#' + user).addClass(role);
						toastr.success('Role changed for user.');
					} else toastr.error('An error occurred. Could not change roles for this user.');
				} catch(err) {
					console.log(err);
					toastr.error('An error occurred. Could not change roles for this user.');
				}
			});
		};
	});
	$('.del-student').click(function(e) {
		e.preventDefault();
		var uid = $(this).attr('data');
		alertify.confirm('Are you sure you want to delete this student? This cannot be undone.', function(c) {
			if(c) {
				$.post('/accounts/student/delete', {id: uid}, function(response) {
					try {
						var result = eval('(' + response + ')');
						if(result) window.location.reload();
						else toastr.error('Something went wrong! Could not delete student user.');
					} catch(err) {
						console.log(err);
						toastr.error('Something went wrong! Could not delete student user.');
					}
				});
			}
		});
	});
	$('.edit-student').click(function(e) {
		e.preventDefault();
		var data = eval('(' + $(this).attr('data') + ')');
		$('#edit-id').val(data.id);
		$('#edit-email').val(data.email);
		$('#edit-roll').val(data.roll);
		$('#edit-name').val(data.name);
		$('#edit-section').val(data.section);
		if(data.auth_type === 0) {
			$('input[type=radio]').first().iCheck('check');
		} else {
			$('input[type=radio]').last().iCheck('check');
		}
		$('#edit-dialog').modal('show');
	});
	$('input[type=radio]').first().on('ifToggled', function(event) {
		var auth = event.target.checked ? 0 : 1;
		$('#edit-auth').val(auth);
	});
	$('#edit-dialog .ok').click(function() {
		$('#edit-dialog').modal('hide');
		$.post('/accounts/student/update', {
			id: $('#edit-id').val(),
			email: $('#edit-email').val().trim(),
			roll: $('#edit-roll').val().trim(),
			name: $('#edit-name').val().trim(),
			section: $('#edit-section').val().trim(),
			auth: $('#edit-auth').val()
		}, function(response) {
			try {
				var result = eval('(' + response + ')');
				if(result) window.location.reload();
				else toastr.error('Something went wrong! Could not save student details.');
			} catch(err) {
				console.log(err);
				toastr.error('Something went wrong! Could not save student details.');
			}
		});
	});
	$('#add-student').click(function(e) {
		e.preventDefault();
		var email = $('#student-email').val().trim();
		$.post('/accounts/student/create', {email: email}, function(response) {
			if(response) window.location.reload();
			else toastr.error('Something went wrong! Could not add student user.');
		});
	});
	$('.pwd-reset').click(function(e) {
		e.preventDefault();
		var id = $(this).attr('data-id');
		alertify.confirm('Are you sure you want to initiate a password reset for this user? The user will not be able to login until the password is reset.', function(e) {
			if(e) {
				$.post('/accounts/initresetpwd', {id: id}, function(response) {
					if(response) window.location.reload();
					else toastr.error('An error occurred! Could not initiate password reset.');
				});
			}
		});
	});
}