/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var tinycolor = null;
var offset = 0;

define(['tinycolor', 'bootstrap', 'd3', 'nvd3', 'icheck'], function(tc) {
	
	tinycolor = tc;
	
	load_css('/styles/loader');
	load_css('nvd3');
	load_css('icheck');
	load_css('ionicons');
	load_css('/styles/admin/data');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
		plotAssignmentDistribution();
		window.onresize = function() {
			stylize();
		};
	});
});

function getLoader(text) {

	var child = $('<div>').addClass('gmb-loader');
	child.append($('<div>').css('background-color', '#428BCA'));
	child.append($('<div>').css('background-color', '#428BCA'));
	child.append($('<div>').css('background-color', '#428BCA'));
	
	var loader = $('<h2>').attr('align', 'center').addClass('loader');
	loader.append(child);
	loader.append($('<br>'));
	loader.append($('<span>').text(text));
	
	return loader;
} 

function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		$('#dataviz').addClass('active');
	});
	$('.sidebar').css('height', window.innerHeight + 'px');
}

function bindHandlers() {
	$('#link-ad').click(function(e) {
		e.preventDefault();
		$('.sidemenu li').removeClass('active');
		$(this).parent().addClass('active');
		$('#viewer').hide();
		$('.chart').hide();
		plotAssignmentDistribution();
	});
	$('#link-cq').click(function(e) {
		e.preventDefault();
		$('.sidemenu li').removeClass('active');
		$(this).parent().addClass('active');
		$('#viewer').hide();
		$('.chart').hide();
		plotQuality();
	});
}

function plotQuality() {
	$('#progress').modal('show');
	$.getJSON('/dataviz/quality', {}, function(response) {
		console.log(response);
		var dataset = [];
		for(var i = 0; i < response.length; i++) {
			var datum = response[i];
			if(datum.success + datum.failure == 0) continue;
			var failureRate = datum.failure / (datum.success + datum.failure);
			var H = parseInt(1 + (1 - failureRate) * 90);
			var color = tinycolor({h: H, s: 200, v: 198});
			dataset.push({
				key: datum.user_id,
				values: [{
					x: datum.success,
					y: datum.failure,
					size: parseInt(failureRate * 100),
					color: color.toRgbString()
				}] 
			});
		}
		$('#chart-cq svg').html('');
		nv.addGraph(function() {
			var chart = nv.models.scatterChart()
	            .showDistX(true)
	            .showDistY(true)
	            .showLegend(false)
	            .transitionDuration(350)
	            .color(d3.scale.category10().range());
			chart.xAxis
			    .axisLabel('Compilation Success');
			chart.yAxis
		    	.axisLabel('Compilation Failure');
			chart.tooltipContent(function(key) {
				return '<h4>User#' + key + '</h4>';
		    });
			chart.xAxis.tickFormat(d3.format('d'));
			chart.yAxis.tickFormat(d3.format('d'));
			d3.select('#chart-cq svg')
				.attr('height', window.innerHeight - 90 + 'px')
			    .datum(dataset)
			    .call(chart);
			nv.utils.windowResize(chart.update);
			return chart;
		});
		$('#chart-cq').show();
		$('#progress').modal('hide');
	});
}

function plotAssignmentDistribution() {
	$('#chart-pd svg').html('');
	$('#chart-pd').show();
	$.getJSON('/dataviz/assignments', {}, function(response) {
		$('#viewer').html('');
		var events = [];
		for(var i = 0; i < response.length; i++) {
			var item = response[i];
			if(typeof events[item.event_id] === 'undefined') events[item.event_id] = [];
			events[item.event_id].push(item);
		}
		var dataset = [];
		for(var eid in events) {
			var dist = [];
			for(var i = 0; i < events[eid].length; i++) {
				var event = events[eid][i];
				dist.push({x: i, y: event.assigned_to});
			}
			dataset.push({
				values: dist,
				key: events[eid][0].event,
				area: true
			});
		}
		nv.addGraph(function() {
			var chart = nv.models.lineChart()
				.margin({left: 100})
				.useInteractiveGuideline(true)
				.transitionDuration(350)
				.showLegend(true)
				.showYAxis(true)
				.showXAxis(true);
			chart.xAxis
			    .axisLabel('Problem');
			chart.yAxis
		    	.axisLabel('Assignments');
			d3.select('#chart-pd svg')
				.attr('height', window.innerHeight - 90 + 'px')
				.datum(dataset)
				.call(chart);
			nv.utils.windowResize(function() { chart.update() });
			return chart;
		});
	});
}