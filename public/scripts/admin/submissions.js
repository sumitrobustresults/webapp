/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap', 'icheck', 'd3'], function() {
	
	load_css('icheck');
	load_css('/styles/loader');
	load_css('/styles/admin/submissions');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
		$('#event-menu a').first().click();
	});
});

function stylize() {
	
	require(['../scripts/admin/styles/default'], function() {
		$('#codeset').addClass('active');
	});
	
	$('input')
	.iCheck({
		checkboxClass: 'icheckbox_square-blue'
	});
}

function bindHandlers() {
	$('.link-schedule').click(function(e) {
		
		e.preventDefault();
		
		var scheduleID = $(this).attr('value');
		var eventID = $(this).attr('data-event');
		
		$('#evt' + eventID + ' .link-schedule').each(function() { $(this).parent().removeClass('active'); });
		$(this).parent().addClass('active');
		
		loadSubmissions(scheduleID, eventID);
	});
	
	$('input[type=checkbox]').each(function() {
		var eventID = $(this).attr('data-event');
		
		if($(this).hasClass('filter-graded')) {
			$(this)
			.on('ifChecked', function(event) {
				var id = event.target.id;
				$('#evt' + eventID + ' .gradpill').show();
			})
			.on('ifUnchecked', function(event) {
				$('#evt' + eventID + ' .gradpill').hide();
			});
		} else if($(this).hasClass('filter-not-graded')) {
			$(this)
			.on('ifChecked', function(event) {
				var id = event.target.id;
				$('#evt' + eventID + ' .subpill').show();
			})
			.on('ifUnchecked', function(event) {
				$('#evt' + eventID + ' .subpill').hide();
			});
		} else if($(this).hasClass('filter-submitted')) {
			$(this)
			.on('ifChecked', function(event) {
				var id = event.target.id;
				$('#evt' + eventID + ' .submitted').show();
			})
			.on('ifUnchecked', function(event) {
				$('#evt' + eventID + ' .submitted').hide();
			});
		} else if($(this).hasClass('filter-not-submitted')) {
			$(this)
			.on('ifChecked', function(event) {
				var id = event.target.id;
				$('#evt' + eventID + ' .not-submitted').show();
			})
			.on('ifUnchecked', function(event) {
				$('#evt' + eventID + ' .not-submitted').hide();
			});
		}
	});
}

var offsets = {};

function loadSubmissions(scheduleID, eventID) {
	
	$('#progress').modal('show');
	
	$.getJSON('/dataviz/submissions', {schedule_id: scheduleID, offset: 0}, function(response) {
		
		$('#evt' + eventID + ' .submissions').empty();
		
		var table = $('<table>').addClass('table table-condensed table-hover');
		table.append($('<tr>')
			.append($('<th>').text('Assignment'))
			.append($('<th>').text('Question'))
			.append($('<th>').text('Submitted'))
			.append($('<th>').text('Graded'))
		);
		$('#evt' + eventID + ' .submissions').append(table);
		
		console.log(response);
		
		for(var i = 0; i < response.length; i++) {
			var submission = response[i];
			addToSubmissionsTable(eventID, submission);
		}
		
		$('#evt' + eventID + ' .filter-pane').show();
		$('#evt' + eventID + ' input').iCheck('check');
		
		offsets[scheduleID] = 0;
		
		$('#evt' + eventID + ' .more')
		.show()
		.click(function(e) {
			e.preventDefault();
			loadMoreSubmissions(eventID, scheduleID);
		});
		
		$('#progress').modal('hide');
	});
}

function loadMoreSubmissions(eventID, scheduleID) {
	
	var offset = offsets[scheduleID] + 250;
	offsets[scheduleID] = offset;
	
	$('#evt' + eventID + ' .more').hide();
	$('#evt' + eventID + ' .more-loader').show();
	
	$.getJSON('/dataviz/submissions', {schedule_id: scheduleID, offset: offset}, function(response) {
		
		if(response.length == 0) {
			$('#evt' + eventID + ' .more')
				.text("----That's All!----")
				.removeAttr('href')
				.off();
		}
		
		for(var i = 0; i < response.length; i++) {
			var submission = response[i];
			addToSubmissionsTable(eventID, submission);
		}
		
		$('#evt' + eventID + ' .more-loader').hide(500);
		$('#evt' + eventID + ' .more').show();
	});
}

function addToSubmissionsTable(eventID, submission) {
	var table = $('#evt' + eventID + ' .submissions table');
	var row = $('<tr>')
		.addClass((submission.grading_time !== null) ? 'success' : '')
		.addClass((submission.grading_time !== null) ? 'gradpill' : 'subpill')
		.addClass((submission.is_submitted == 1) ? 'submitted' : 'not-submitted');
	row.append($('<td>').append($('<a>')
		.text('#' + submission.id)
		.attr('href', '/admin/viewer/' + submission.id)
		.attr('target', '_blank'))
	);
	row.append($('<td>').text('Q' + submission.question));
	row.append($('<td>')
		.text((submission.is_submitted == 1) ? 'submitted' : 'not-submitted'));
	row.append($('<td>')
		.text((submission.grading_time !== null) ? 'graded' : 'not-graded'));
	table.append(row);
}