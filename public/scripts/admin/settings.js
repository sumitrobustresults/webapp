/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	load_css('/styles/admin');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
	});
});

function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		//
	});
	$('.deco-text').css({
		'border': 'none',
		'border-radius': '5px',
		'font-size': '24px',
		'padding': '10px',
		'margin': '5px',
		'background-color': '#F5F5F5'
	});
}

function bindHandlers() {
	$('#btn-username').click(function() {
		var name = $('#user-name').val().trim();
		if(name == '') {
			toastr.error('Please enter your name.');
			return;
		}
		$.post('/accounts/name', {name: name}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) {
					toastr.success('Your name has been updated.');
					window.location.reload();
				} else toastr.error('Your name could not be updated.');
			} catch(err) {
				toastr.error('An error occurred. Could not update your name.');
			}
		});
	});
	$('#btn-password').click(function() {
		var old = $('#user-oldpass').val();
		var password = $('#user-newpass').val();
		var confirm = $('#user-repass').val();
		if(password !== confirm) {
			toastr.error('Entered passwords do not match!');
			return;
		}
		$.post('/accounts/password', {oldpass: old, newpass: password}, function(response) {
			try {
				response = eval('(' + response + ')');
				if(response) toastr.success('Password changed successfully!');
				else toastr.error('Your password could not be changed.');
			} catch(err) {
				toastr.error('An error occurred. Could not update your password.');
			}
		});
	});
}