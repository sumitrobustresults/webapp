/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap', 'jstree'], function() {
	
	load_css('jstree');
	load_css('/styles/admin/tasks');
	load_css('/styles/loader');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
	});
});

function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		$('#home').addClass('active');
	});
	$('.col-md-3').css({
		'font-size': '30px'
	});
	$('.col-md-9').css({
		'min-height': (window.innerHeight - 180) + 'px',
		'border-left': 'solid',
		'border-width': '1px',
		'border-color': '#428BCA'
	});
	
	$('#menu a, #sub').css({
		'border-radius': '0px'
	});
}

function bindHandlers() {
	
	$('#menu a').click(function() {
		if($(this).hasClass('status')) {
			$('#sub').slideDown(350);
		} else {
			$('#sub').hide();
		}
	});
	
	$('#events a').click(function(e) {
		e.preventDefault();
		$('#events li').removeClass('active');
		$(this).parent().addClass('active');
		var eventID = $(this).attr('data');
		$('#progress').modal('show');
		$.getJSON('/tasks/status', {id: eventID}, function(data) {
			populate(data);
			$('#progress').modal('hide');
		});
	});
}

function populate(list) {
	
	var pending = {};
	
	var numPending = 0;
	var numComplete = 0;
	
	for(var i = 0; i < list.length; i++) {
		if(typeof pending[list[i].assignee] === 'undefined') 
			pending[list[i].assignee] = {name: list[i].name, email: list[i].email, complete: [], pending: []};
		if(list[i].is_complete == 1) {
			pending[list[i].assignee].complete.push(list[i]);
			numComplete++;
		} else {
			pending[list[i].assignee].pending.push(list[i]);
			numPending++;
		}
	}
	
	$('#overall').html('');
	$('#overall')
		.append($('<b>').text(numPending))
		.append($('<span>').text(' out of '))
		.append($('<b>').text(numPending + numComplete))
		.append($('<span>').text(' tasks pending.'));
	
	// Sort pending tasks.
	var sorted = [];
	for(var uid in pending) {
		var i = sorted.length - 1;
		if(i < 0) {
			sorted.push(uid);
			continue;
		}
		
		while(i >= 0) {
			var val_new = pending[uid].pending.length;
			var val_old = pending[sorted[i]].pending.length;
			if(val_old >= val_new) break;
			i--;
		}
		for(var j = sorted.length; j > i + 1; j--) {
			sorted[j] = sorted[j - 1];
		}
		sorted[i + 1] = uid;
	}
	
	// Build tree structure.
	var data = [];
	for(var i = 0; i < sorted.length; i++) {
		var tasks = [];
		var left = pending[sorted[i]].pending;
		var done = pending[sorted[i]].complete;
		for(var x = 0; x < left.length; x++) {
			tasks.push({
				data: left[x].reference,
				text: '#' + left[x].reference,
				icon: 'glyphicon glyphicon-exclamation-sign',
				li_attr: {'class': 'pending'}
			});
		}
		for(var x = 0; x < done.length; x++) {
			tasks.push({
				text: '#' + done[x].reference,
				data: done[x].reference,
				icon: 'glyphicon glyphicon-ok-sign',
				li_attr: {'class': 'complete'}
			});
		}
		var icon = (pending[sorted[i]].pending.length > 0) ? 'glyphicon glyphicon-exclamation-sign' : 'glyphicon glyphicon-ok-sign';
		var userStatus = (pending[sorted[i]].pending.length > 0) ? 'pending' : 'complete';
		data.push({
			text: '<b>(' + pending[sorted[i]].pending.length + ' pending)</b> ' + pending[sorted[i]].name + ' [' + pending[sorted[i]].email + ']',
			icon: icon,
			children: tasks,
			data: null,
			li_attr: {'class': userStatus}
		});
	}
	
	$('.tree').jstree('destroy');
	$('.tree').empty();
	
	$('.tree').jstree({
		core: {
			data: data,
			themes: {
				stripes: true
			}
		},
		plugins: ['wholerow']
	}).on('select_node.jstree', function(e, sel) {
		var id = sel.node.data;
		if(id === null) return;
		window.location.href = '/admin/viewer/' + id;
	});
}