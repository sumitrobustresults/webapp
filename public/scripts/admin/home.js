/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap', 'modernizr', 'gridster'], function() {
	
	load_css('gridster');
	
	$(document).ready(function() {
		
		var containerWidth = $('.gridster')[0].clientWidth - 200;
		var containerHeight = window.innerHeight - 120;
		
		adapt(containerWidth, containerHeight);
		
		stylize();
	});
});

function adapt(containerWidth, containerHeight) {
	
	var unitWidth = parseInt(containerWidth / 6);
	var unitHeight = parseInt(containerHeight / 2);
	
	var minHeight = 250;
	
	unitHeight = (unitHeight > minHeight) ? unitHeight : minHeight;
	
	if(isMobile) {
		$('.widget').each(function(index) {
			$(this).attr({
				'data-row': index + 1,
				'data-col': 1,
				'data-sizex': 1,
				'data-sizey': 1
			});
		});
		unitWidth = containerWidth + 200;
	}
	
	$('.gridster ul').gridster({
		widget_selector: '.widget',
		widget_margins: [10, 10],
		widget_base_dimensions: [unitWidth, unitHeight],
		resize: {
			enabled: !isMobile
		}
	});
}

function stylize() {
	require(['../scripts/admin/styles/default'], function() {
		$('#home').addClass('active');
		$('.widget').css({
			'background-color': '#2A6496',
			'color': '#FFFFFF',
			'padding': '10px',
			'cursor': 'move',
			'text-align': 'center'
		});
		$('.gridster .icon').css({
			'font-size': '90px'
		});
		$('.widget a').css({
			'color': '#8CC7F7'
		});
	});
}