/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	load_css('/styles/loader');
	
	$(document).ready(function() {
		require(['../scripts/admin/styles/default'], function() {
			//$('#jobs').addClass('active');
			$('td,th').css({'padding': '5px'});
			refreshEventStatus(eventID);
			if($('.time-taken').length > 0) {
				require(['moment'], function(moment) {
					var start = $('.time-taken').attr('data-start');
					var stop = $('.time-taken').attr('data-stop');
					var duration = moment.duration(moment(stop).diff(moment(start)) , 'milliseconds');
					$('.time-taken').text(duration.humanize());
				});
			}
		});
		
		$('#fetch-summary').click(function(e) {
			e.preventDefault();
			$('.fetch').hide();
			$('.summary-loader').show();
			getSummary();
		});
		
		$('#btn-clean').click(function() {
			alertify.confirm('Clearing a job implies that the all data related to this job will be deleted. It will seem that the job never happened. Are you sure you want to do this?', function(ok) {
				if(ok) {
					$.post('/job', {type: 'clean_memoization', event_id: eventID}, function(response) {
						try {
							var result = eval('(' + response + ')');
							if(result) {
								window.location.reload();
							} else {
								toastr.error('Something went wrong! Unable to clear this job.');
							}
						} catch(err) {
							console.log(response);
							toastr.error('Something went wrong! Unable to clear this job.');
						}
					});
				} else {
					toastr.info('This job has not been cleared. No changes have been made.');
				}
			});
		});
	});
});

function refreshEventStatus(eventID) {
	$.post('/job', {type: 'event_status', event_id: eventID}, function(response) {
		//console.log(response);
		try {
			response = eval('(' + response + ')');
			var job = response.job;
			var rate = response.rate;
			var total = response.total;
			var pending = response.pending;
			var complete = response.complete;
			var gears = response.gears;
			var percentComplete = parseInt(complete * 100 / (pending + complete));
			var etr = parseInt(pending / rate);
			var status = '';
			if(!response.initiated) status = 'not started';
			if(response.initiated && response.running) status = 'running';
			if(response.initiated && !response.running) status = 'paused';
			if(!response.initiated && complete > 0) status = 'stopped';
			if(pending == 0) status = 'complete';
			
			if(!response.running) {
				if(status === 'paused') $('.current-icon').addClass('ion-pause');
				else if(status === 'stopped') $('.current-icon').addClass('ion-stop');
				else if(status === 'complete') $('.current-icon').addClass('ion-android-checkmark');
				$('.progress-value, .progress-label').text('');
			} else {
				$('.current-icon').addClass('ion-play');
				$('.running-stats h3').html('');
				for(var i = 0; i < gears.length; i++) {
					$('.running-stats h3').append($('<span>')
						.addClass('icon')
						.addClass((gears[i].running) ? 'ion-loading-b' : 'ion-load-b')
					);
				}
				$('.progress-label').text('Estimated Time Remaining: ');
				require(['moment'], function(moment) {
					var duration = moment.duration(etr, 'seconds');
					if(status !== 'paused') $('.progress-value').text(duration.humanize());
				});
			}
			
			$('.current-status').text(status);
			$('.percent').text(percentComplete + '%');
			$('.progress-bar').css({'width': percentComplete + '%'});
			$('.processed').text(total);
			$('.pending').text(pending);
			
			if(status === 'complete') {
				$('.wait').hide();
			}
		} catch(err) {
			console.log(err);
			toastr.error('Unable to get memoization status for event.');
		}
		
		if(status !== 'complete') setTimeout('refreshEventStatus(' + eventID + ')', 10000);
	});
}

function getSummary() {
	$.post('/job', {type: 'check_consistency', event_id: eventID}, function(response) {
		try {
			response = eval('(' + response + ')');
			var assignments = response.assignments;
			var units = response.units;
			
			$('.num-complete').text(assignments.complete);
			$('.num-partial').text(assignments.partial);
			$('.num-notcompile').text(assignments.notcompiled);
			$('.num-defective').text(assignments.defective);
			$('.num-nil').text(assignments.pending);
			$('.num-total').text(assignments.total);
			
			$('.succeed-memoize').text(units.complete);
			$('.fail-save').text(units.database_fail);
			$('.fail-error').text(units.system_fail);
			$('.fail-compile').text(units.compilation_fail);
			$('.fail-unknown').text(units.unaccounted);
			$('.expected-units').text(units.total);
			
			$('.summary-loader').hide();
			$('.summary').show();
		} catch(err) {
			toastr.error('Something went wrong! Could not fetch summary.');
		}
	});
}