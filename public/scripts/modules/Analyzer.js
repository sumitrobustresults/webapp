/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var keywordRegex = /^(char|auto|break|case|char|const|continue|default|do|double|else|enum|extern|float|for|goto|if|int|long|register|return|short|signed|sizeof|static|struct|switch|typedef|union|unsigned|void|volatile|while)$/;
var keywordList = ['char','auto','break','case','char','const','continue','default','do','double','else','enum','extern','float','for','goto','if','int','long','register','return','short','signed','sizeof','static','struct','switch','typedef','union','unsigned','void','volatile','while'];
var knownDefs = ['main', 'printf', 'scanf', 'sqrt', 'pow'];

var source = null;
var tokens = null;
var stream = null;
var commentList = null;
var definitions = null;
var identifiers = null;

var Levenshtein = null;

var analyzer = {};

function init(code, tokenObj) {
	
	source = code;
	tokens = tokenObj.tokens;
	stream = tokenObj.stream;
	
	return analyzer;
}

function removeComments() {
	
	stream = stream.replace(/#[0-9]+#COMMENT[ ]*/g, '');
	var cor = [];
	
	for(var i = 0; i < tokens.length; i++) {
		var token = tokens[i];
		if(token[0].indexOf('COMMENT') !== 0)
			continue;
		var len = parseInt(token[2]);
		var comment = Base64.decode(token[3]);
		var pos = source.indexOf(comment);
		cor.push([pos, comment]);
		source = source.replace(comment, generateSpaces(len));
	}
	
	commentList = cor;
	
	return commentList;
}

function restoreComments(edits) {
	
	for(var i = 0; i < edits.length; i++) {
		var loc = edits[i][0];
		var diff = edits[i][1];
		for(var c = 0; c < commentList.length; c++) {
            if(commentList[c][0] > loc)
                commentList[c][0] += diff;
        }
	}
	
	for(var c = 0; c < commentList.length; c++) {
	    var clen = commentList[c][1].length;
	    var pos = commentList[c][0];
	    source = source.substring(0, pos) + commentList[c][1] + source.substring(pos + clen);
	}
	
	return source;
}

function refactorDataTypes() {
	
	var reg = "(#[0-9]+#LONG #[0-9]+#LONG|#[0-9]+#LONG #[0-9]+#INT|#[0-9]+#LONG #[0-9]+#DOUBLE|#[0-9]+#LONG #[0-9]+#LONG #[0-9]+#INT|" +
			"#[0-9]+#UNSIGNED #[0-9]+#INT|#[0-9]+#UNSIGNED #[0-9]+#SHORT|#[0-9]+#UNSIGNED #[0-9]+#CHAR|#[0-9]+#UNSIGNED #[0-9]+#LONG|" +
			"#[0-9]+#UNSIGNED #[0-9]+#LONG #[0-9]+#LONG|#[0-9]+#UNSIGNED #[0-9]+#LONG #[0-9]+#INT|#[0-9]+#UNSIGNED #[0-9]+#LONG #[0-9]+#LONG #[0-9]+#INT)";
	
	stream = stream.replace(RegExp(reg, "g"), 'DATA_TYPE');
	stream = stream.replace(/(UNSIGNED|INT|FLOAT|DOUBLE|CHAR|SHORT|LONG|VOID)/g, 'DATA_TYPE');
	
	return stream;
}

function createDefinitionTable() {
	
	refactorDataTypes();
	
	var regex = /#[0-9]+#IDENTIFIER/g;
	var defreg = /#[0-9]+#DATA_TYPE #[0-9]+#IDENTIFIER/g;
	var numbers = /#[0-9]+#/g;
	
	var decs = stream.match(defreg);
	if(!decs) decs = [];
	
	var decMap = {};
	
	for(var i = 0; i< decs.length; i++) {
	    var nums = decs[i].match(numbers);
	    for(var j = 0; j < nums.length; j++)
	        nums[j] = parseInt(nums[j].substring(1, nums[j].length - 1));
	    var type = Base64.decode(tokens[nums[0]][3]);
	    for(var j = nums[1]; j < tokens.length; j++) {
	        if(tokens[j][0].indexOf('STM_DELIM') >= 0 || tokens[j][0].indexOf('L_PAREN') >= 0)
	            break;
	        else if(tokens[j][0].indexOf('IDENTIFIER') >= 0)
	            decMap[Base64.decode(tokens[j][3])] = type;
	    }
	}
	
	definitions = decMap;
	
	var ids = stream.match(regex);
	if(!ids) ids = [];
	
	var strayIds = [];
	var idMap = {};
	
	for(var i = 0; i < ids.length; i++) {
	    var nums = ids[i].match(numbers);
	    for(var j = 0; j < nums.length; j++)
	        nums[j] = parseInt(nums[j].substring(1, nums[j].length - 1));
	    var id = Base64.decode(tokens[nums[0]][3]);
	    if(typeof decMap[id] === 'undefined' && knownDefs.indexOf(id) == -1)
	        strayIds.push(id);
	    if(typeof idMap[id] === 'undefined')
	    	idMap[id] = [];
	    idMap[id].push(nums[0]);
	}
	
	identifiers = idMap;
	
	return definitions;
}

function getDefinitionsTable() {
	
	return definitions;
	
}

function executeRepairRule(ruleContents, callback) {
	
	try {
		var rule = eval('(' + ruleContents + ')');
		var result = rule();
		callback(null, result);
	} catch(err) {
		//console.log(err);
		callback(err, null);
	}
}

define(['levenshtein', 'base64'], function(lev) {
	
	Levenshtein = lev;
	
	analyzer.createDefinitionTable = createDefinitionTable;
	analyzer.getDefinitionsTable = getDefinitionsTable;
	analyzer.removeComments = removeComments;
	analyzer.refactorDataTypes = refactorDataTypes;
	analyzer.restoreComments = restoreComments;
	analyzer.executeRepairRule = executeRepairRule;
	
	return init;
});