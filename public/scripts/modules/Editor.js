/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['ace'], function(editor) {
	
	var showCompilerErrors = function(editor, errors) {
		
		if(errors === null)
			return;
		
		var lines = [];
		
		for(var index = 0; index < errors.length; index++) {
			var error = errors[index];
			var line = parseInt(error['line']);
			
			var msg = arrange_lines(error['feedback'].trim());
			
			lines.push({
				row: line-1,
				column: 1,
				text: msg,
				type: error['type']
			});
		}
		
		editor.getSession().setAnnotations(lines);
	};
	
	return {
		showCompilerErrors : showCompilerErrors
	};
});