/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	load_css('/styles/theme');
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
	});
});

function stylize() {
	$('body').css({
		'background-color': '#F8F8F8',
		'color': '#777777'
	});
	$('.container').css({
		'min-height': window.innerHeight - 50
	});
}

function bindHandlers() {
	$('.replies').click(function(e) {
		e.preventDefault();
		$(this).hide();
		var id = $(this).attr('data');
		var channel = $(this).parent().find('.channel');
		$.getJSON('/pager/messages', {'thread_id': id}, function(data) {
			for(var i = 0; i < data.length; i++) {
				var date = new Date(data[i].create_time);
				var person = (data[i].composer === me) ? 'Me' : data[i].name;
				var msg = $('<div>')
					.attr('id', 'msg-' + data[i].id)
					.addClass('alert alert-info')
					.append($('<em>').text('"' + data[i].contents + '"'))
					.append($('<hr>'));
				if(data[i].composer === me)
					msg.append(
							$('<a>')
							.attr('href', '')
							.attr('data', data[i].id)
							.addClass('pull-right text-danger remove-reply')
							.append($('<span>').addClass('glyphicon glyphicon-trash'))
							.click(function(e) {
								e.preventDefault();
								var id = $(this).attr('data');
								alertify.confirm('Are you sure you want to delete this reply? This cannot be undone.', function(e) {
									if(e) {
										$.post('/pager/remove', {id: id}, function(response) {
											try {
												var result = eval('(' + response + ')');
												if(result) {
													$('#msg-' + id).remove();
													toastr.success('Your reply has been removed.');
												} else toastr.error('Something went wrong! Unable to delete your message.');
											} catch(err) {
												toastr.error('Something went wrong! Unable to delete your message.');
											}
										});
									}
								});
							})
					);
				msg
					.append($('<strong>').text('by ' + person))
					.append($('<h5>').text('on ' + date.toLocaleDateString() + ' at ' + date.toLocaleTimeString()));
				channel.append(msg);
			}
		});
	});
	
	$('.reply').click(function(e) {
		e.preventDefault();
		$(this).hide();
		$(this).parent().parent().find('.reply-form').slideDown();
	});
	
	$('.reply-form button').click(function() {
		$(this).attr('disabled', 'disabled');
		var button = $(this);
		var threadID = $(this).attr('data');
		var message = $(this).parent().parent().find('textarea').val().trim();
		var channel = $(this).parent().parent().parent().find('.channel'); 
		var form = $(this).parent().parent();
		if(message === '') return;
		$.post('/pager/respond', {id: threadID, message: message}, function(response) {
			try {
				var result = eval('(' + response + ')');
				var date = new Date();
				channel.prepend(
					$('<div>')
						.addClass('alert alert-info')
						.append($('<em>').text('"' + message + '"'))
						.append($('<hr>'))
						.append($('<strong>').text('by Me'))
						.append($('<h5>').text('on ' + date.toLocaleDateString() + ' at ' + date.toLocaleTimeString()))
				).hide().fadeIn();
				form.hide();
				form.find('textarea').val('');
				form.parent().find('.reply').show();
			} catch(err) {
				toastr.error('An error occurred! Could not post reply.');
			} finally {
				button.removeAttr('disabled');
			}
		});
	});
	
	$('.expand-thread').click(function(e) {
		e.preventDefault();
		var msg = $(this).parent().attr('data');
		$(this).parent().text(msg).hide().fadeIn();
		$(this).hide();
	});
}