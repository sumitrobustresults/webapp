/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['bootstrap'], function() {
	
	load_css('/styles/loader');
	
	$(document).ready(function() {
		stylize();
		bind_handlers();
		window.onresize = function() {
			stylize();
		};
	});
});

function getLoader(text) {

	var child = $('<div>').addClass('gmb-loader');
	child.append($('<div>'));
	child.append($('<div>'));
	child.append($('<div>'));
	
	var loader = $('<h2>').attr('align', 'center').addClass('loader');
	loader.append(child);
	loader.append($('<br>'));
	loader.append($('<span>').text(text));
	
	return loader;
} 

function stylize() {
	if(isMobile) {
		$('footer').css({
			position: 'relative'
		});
	}
}

function bind_handlers() {
	$('#btn-submit').click(function(e) {
		e.preventDefault();
		var username = $('form').find('input[type=email]').val();
		var password = $('form').find('input[type=password]').val();
		$('.alert').remove();
		$('#btn-submit').hide();
		$('#btn-submit').parent().append(getLoader('logging you in'));
		$.post('/accounts/login', {username: username, password: password}, function(response) {
			console.log(response);
			try {
				response = eval('(' + response + ')');
				if(response) {
					require(['jstorage'], function() {
						$.jStorage.set('active', true);
					});
					window.location.reload();
				} else {
					$('.loader').remove();
					$('#btn-submit').show();
					alertify.alert('Login Failed! You may have entered invalid credentials.');
				}
			} catch(err) {
				$('#error').modal('show');
			}
		});
	});
}