/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

// Displays the results of program compilation.
function showCompilationResult(result) {
	
	var status = result['result'];
	var feedback = result['checks'];
	
	if(feedback) {
		var list = $('<ul>');
		for(var i = 0; i < feedback.length; i++) {
			var instance = feedback[i].feedback;
			for(var j = 0; j < instance.length; j++)
				list.append($('<li>').text(instance[j]));
		}
		if(feedback.length > 0) {
			$('#tutor .display').prepend($('<div>').addClass('alert alert-info').html(list));
			$('#tutor .display').prepend($('<h3>').text(new Date().toLocaleTimeString()));
			$('#tutor .display').prepend($('<hr>'));
			$('#link-tutor').click();
			toastr.info('You have feedback on your program. Look at the tutor tab to view the feedback.');
		}
	}
	
	if(status === 'success') {
		// Compiled successfully
		executable = result['executable'];
		require(['/scripts/modules/Editor.js'], function(Editor) {
			Editor.showCompilerErrors(editor, result['messages']);
		});
		// Display results
		toastr.success('Your program compiled successfully! You may view any compiler messages in the console.');
		logActivity('ion-ios-checkmark', 'success', 'COMPILATION', 'Compiled successfully!');
		$('#tab-console .log').append('Compiled successfully.' + "\n");
		if(result['messages'] && result['messages'].length > 0) {
			$('#tab-console .log').append(result['raw'].replace(/e[0-9]+_[0-9]+.c/g, 'program.c') + "\n");
		}
		$('#link-input').click();
	} else if(status === 'failure') {
		// Compilation error
		executable = null;
		require(['/scripts/modules/Editor.js'], function(Editor) {
			Editor.showCompilerErrors(editor, result['messages']);
		});
		toastr.error('Your program did NOT compile successfully! Please check the annotations on the editor and/or the compiler messages in the console.');
		logActivity('ion-ios-close', 'danger', 'COMPILATION', 'Compilation failed.');
		$('#tab-console .log').append('Compilation failed.' + "\n");
		$('#tab-console .log').append(result['raw'].replace(/e[0-9]+_[0-9]+.c/g, 'program.c') + "\n");
		$('#link-console').click();
	} else {
		// An error occurred.
		executable = null;
	}
}

// Displays the results of program execution.
function showExecutionResult(result) {
	var status = result['status'];
	$('#tab-output textarea').val('');
	if(status == 'ER') {
		toastr.error('Something wicked happened. The execution went wrong.');
		logActivity('ion-bug', 'danger', 'EXECUTION', 'System error.');
		$('#tab-console .log').append('Execution failed.' + "\n");
		$('#link-log').click();
	} else if(status == 'OK') {
		toastr.success('Your program executed successfully! See the output window to view the output of this program.');
		$('#tab-console .log').append('Execution succeeded.' + "\n");
		logActivity('ion-ios-checkmark', 'success', 'EXECUTION', 'Executed successfully!');
		showProgramOutput(result['output']);
	} else if(status == 'TL') {
		toastr.warning('The time limit exceeded for this program.');
		$('#tab-console .log').append('Time limit exceeded.' + "\n");
		logActivity('ion-ios-clock', 'warning', 'EXECUTION', 'Time limit exceeded.');
		$('#tab-output textarea').val(result['output']);
		$('#link-log').click();
	} else if(status == 'RF') {
		toastr.warning('This program tried to call prohibited functions. Execution was blocked.');
		$('#tab-console .log').append('Restricted function call.' + "\n");
		logActivity('ion-nuclear', 'warning', 'EXECUTION', 'Restricted function call.');
		$('#link-log').click();
	} else if(status == 'ML') {
		toastr.warning('The memory limit exceeded for this program.');
		$('#tab-console .log').append('Memory limit exceeded.' + "\n");
		logActivity('ion-card', 'warning', 'EXECUTION', 'Memory limit exceeded.');
		$('#link-log').click();
		var element = $('<h1>').addClass('alert alert-danger');
		element.append($('<span>').addClass('glyphicon glyphicon-credit-card'));
		element.append($('<span>').text(' Memory limit exceeded.'));
		$('#tab-output textarea').val(result['output']);
		$('#results').html(element);
	} else if(status == 'AT') {
		toastr.warning('This program terminated abnormally. Please check the return values for the main method.');
		$('#tab-console .log').append('Abnormal termination.' + "\n");
		logActivity('ion-close-circled', 'warning', 'EXECUTION', 'Abnormal termination.');
		$('#tab-output textarea').val(result['output']);
		$('#link-log').click();
	} else if(status == 'RT') {
		toastr.warning('This program has a runtime error. Execution was aborted.');
		$('#tab-console .log').append('Runtime error.' + "\n");
		logActivity('ion-android-warning', 'warning', 'EXECUTION', 'Runtime error.');
		$('#tab-output textarea').val(result['output']);
		$('#link-log').click();
		//showExecutionErrors(result['feedback']);
	} else if(status == 'CT') {
		toastr.warning('This program did not execute successfully.');
		$('#tab-console .log').append('Execution error.' + "\n");
		$('#tab-console .log').append(result['errors'] + "\n");
		logActivity('ion-close-circled', 'warning', 'EXECUTION', 'Execution error.');
		$('#link-log').click();
	} else {
		toastr.error('Something wicked happened. The execution went wrong.');
		logActivity('ion-bug', 'danger', 'EXECUTION', 'System error.');
		$('#tab-console .log').append('Execution failed.' + "\n");
		$('#link-log').click();
	}
}

function showProgramOutput(output) {
	if(env.display === 'text') {
		$('#tab-output textarea').val(output);
		$('#link-output').click();
	} else if(env.display === 'link') {
		var link = env.link_template;
		link = link.replace(/%s/g, encodeURI(output));
		$('#tab-output').html($('<a>').attr({
			href: link,
			target: '_blank'
		}).text(link));
		$('#link-output').click();
	}
}

//Displays the result of evaluating the program on test cases.
function showEvaluationResult(response) {
	
	if(response.results.length == 0) {
		toastr.info('Test cases are not available for this problem. :(');
		showFeedback(response.feedback);
		return;
	}
	
	var table = $('<table>').addClass('table table-bordered table-hover');
	table.append($('<tr>')
		.append($('<th>').text('#'))
		.append($('<th>').text('INPUT'))
		.append($('<th>').text('EXPECTED OUTPUT'))
		.append($('<th>').text('ACTUAL OUTPUT'))
	);
	var results = response.results;
	var passed = true;
	for(var i = 0; i < results.length; i++) {
		var status = results[i].actual.status;
		var icon = 'glyphicon ';
		if(results[i].actual.output == null) results[i].actual.output = ''; 
		if(status == 'OK') {
			if(results[i].expected.trim() == results[i].actual.output.trim()) {
				passed = true;
				icon = icon + 'glyphicon-ok';
			} else {
				passed = false;
				icon = icon + 'glyphicon-remove';
			}
		} else if(status == 'TL') {
			passed = false;
			icon = icon + 'glyphicon-time';
		} else if(status == 'RT') {
			passed = false;
			icon = icon + 'glyphicon-warning-sign';
		} else if(status == 'AT') {
			passed = false;
			icon = icon + 'glyphicon-remove-circle';
		} else if(status == 'RF') {
			passed = false;
			icon = icon + 'glyphicon-eye-close';
		} else if(status == 'ML') {
			passed = false;
			icon = icon + 'glyphicon-credit-card';
		}
		var context = 'danger';
		if(passed) context = 'success';
		table.append($('<tr>').addClass(context)
			.append($('<td>').text(i+1))
			.append($('<td>').addClass('pre').text(results[i].input))
			.append($('<td>').addClass('pre').text(results[i].expected))
			.append($('<td>').addClass('pre').text(results[i].actual.output))
			.append($('<td>').append($('<span>').addClass(icon)))
		); 
	}
	
	var invisible = response.invisible;
	
	if(invisible.total > 0) {
		$('#tutor .display').prepend($('<h3>')
			.text('Your program passed ' + invisible.passed + ' out of ' + invisible.total + ' hidden test case(s).')
			.append($('<div>').css('font-size', '15px').text('NOTE: These may not be the only hidden test cases that your program be evaluated upon.'))
			.addClass('alert alert-info')
		);
 		if (invisible.total != invisible.passed) {
			passed = false;
		}
	} else {
		$('#tutor .display').prepend($('<h3>')
			.text('No hidden test cases were available for this problem.')
			.append($('<div>').css('font-size', '15px').text('NOTE: There may exist hidden test cases which your program be evaluated upon in future.'))
			.addClass('alert alert-info')
		);
	}
	if(passed) {
		toastr.success('Congratulations! Your program has passed all test cases. You can now submit this program.');
		logActivity('ion-ios-checkmark', 'success', 'EVALUATION', 'Program accepted.');
		$('#tab-console .log').append('Program accepted.' + "\n");
	} else {
		toastr.warning('Your program did not pass all test cases. Please view the tutor panel to get details.');
		logActivity('ion-ios-close', 'danger', 'EVALUATION', 'Program not-accepted.');
		$('#tab-console .log').append('Program not-accepted.' + "\n");
	}
	
	
	$('#tutor .display').prepend(table);
	$('#tutor .display').prepend($('<div>').addClass('header-title').text('Evaluation Results'));
	$('#tutor .display th').css('text-align', 'left');
	
	$('#tutor .display').prepend($('<h3>').text(new Date().toLocaleTimeString()));
	$('#tutor .display').prepend($('<hr>'));
	
	$('#link-tutor').click();
	
	showFeedback(response.feedback);
}

function showFeedback(feedback) {
	console.log(feedback);
	if(feedback && feedback.length > 0) {
		for(var k = 0; k < feedback.length; k++) {
			var lines = feedback[k].split("*");
			var list = $('<ol>');
			for(var i = 0; i < lines.length; i++) {
				var line = lines[i].trim();
				if(line) list.append($('<li>').text(line));
			}
			$('#tutor .display').prepend($('<div>').addClass('alert alert-info').html(list));
		}
		$('#tutor .display').prepend($('<h3>').text(new Date().toLocaleTimeString()));
		$('#tutor .display').prepend($('<hr>'));
		$('#link-tutor').click();
		toastr.info('You have feedback on your program. Look at the tutor tab to view the feedback.');
	}
}

function showSyntacticFeedback(feedback, diff) {
	
	if(!feedback)
		return;
	
	var list = $('<ul>');
	var diffPane = $('<div>');
	
	for(var i = 0 ;i < feedback.length; i++) {
		var msg = feedback[i];
		list.append($('<li>').text(msg));
	}
	
	for(var i = 0; i < diff.length; i++) {
		diffPane.append($('<div>').css('font-family', 'monospace')
			.append($('<span>').text('Change '))
			.append($('<b>').css('color', 'red').text(diff[i].removed))
			.append($('<br>'))
			.append($('<span>').text(' to '))
			.append($('<b>').css('color', 'green').text(diff[i].added))
			.append($('<br>'))
		);
	}
	
	var section = $('<div>').append(list).append(diffPane);
	
	$('#tutor .display').prepend($('<div>').addClass('alert alert-info').html(section));
	//$('#tutor .display').prepend($('<h3>').text(new Date().toLocaleTimeString()));
	//$('#tutor .display').prepend($('<hr>'));
	$('#link-tutor').click();
	
	toastr.info('You have feedback on your program. Look at the tutor tab to view the feedback.');
}

// An utility to arrange the lines of editor annotations.
function arrange_lines(text) {
	
	var words = text.split(' ');
	var arranged = '';
	var line = '';
	
	for(var index = 0; index < words.length; index++) {
		line = line + words[index] + ' ';
		if(line.length >= 50) {
			// for clang, errors are multiline and depend on indentation.
			// Do not use extra newline.
			arranged = arranged + line;
			line = '';
		}
	}
	
	arranged = arranged + line;
	
	return arranged;
}

function logActivity(icon, context, title, message) {
	$('#tab-log table').prepend(
		$('<tr>').addClass(context)
			.append($('<th>').text(new Date().toLocaleTimeString()))
			.append($('<td>').append($('<span>').addClass('icon ' + icon)))
			.append($('<td>').text(title))
			.append($('<td>').text(message))
	);
}

//Displays the errors that occurred on program execution.
/*function showExecutionErrors(feedback) {
	
	editor.getSession().clearAnnotations();
	
	if(feedback === null) return;
	
	var type = feedback.type;
	var details = feedback.details;
	
	if(type === 'RT') {
		var rt_map = {
			'SIGFPE' : 'An arithmetic exception occurred here. You must be dividing by zero somewhere here or maybe an overflow occurred.',
			'SIGSEGV' : 'A segmentation fault occurred here. Are you accessing an invalid memory location?'
		};
		
		var signal = details.signal;
		var stackTrace = details.trace;
		var locals = details.locals;
		var globals = details.globals;
		
		var table = $('<table>').addClass('table table-bordered');
		table.append(
			$('<tr>')
				.append($('<th>').text('VARIABLE'))
				.append($('<th>').text('DATA TYPE'))
				.append($('<th>').text('VALUE'))
				.append($('<th>').text('SCOPE'))
		);
		
		for(var key in globals.values) {
			var type = globals.types[key];
			if(typeof type === 'undefined') type = 'UNKNOWN';
			table.append(
				$('<tr>')
					.append($('<td>').text(key))
					.append($('<td>').text(type))
					.append($('<td>').text(globals.values[key]))
					.append($('<td>').text('GLOBAL'))
			);
		}
		
		for(var i = 0; i < locals.length; i++) {
			for(var key in locals[i].values) {
				var type = locals[i].types[key];
				if(typeof type === 'undefined') type = 'UNKNOWN';
				table.append(
					$('<tr>')
						.append($('<td>').text(key))
						.append($('<td>').text(type))
						.append($('<td>').text(locals[i].values[key]))
						.append($('<td>').text('LOCAL'))
				);
			}
		}
		
		$('#results').append($('<div>').addClass('header-title').text('VARIABLES DUMP'));
		$('#results').append(table);
		$('#results th').css('text-align', 'left');
		
		var annotations = [];
		annotations.push({
			row: stackTrace[0].line - 1,
			column: 0,
			text: arrange_lines(rt_map[signal]),
			type: 'error'
		});
		
		for(var index = 0; index < stackTrace.length; index++) {
			var line = stackTrace[index].line - 1;
			if(index == 0) editor.getSession().addMarker(new Range(line,0,line,1), "error", "fullLine");
			else editor.getSession().addMarker(new Range(line,0,line,1), "warning", "fullLine");
		}
		
		editor.getSession().setAnnotations(annotations);
	}
}*/
