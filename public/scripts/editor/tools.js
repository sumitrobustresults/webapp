/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var processing = false;

function compile(perspective, code, callback) {
	if(processing) {
		toastr.warning('Please wait for the current operation to complete!');
		return;
	}
	processing = true;
	setTimeout(function() {
		_compile(perspective, code, callback)
	}, 100);
}

function execute(perspective, code, testCase, callback) {
	if(processing) {
		toastr.warning('Please wait for the current operation to complete!');
		return;
	}
	processing = true;
	setTimeout(function() {
		_execute(perspective, code, testCase, callback);
	}, 100);
}

function evaluate(callback) {
	if(processing) {
		toastr.warning('Please wait for the current operation to complete!');
		return;
	}
	processing = true;
	setTimeout(function() {
		_evaluate(callback);
	}, 100);
}

// Compiles the program and shows the result of compilation.
function _compile(perspective, code, callback) {
	if(perspective === 'event') {
		$.post('/compile', {
			code: code, 
			assignment_id: assignmentID,
			env: env.name
		}, function(response) {
			try {
				var result = eval( '(' + response + ')' );
				callback(null, result);
			} catch(err) {
				console.log(response);
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/compile',
					req_params: JSON.stringify({code: code, assignment_id: assignmentID}),
					response: response
				}, function(response) {});
				toastr.error('An error occurred while compiling your program. Please ask for assistance.');
				callback(err, null);
			} finally {
				processing = false;
			}
		});
	} else {
		$.post('/compile', {
			code: Base64.encode(code), 
			scratch: 1,
			env: env.name
		}, function(response) {
			try {
				var result = eval( '(' + response + ')' );
				callback(null, result);
			} catch(err) {
				console.log(err);
				console.log(response);
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/compile',
					req_params: JSON.stringify({code: Base64.encode(code), assignment_id: 0}),
					response: response
				}, function(response) {});
				toastr.error('An error occurred while compiling your program. Please ask for assistance.');
				callback(err, null);
			} finally {
				processing = false;
			}
		});
	}
}

// Executes the program on a user-provided test case.
function _execute(perspective, code, testCase, callback) {
	if(perspective === 'event') {
		$.post('/execute', {
			assignment_id: assignmentID, 
			code: code, 
			testcase: testCase,
			env: env.name
		}, function(response) {
			try {
				var result = eval( '(' + response + ')' );
				callback(null, result);
			} catch(err) {
				console.log(response);
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/execute',
					req_params: JSON.stringify({assignment_id: assignmentID, testcase: testCase}),
					response: response
				}, function(response) {});
				toastr.error('An error occurred while trying to execute your program. Please ask for assistance.');
				callback(err, null);
			} finally {
				processing = false;
			}
		});
	} else {
		$.post('/execute', {
			scratch: 1, 
			code: code, 
			testcase: testCase,
			env: env.name
		}, function(response) {
			try {
				var result = eval( '(' + response + ')' );
				console.log(result);
				callback(null, result);
			} catch(err) {
				console.log(response);
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/execute',
					req_params: JSON.stringify({assignment_id: 0, testcase: testCase}),
					response: response
				}, function(response) {});
				toastr.error('An error occurred while trying to execute your program. Please ask for assistance.');
				callback(err, null);
			} finally {
				processing = false;
			}
		});
	}
}

//Evaluates the program on the set of test cases.
function _evaluate(callback) {
	$.post('/evaluate', {
		assignment_id: assignmentID, 
		admin: false
	}, function(response) {
		try {
			var result = eval( '(' + response + ')' );
			callback(null, result);
		} catch(err) {
			console.log(response);
			$.post('/bug', {
				location: window.location.pathname,
				req_type: 'POST',
				req_uri: '/evaluate',
				req_params: JSON.stringify({assignment_id: assignmentID}),
				response: response
			}, function(response) {});
			toastr.error('An error occurred trying to evaluate your program. Please ask for assistance.');
			callback(err, null);
		} finally {
			processing = false;
		}
	});
}