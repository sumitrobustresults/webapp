/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var openFiles = {};
var currentFile = null;
var currentNode = null;
var deafen = false;

define(['jstree'], function() {

	load_css('jstree');
	
	editor.setReadOnly(true);
	
	renderWorkspace();
	bindEventHandlers();
	
	editor.getSession().on('change', function(event) {
		//var action = event.data.action;
		if(deafen) return;
		//console.log('heard');
		openFiles[currentFile].dirty = true;
		$('#p' + currentFile + ' .tab-name').text(openFiles[currentFile].name + '*');
    });
});

// Binds event handlers to various controls.
function bindEventHandlers() {
	
	// Upload
	$('#fileuploadinput').change(startUpload);
	
	$('#file-upload').off().click(function(e) {
		e.preventDefault();
		uploadFile(currentNode);
	});
	
	$('#file-download').off().click(function(e) {
		e.preventDefault();
		downloadFile(currentNode);
	});
	
	// New file.
	$('#file-newfile').click(function(e) {
		e.preventDefault();
		createFile(currentNode);
	});
	
	// New folder.
	$('#file-newdir').click(function(e) {
		e.preventDefault();
		createFolder(currentNode);
	});
	
	// Save
	$('#file-save').click(function(e) {
		e.preventDefault();
		if(currentFile === null) {
			toastr.info('Nothing to save.');
			return;
		}
		var contents = editor.getValue();
		if(Base64.encode(contents) === openFiles[currentFile].contents) {
			toastr.info('No changes to save.');
			return;
		}
		$.post('/editor/savefile', {contents: Base64.encode(contents), id: currentFile}, function(response) {
			try {
				//var result = eval('(' + response + ')');
				var result = response;
				if(result) {
					toastr.success('File saved successfully!');
					openFiles[currentFile].last_saved = result.savetime;
					openFiles[currentFile].contents = Base64.encode(contents);
					showCurrentFile();
					openFiles[currentFile].dirty = false;
					$('#p' + currentFile + ' .tab-name').text(openFiles[currentFile].name);
				} else toastr.error('Something went wrong! Unable to save your file.');
			} catch(err) {
				console.log(response);
				console.log(err);
				toastr.error('Something went wrong! Unable to save your file.');
			}
		});
	});
	
	// Compile
	$('#run-compile').click(function(e) {
		e.preventDefault();
		if(currentFile === null) return;
		var code = editor.getValue();
		var tools = '/scripts/editor/tools.js';
		var display = '/scripts/editor/display.js';
		$('.status .msg').text('Compiling your program');
		$('.status').show();
		$('#link-console').click();
		$.post('/editor/savefile', {contents: Base64.encode(code), id: currentFile}, function(response) {
			try {
				//var result = eval('(' + response + ')');
				var result = response;
				if(result) {
					openFiles[currentFile].last_saved = result.savetime;
					openFiles[currentFile].contents = Base64.encode(code);
					showCurrentFile();
					openFiles[currentFile].dirty = false;
					$('#p' + currentFile + ' .tab-name').text(openFiles[currentFile].name);
					require([tools, display], function() {
						compile('scratch', code, function(err, compilation) {
							$('.status').hide();
							if(err) return;
							console.log(compilation);
							editor.session.clearAnnotations();
							showCompilationResult(compilation);
						});
					});
				} else toastr.error('Something went wrong! Unable to save your file.');
			} catch(err) {
				console.log(response);
				console.log(err);
				toastr.error('Something went wrong! Unable to save your file.');
			}
		});
	});
	
	// Execute
	$('#run-execute').click(function(e) {
		e.preventDefault();
		if(currentFile === null) return;
		var tools = '/scripts/editor/tools.js';
		var display = '/scripts/editor/display.js';
		
		if(env.compile) {
			// Compile
			$('.status .msg').text('Compiling your program');
			$('.status').show();
			$('#link-console').click();
			var code = editor.getValue();
			require([tools, display], function() {
				compile('scratch', code, function(err, compilation) {
					$('.status').hide();
					if(err) return;
					if(compilation.result === 'success') {
						// Execute
						var testCase = $('#tab-input textarea').val();
						$('.status .msg').text('Executing your program');
						$('.status').show();
						$('#link-console').click();
						editor.session.clearAnnotations();
						require([tools, display], function() {
							execute('scratch', Base64.encode(code), testCase, function(err, execution) {
								$('.status').hide();
								if(err) return;
								if(execution) showExecutionResult(execution);
							});
						});
					} else showCompilationResult(compilation);
				});
			});
		} else {
			$('.status .msg').text('Executing your program');
			$('.status').show();
			var code = Base64.encode(editor.getValue());
			var testCase = $('#tab-input textarea').val();
			editor.session.clearAnnotations();
			require([tools, display], function() {
				execute('scratch', code, testCase, function(err, execution) {
					$('.status').hide();
					if(err) return;
					if(execution) showExecutionResult(execution);
				});
			});
		}
	});
}

// Renders the directory tree.
function renderWorkspace() {
	
	//files = [{path: ['bar.c'], dir: 0, id: 1}, {path: ['foo'], dir: 1, id: 2}, {path: ['foo', 'boo.c'], dir: 0, id: 3}];
	// Create tree nodes.
	var nodes = [];
	
	for(var i = 0; i < files.length; i++) {
		var path = files[i].path;
		var node = nodes;
		for(var j = 0; j < path.length - 1; j++) {
			for(var k = 0; k < node.length; k++) {
				if(node[k].text === path[j]) {
					node = node[k].children;
					break;
				}
			}
		} 
		if(files[i].dir == 1) {
			node.push({
				data: files[i],
				text: path[path.length - 1],
				icon: 'icon ion-folder',
				state: {
					opened: false,
					disabled: false,
					selected: false
				},
				children: []
			});
		} else {
			node.push({
				data: files[i],
				text: path[path.length - 1],
				icon: 'icon ion-document-text'
			});
		}
	}
	
	// Render tree.
	$('.files').jstree({
		core: {
			data: nodes,
			themes: {
				stripes: true
			}
		},
		contextmenu: {
			items: {
				open: {label: 'Open', action: openFile, icon: 'glyphicon glyphicon-open'},
				addfile: {label: 'New File', action: createFile, icon: 'glyphicon glyphicon-file', 'separator_before': true},
				addfolder: {label: 'New Folder', action: createFolder, icon: 'glyphicon glyphicon-folder-open'},
				remove: {label: 'Delete', action: deleteFile, icon: 'glyphicon glyphicon-trash', 'separator_before': true},
				download: {label: 'Download', action: downloadFile, icon: 'glyphicon glyphicon-save', 'separator_before': true},
				upload: {label: 'Upload', action: uploadFile, icon: 'glyphicon glyphicon-open'}
			}
		},
		dnd: {
			//
		},
		crrm: {
			move: {
				'check_move': function(node) {
					console.log(node);
					return true;
				}
			}
		},
		plugins: ['wholerow', 'contextmenu', 'state', 'dnd', 'crrm']
	}).on('select_node.jstree', function(e, sel) {
		currentNode = sel.node;
	}).on('ready.jstree', function() {
		//
	}).bind('dblclick.jstree', function(event) {
		var node = $(event.target).closest("li").get(0);
		openFile($(node).attr('id'));
	}).bind('move_node.jstree', function(e, data) {
		console.log(data);
	});
}

// Displays the contents of the current file and its meta information.
function showCurrentFile() {
	
	if(currentFile === null || typeof openFiles[currentFile] === 'undefined') return false;
	
	var file = openFiles[currentFile];
	
	deafen = true;
	editor.setValue(Base64.decode(file.contents));
	deafen = false;
	$('#tag-file').text('[' + file.name + ']');
	$('#save-status').html($('<span>').addClass('glyphicon glyphicon-floppy-saved'));
	$('#save-status').append(' ' + new Date(file.last_saved).toLocaleTimeString().toUpperCase() + ' ');
	
	editor.setReadOnly(false);
	
	return true;
}

// Opens a file.
function openFile(node) {
	
	var ext = '.' + env.source_ext;
	
	reduction = 44;
	
	if(typeof node === 'string') {
		var id = node;
	} else {
		var id = node.reference.prevObject[0].id;
	}
	node = $('.files').jstree('get_node', id);
	var data = node.data;
	
	id = data.id;
	var name = node.text;
	
	if(name.indexOf(ext) == name.length - ext.length) {
		// Save contents of editor for current file locally
		if(currentFile !== null) {
			openFiles[currentFile].contents = Base64.encode(editor.getValue());
		}
		// Load file asynchronously if not loaded.
		if(typeof openFiles[id] === 'undefined') {
			$.post('/editor/file', {id: id}, function(response) {
				try {
					//var file = eval('(' + response + ')');
					var file = response;
					if(file) {
						file.name = name;
						file.dirty = false;
						openFiles[id] = file;
						currentFile = id;
						showCurrentFile();
					}
					// Create a new tab for file.
					$('#editor-tabs .nav').append(
						$('<li>').append(
							$('<a>')
								.attr('data-toggle', 'tab')
								.attr('href', '')
								.attr('id', 'p' + id)
								.append($('<span>').addClass('tab-name').css({'font-size': 'medium'}).text(name))
								.append($('<button>')
									.css({'margin-left': '5px'})
									.attr('type', 'button')
									.attr('data-id', id)
									.addClass('close pull-right')
									.append($('<span>').html('&times'))
									.click(function(e) {
										e.preventDefault();
										var codeID = $(this).attr('data-id');
										var tab = $(this);
										if(openFiles[codeID].dirty) {
											alertify.confirm('You have unsaved changes in this file. If you continue your changes will be lost. Are you sure you want to discard changes?', function(e) {
												if(e) {
													closeTab(codeID, tab);
												}
											});
										} else {
											closeTab(codeID, tab);
										}
									})
								)
						)
					);
					$('#p' + id).click();
					$('#p' + id).click(function(e) {
						e.preventDefault();
						var id = $(this).attr('id').substring(1);
						currentFile = id;
						showCurrentFile();
					});
					onResize();
				} catch(err) {
					toastr.error('Something went wrong! Could not open file.');
				}
			});
		} else {
			currentFile = id;
			showCurrentFile();
		}
	} else {
		$('.files').jstree('open_node', node.id);
	}
	
	if(isMobile) {
		$('#link-editor').click();
	}
}

// Closes a tab.
function closeTab(codeID, tab) {
	delete openFiles[codeID];
	deafen = true;
	editor.setValue('');
	deafen = false;
	editor.setReadOnly(true);
	if(tab.parent().parent().prev().length != 0) {
		tab.parent().parent().prev().find('a').click();
	} else if(tab.parent().parent().next().length != 0) {
		tab.parent().parent().next().find('a').click();
	} else {
		currentFile = null;
		$('#tag-file').text('[No file selected]');
		reduction = 0;
		onResize();
	}
	$('#p' + codeID).parent().remove();
}

// Creates a new file.
function createFile(node) {
	
	var ext = '.' + env.source_ext;
	
	if(typeof ext === 'undefined') {
		toastr.error('The system is not configured properly. Please contact the administrator.');
		return;
	}
	
	if(node === null) {
		console.log('new');
		var id = '#';
		node = $('.files').jstree('get_node', id);
	} else if(typeof node['id'] === 'undefined') {
		console.log('old');
		var id = node.reference.prevObject[0].id;
		node = $('.files').jstree('get_node', id);
	}
	
	var data = node.data;
	var path = '';
	
	if(typeof data === 'undefined') {
		path = '';
	} else if(data.dir == 0) {
		var parent = $('.files').jstree('get_node', node.parent);
		if(parent.parent === null) path = '';
		else path = parent.data.loc + '/'; 
	} else {
		path = data.loc + '/';
	}
	
	alertify.prompt('Enter the name of your file:', function(e, filename) {
		if(e) {
			if(filename.indexOf(ext) < 0 || filename.indexOf(ext) != filename.length - ext.length) 
				filename = filename + ext;
			path = path + filename;
			path = path.replace("'", "-"); // Replace quote by dash to avoid json/sql errors.
			$.post('/editor/create', {path: path, dir: 0}, function(response) {
				try {
					var result = eval('(' + response + ')');
					if(result) {
						toastr.success('A new file has been created.');
						window.location.reload();
					} else toastr.error('Something went wrong! Could not create your file.');
				} catch(err) {
					toastr.error('Something went wrong! Could not create your file.');
				}
			});
		} 
	});
}

// Creates a new folder.
function createFolder(node) {
	
	if(node === null) {
		console.log('new');
		var id = '#';
		node = $('.files').jstree('get_node', id);
	} else if(typeof node['id'] === 'undefined') {
		console.log('old');
		var id = node.reference.prevObject[0].id;
		node = $('.files').jstree('get_node', id);
	}
	
	var data = node.data;
	var path = '';
	
	if(typeof data === 'undefined') {
		path = '';
	} else if(data.dir == 0) {
		var parent = $('.files').jstree('get_node', node.parent);
		if(parent.parent === null) path = '';
		else path = parent.data.loc + '/'; 
	} else {
		path = data.loc + '/';
	}
	
	alertify.prompt('Enter the name of your folder:', function(e, filename) {
		if(e) {
			if(filename.indexOf('.c') == filename.length - 2) 
				filename = filename.substring(0, filename.length - 2);
			path = path + filename;
			$.post('/editor/create', {path: path, dir: 1}, function(response) {
				try {
					var result = eval('(' + response + ')');
					if(result) {
						toastr.success('A new folder has been created.');
						window.location.reload();
					} else toastr.error('Something went wrong! Could not create your folder.');
				} catch(err) {
					toastr.error('Something went wrong! Could not create your folder.');
				}
			});
		} 
	});
}

// Deletes a file.
function deleteFile(node) {
	
	var id = node.reference.prevObject[0].id;
	node = $('.files').jstree('get_node', id);
	var data = node.data;
	
	if(data.dir == 1 && node.children.length > 0) {
		alertify.alert('This folder cannot be deleted as it is not empty. Please empty the folder and then delete it.');
		return;
	}
	
	alertify.confirm('Are you sure you want to delete this file/folder? This action cannot be undone.', function(e) {
		if(e) {
			$.post('/editor/delete', {id: data.id}, function(response) {
				try {
					if(response) {
						toastr.success('Your file/folder has been deleted successfully!');
						window.location.reload();
					} else toastr.error('Something went wrong! Unable to delete your file/folder.');
				} catch(err) {
					console.log(response);
					console.log(err);
					toastr.error('Something went wrong! Unable to delete your file/folder.');
				}
			});
		}
	});
}

function downloadFile(node) {
	
	if(node === null) {
		console.log('null node');
		var id = '#';
		node = $('.files').jstree('get_node', id);
	} 
	
	if(typeof node === 'string') {
		var id = node;
	} else if(typeof node.id !== 'undefined') {
		var id = node.id;
	} else {
		var id = node.reference.prevObject[0].id;
	}
	
	node = $('.files').jstree('get_node', id);
	var filename = node.text;
	var data = node.data;
	
	console.log(data);
	
	var link = '/editor/download/scratch/' + data.id + '?name=' + filename;
	
	window.location.href = link;
}

function uploadFile(node) {
	
	if(node === null) {
		console.log('new');
		var id = '#';
		node = $('.files').jstree('get_node', id);
	} else if(typeof node['id'] === 'undefined') {
		console.log('old');
		var id = node.reference.prevObject[0].id;
		node = $('.files').jstree('get_node', id);
	}
	
	var data = node.data;
	var path = '';
	
	if(typeof data === 'undefined') {
		path = '';
	} else if(data.dir == 0) {
		var parent = $('.files').jstree('get_node', node.parent);
		if(parent.parent === null) path = '';
		else path = parent.data.loc + '/'; 
	} else {
		path = data.loc + '/';
	}
	
	$('#fileuploadinput').attr('data-path', path).click();
}

function startUpload(e) {
	var ext = '.' + env.source_ext;
	var file = e.target.files[0];
	var filename = file.name;
	if(filename.indexOf(ext) < 0 || filename.indexOf(ext) != filename.length - ext.length) 
		filename = filename + ext;
	var path = $('#fileuploadinput').attr('data-path');
	path = path + filename;
	path = path.replace("'", "-");
	console.log(path);
	console.log(file);
	if(file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			var contents = Base64.encode(e.target.result);
			$.post('/editor/upload', {path: path, dir: 0, contents: contents}, function(success) {
				if(success) {
					toastr.success('File has been successfully uploaded.');
					window.location.reload();
				} else {
					toastr.error('Something went wrong! Could not upload your file.');
				}
			});
		};
		reader.readAsText(file);
	} else {
		toastr.error('Unable to read file!');
	}
}