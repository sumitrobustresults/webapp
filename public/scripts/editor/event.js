/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define(['mathjax'], function() {
	
	editor.on('paste', function(text) {
    	// Buggy
    	setTimeout("save('paste')", 1000);
    });
	
	editor.getSession().on('change', function(event) {
		$('#btn-evaluate').attr('disabled', 'disabled');
    	save('auto');
    });
	
	bindScratchHandlers();
	
	MathJax.Hub.Config({
		tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] }
	});
});

// Binds event handlers to various controls.
function bindScratchHandlers() {
	// Menu bar
	
	$('#file-upload').click(function(e) {
		e.preventDefault();
		$('#fileinput').click();
	});
	
	$('#fileinput').change(upload);
	
	$('#file-save').click(function(e) {
		e.preventDefault();
		var triggered = save('manual', function(result) {
			if(result) toastr.success('Code saved successfully!');
			else toastr.error('Unable to save your code! Please ask for assistance.');
		});
		if(!triggered) toastr.info('There are no changes to save.');
	});
	
	$('#run-compile').click(function(e) {
		e.preventDefault();
		var code = editor.getValue();
		var tools = '/scripts/editor/tools.js';
		var display = '/scripts/editor/display.js';
		$('.status .msg').text('Compiling your program');
		$('.status').show();
		$('#link-console').click();
		require([tools, display], function() {
			compile('event', code, function(err, compilation) {
				$('.status').hide();
				if(err) return;
				codeID = compilation.code_id;
				$('#sid').attr('title', 'ID: ' + codeID);
				editor.session.clearAnnotations();
				showCompilationResult(compilation);
			});
		});
	});
	
	$('#run-execute').click(function(e) {
		e.preventDefault();
		var tools = '/scripts/editor/tools.js';
		var display = '/scripts/editor/display.js';
		if(env.compile) {
			// Compile
			$('.status .msg').text('Compiling your program');
			$('.status').show();
			$('#link-console').click();
			var code = editor.getValue();
			require([tools, display], function() {
				compile('event', code, function(err, compilation) {
					$('.status').hide();
					if(err) return;
					codeID = compilation.code_id;
					$('#sid').attr('title', 'ID: ' + codeID);
					editor.session.clearAnnotations();
					if(compilation.result === 'success') {
						// Execute
						var testCase = $('#tab-input textarea').val();
						$('.status .msg').text('Executing your program');
						$('.status').show();
						$('#link-console').click();
						require([tools, display], function() {
							execute('event', null, testCase, function(err, execution) {
								$('.status').hide();
								if(err) return;
								if(execution) showExecutionResult(execution);
							});
						});
					} else showCompilationResult(compilation);
				});
			});
		} else {
			$('.status .msg').text('Executing your program');
			$('.status').show();
			$('#link-console').click();
			var code = Base64.encode(editor.getValue());
			var testCase = $('#tab-input textarea').val();
			require([tools, display], function() {
				execute('event', code, testCase, function(err, execution) {
					$('.status').hide();
					if(err) return;
					if(execution) showExecutionResult(execution);
				});
			});
		}
	});
	
	$('#run-evaluate').click(function(e) {
		e.preventDefault();
		var tools = '/scripts/editor/tools.js';
		var display = '/scripts/editor/display.js';
		if(env.compile) {
			// Compile
			$('.status .msg').text('Compiling your program');
			$('.status').show();
			$('#link-console').click();
			var code = editor.getValue();
			require([tools, display], function() {
				compile('event', code, function(err, compilation) {
					$('.status').hide();
					if(err) return;
					codeID = compilation.code_id;
					$('#sid').attr('title', 'ID: ' + codeID);
					editor.session.clearAnnotations();
					if(compilation.result === 'success') {
						// Evaluate
						$('.status .msg').text('Evaluating your program');
						$('.status').show();
						$('#link-console').click();
						require([tools, display], function() {
							evaluate(function(err, evaluation) {
								$('.status').hide();
								if(err) return;
								console.log(evaluation);
								if(evaluation) showEvaluationResult(evaluation);
							});
						});
					} else showCompilationResult(compilation);
				});
			});
		} else {
			$('.status .msg').text('Evaluating your program');
			$('.status').show();
			$('#link-console').click();
			require([tools, display], function() {
				evaluate(function(err, evaluation) {
					$('.status').hide();
					if(err) return;
					console.log(evaluation);
					if(evaluation) showEvaluationResult(evaluation);
				});
			});
		}
	});
	
	$('#file-submit').click(function(e) {
		e.preventDefault();
		save('submit', function(result) {
			if(result) {
				window.location.href = '/home';
			} else toastr.error('Unable to take submission');
		});
	});
}

// Updates the timer for the ongoing event.
function updateTimer() {
	var remaining = moment(expiry).fromNow();
	$('#timer').text(' Ends ' + remaining);
	var left = moment(expiry).diff(moment(new Date()), 'minutes');
	if(left == 30) {
		alertify.alert('You have less than 30 minutes remaining. Please ensure that your code is saved and submitted before the time expires.'); 
	} else if( left == 15) {
		alertify.alert('You have less than 15 minutes remaining. Please ensure that your code is saved and submitted before the time expires.');
	} else if(left == 5) {
		alertify.alert('You have less than 5 minutes remaining. Please ensure that your code is saved and submitted before the time expires.');
	} 
	if(moment(expiry).diff(moment(new Date()), 'seconds') <= 0) {
		alertify.alert('Time up! We are sorry but you cannot continue anymore. You will now be redirected to the home page.', function() {
			window.location.href = '/';
		});
	}
	setTimeout('updateTimer()', 50000);
}

// Saves code to maintain code history
function save(trigger, callback) {
	
	if(saveLock && trigger === 'auto') return false;
	saveLock = true;
	
	if(trigger === 'auto' && !autosaveEnabled) {
		saveLock = false;
		return false;
	}
	var newCode = editor.getValue().trim();
	if(newCode === code && trigger != 'submit') {
		saveLock = false;
		return false;
	}
	if(trigger === 'auto') {
		if(Math.abs(newCode.length - code.length) < THRESHOLD_CHAR 
				|| new Date().getTime() < (lastSaved + THRESHOLD_TIME)) {
			saveLock = false;
			return false;
		}
	}
	// Added to prevent multiple saves due to copy-paste
	lastSaved = new Date().getTime();
	$('#save-status').html($('<span>').addClass('glyphicon glyphicon-floppy-save'));
	$('#save-status').prepend('Saving... ');
	code = newCode;
	require(['base64'], function() {
		var base64Code = Base64.encode(code);
		// Save locally
		/*if(Math.abs(newCode.length - localCode.length) > THRESHOLD_CHAR * 5 || trigger ==='manual') {
			storeLocally(code);
		}*/
		$.post('/editor/save', {assignment_id: assignmentID, branch_id: 0, code: base64Code, trigger: trigger}, function(response) {
			var res = response;
			try {
				response = eval('(' + response + ')');
			} catch(err) {
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/e/save',
					req_params: JSON.stringify({assignment_id: assignmentID, branch_id: 0, code: base64Code, trigger: trigger}),
					response: res
				}, function(response) {});
				$('#error').modal('show');
				response = null;
			} finally {
				saveLock = false;
			}
			if(response !== null) {
				lastSaved = new Date().getTime();
				var at = new Date(lastSaved).toLocaleTimeString().toUpperCase();
				$('#save-status').html($('<span>').addClass('glyphicon glyphicon-floppy-saved'));
				$('#save-status').append(' Last saved at ' + at + ' ');
				$('#sid').attr('title', 'ID: ' + response);
				codeID = response;
				if(typeof callback !== 'undefined') callback(true);
			} else {
				$('#save-status').html($('<span>').addClass('glyphicon glyphicon-floppy-remove'));
				$('#save-status').append(' Unable to save. ');
				if(typeof callback !== 'undefined') callback(false);
			}
		});
	});
	
	return true;
}

function upload(e) {
	var file = e.target.files[0];
	console.log(file);
	if(file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			var contents = e.target.result;
			autosaveEnabled = false;
			editor.setValue(contents);
			save('manual', function(success) {
				if(success) {
					toastr.success('Code successfully uploaded.');
				} else {
					toastr.error('Unable to save uploaded code! Please try again later.');
				}
			});
			autosaveEnabled = true;
		};
		reader.readAsText(file);
	} else {
		toastr.error('Unable to read file!');
	}
}

/*function upgrade() {
$.getJSON('/e/upgrade', {assignment_id: assignmentID}, function(response) {
	$('#btn-upgrade').removeAttr('disabled');
	if(response === false) {
		toastr.warning('You cannot upgrade your problem any further.');
	} else if(response) {
		$('#statement .area').text(response.statement);
		toastr.info('Your problem has been upgraded. See your new problem statement.');
	} else {
		toastr.error('An error occurred. Could not upgrade your problem.');
	}
});
} 

function downgrade() {
$.getJSON('/e/downgrade', {assignment_id: assignmentID}, function(response) {
	$('#btn-downgrade').removeAttr('disabled');
	if(response === false) {
		toastr.warning('You cannot downgrade your problem any further.');
	} else if(response) {
		$('#statement .area').text(response.statement);
		toastr.info('Your problem has been downgraded. See your new problem statement.');
	} else {
		toastr.error('An error occurred. Could not downgrade your problem.');
	}
});
} */