/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var debugging = false;
var running = false;
var uid = null;
var accept = null;
var breakpoints = {};
var cursor = null;
var marker = null;

var tools = '/scripts/editor/tools.js';
var display = '/scripts/editor/display.js';

var peer = null;
var conn = null;

define([tools, display], function() {
	
	/*var dbgPeers = debugConfig.peers;
	peer = dbgPeers[Math.round(Math.random() * (dbgPeers.length - 1))];
	if(window.location.hostname === 'localhost') peer = 'localhost';*/
	
	peer = window.location.hostname;
	
	$('#debug-menu li').addClass('disabled');
	$('.init').removeClass('disabled');
	bindDebugHandles();
});

// Binds event listeners to various controls.
function bindDebugHandles() {
	
	editor.on('guttermousedown', function(e) {
		if(!debugging) return;
		var target = e.domEvent.target; 
		if(target.className.indexOf("ace_gutter-cell") == -1) return;
		if(!editor.isFocused()) return; 
		if(e.clientX > 25 + target.getBoundingClientRect().left) return;
		var row = e.getDocumentPosition().row;
		var breakpoints = e.editor.session.getBreakpoints();
		if(typeof breakpoints[row] === 'undefined') {
			breakpoint_add(row);
		} else {
			breakpoint_remove(row);
		}
	    e.stop();
	});
	
	// Start debugger.
	$('#debug-start').click(function(e) {
		e.preventDefault();
		if(currentFile === null) {
			toastr.warning('You have not opened any file. Please open a file to debug.');
			return;
		}
		if(debugging) {
			toastr.warning('A debugging session is already in progress. Please wait for it to complete or stop it manually.');
			return;
		}
		debugging = true;
		editor.setReadOnly(true);
		editor.setOption("highlightActiveLine", false);
		$('.run,.halt').removeClass('disabled');
		$('.init').addClass('disabled');
		$('#link-debug').click();
		debug_init();
	});
	
	// Run
	$('#debug-run').click(function(e) {
		e.preventDefault();
		if(running) return;
		running = true;
		$('.run').addClass('disabled');
		$('.exec').removeClass('disabled');
		run();
	});
	
	// Continue
	$('#debug-continue').click(function(e) {
		e.preventDefault();
		cont();
	});
	
	// Next
	$('#debug-next').click(function(e) {
		e.preventDefault();
		next();
	});
	
	// Step
	$('#debug-step').click(function(e) {
		e.preventDefault();
		step();
	});
	
	// Stop
	$('#debug-stop').click(function(e) {
		e.preventDefault();
		stop();
	});
}

// Start debugging.
function debug_init() {
	$('#tab-output textarea').val('');
	var code = editor.getValue();
	compile(perspective, code, function(err, compilation) {
		if(err) {
			console.log(err);
		} else if(compilation.result === 'success') {
			require(['socketio'], function(io) {
				conn = io();
				conn.on('connect', function() {
					console.log('connected');
					conn.emit('message', JSON.stringify({
						action: 'debug_init', 
						executable: compilation.executable, 
						input: $('#tab-input textarea').val()
					}));
					conn.on('message', function(msg) {
						read_message(msg);
					});
				});
			});
			/*conn = new WebSocket('ws://' + peer + ':9791');
			conn.onopen = function(e) {
				conn.emit('message', JSON.stringify({
					action: 'debug_init', 
					executable: compilation.executable, 
					input: $('#tab-input textarea').val()
				}));
			};
			conn.onmessage = function(e) {
				read_message(e.data);
			};*/
		} else {
			toastr.error('Your program did not compile! You cannot run the debugger until your program compiles successfully.');
			showCompilationResult(compilation);
		}
	});
}

// Adds a breakpoint.
function breakpoint_add(row) {
	editor.session.setBreakpoint(row, 'ace_breakpoint');
	accept = function(data) {
		data = encode_response(data.result[0]);
		console.log(data);
		var bkpt = data.info.bkpt;
		breakpoints[bkpt.line] = bkpt.number;
	};
	conn.emit('message', JSON.stringify({
		action: 'run_command',
		uid: uid,
		cmd: '-break-insert ' + (row + 1)
	}));
}

// Removes a breakpoint.
function breakpoint_remove(row) {
	editor.session.clearBreakpoint(row);
	accept = function(data) {
		data = encode_response(data.result[0]);
		console.log(data);
		delete breakpoints[row + 1];
	};
	conn.emit('message', JSON.stringify({
		action: 'run_command',
		uid: uid,
		cmd: '-break-delete ' + breakpoints[(row + 1)]
	}));
}

// Runs the debugger until next stop.
function run() {
	accept = function(data) {
		console.log(data);
		dig_output(data.result);
		data = encode_response(data.result[data.result.length - 1]);
		analyze_stop(data.info);
		running = false;
	};
	conn.emit('message', JSON.stringify({
		action: 'run_command',
		uid: uid,
		cmd: '-exec-run'
	}));
}

// Continues the program execution till the next stop.
function cont() {
	accept = function(data) {
		dig_output(data.result);
		data = encode_response(data.result[data.result.length - 1]);
		analyze_stop(data.info);
		console.log(data);
	};
	conn.emit('message', JSON.stringify({
		action: 'run_command',
		uid: uid,
		cmd: '-exec-continue'
	}));
}

// Runs the next instruction or function.
function next() {
	accept = function(data) {
		dig_output(data.result);
		data = encode_response(data.result[data.result.length - 1]);
		analyze_stop(data.info);
		console.log(data);
	};
	conn.emit('message', JSON.stringify({
		action: 'run_command',
		uid: uid,
		cmd: '-exec-next'
	}));
}

// Steps on to the next line of instruction.
function step() {
	accept = function(data) {
		dig_output(data.result);
		data = encode_response(data.result[data.result.length - 1]);
		analyze_stop(data.info);
		console.log(data);
	};
	conn.emit('message', JSON.stringify({
		action: 'run_command',
		uid: uid,
		cmd: '-exec-step'
	}));
}

// Stops the debugger.
function stop() {
	accept = function(data) {
		conn.close();
		if(data.success) {
			toastr.info('Debugging has been stopped.');
			$('#debug-menu li').addClass('disabled');
			$('.init').removeClass('disabled');
			debugging = false;
			editor.setReadOnly(false);
			editor.setOption("highlightActiveLine", true);
			editor.getSession().clearAnnotations();
			editor.getSession().clearBreakpoints();
			if(marker) {
				editor.session.removeGutterDecoration(cursor, 'ace-gutter-warning');
				editor.getSession().removeMarker(marker);
			}
		}
	};
	conn.emit('message', JSON.stringify({
		action: 'debug_stop',
		uid: uid
	}));
}

// Get stack variables.
function get_vars() {
	accept = function(data) {
		data = encode_response(data.result[data.result.length - 1]);
		console.log(data);
		var variables = data.info.variables;
		$('.var').remove();
		for(var i in variables) {
			$('#locals').append(
				$('<tr>').addClass('var')
					.append($('<td>').text(variables[i].name))
					.append($('<td>').text(variables[i].type))
					.append($('<td>').text(variables[i].value))
			);
		}
	};
	conn.emit('message', JSON.stringify({
		action: 'run_command',
		uid: uid,
		cmd: '-stack-list-variables --simple-values'
	}));
}

// Analyse stoppage points.
function analyze_stop(stoppage) {
	if(stoppage.reason === 'signal-received') {
		// SIGNAL
		var signal = stoppage['signal-name'];
		var meaning = stoppage['signal-meaning'];
		var annotations = [];
		annotations.push({
			row: parseInt(stoppage.frame.line) - 1,
			column: 0,
			text: arrange_lines(meaning),
			type: 'error'
		});
		console.log(stoppage);
		editor.getSession().setAnnotations(annotations);
		get_vars();
	} else if(stoppage.reason === 'exited-normally') {
		// NORMAL EXIT
		stop();
	} else if(stoppage.reason === 'breakpoint-hit') {
		// BREAKPOINT
		var bkptno = stoppage.bkptno;
		var bkpt = null;
		for(var line in breakpoints) {
			if(breakpoints[line] === bkptno) {bkpt = line; break;} 
		}
		editor.session.clearBreakpoint(line - 1);
		add_cursor(line - 1);
		get_vars();
	} else if(stoppage.reason === 'exited-signalled') {
		// SIGNAL EXIT
		stop();
	} else if(stoppage.reason === 'end-stepping-range') {
		// STEPPING
		var line = stoppage.frame.line;
		add_cursor(line - 1);
		get_vars();
	} else {
		toastr.info('Debugger reported to stop due to reason: ' + stop.reason);
	}
}

// Adds a pointer to the current position of debugger.
function add_cursor(row) {
	if(cursor != null) {
		editor.session.removeGutterDecoration(cursor, 'ace-gutter-warning');
		editor.session.removeMarker(marker);
	}
	marker = editor.session.addMarker(new Range(row, 0, row, 200), "ace-marker-warning", "fullLine");
	editor.session.addGutterDecoration(row, 'ace-gutter-warning');
	cursor = row;
}

// Encodes response into JSON format.
function encode_response(response) {
	var index = response.indexOf(',');
	if(index == -1) return {
		result: response,
		info: {}
	};
	var result = response.substring(0, index);
	var info = response.substring(index + 1);
	info = info.replace(/,([a-zA-Z\-]+)=/g, ",'$1'=");
	info = info.replace(/=/g, ':');
	try {
		info = eval('({' + info + '})');
		return {
			result: result,
			info: info
		};
	} catch(err){
		console.log(err);
		return null
	}
}

// Decodes output from debugger.
function dig_output(response) {
	
	var output = null;
	var markers = ['=', '^', '*', '~'];
	
	for(var i = 0; i < response.length; i++) {
		var msg = response[i];
		if(markers.indexOf(msg.substring(0, 1)) == -1) {
			// Output from program.
			if(output === null) output = '';
			var pos = msg.search(/[=~\^\*]([a-zA-Z]+\-?[a-zA-Z]*,)/g);
			if(pos == -1) pos = msg.length;
			output = output + msg.substring(0, pos) + "\n";
		}
	}
	
	if(output) {
		var console = $('#tab-output textarea');
		console.val(console.val() + output + "\n");
		$('#link-output').click();
	}
	
	return output;
}

// Reads and interprets the messages received from the debug server.
function read_message(msg) {
	
	console.log(msg);
	
	try {
		var data = eval('(' + msg + ')');
		
		if(typeof data['uid'] !== 'undefined') {
			uid = data.uid;
			toastr.info('Debug session has started!');
		} else {
			accept(data);
		}
	} catch(err) {
		console.log(err);
		toastr.error('Something went wrong! Unable to communicate with the debugger properly.');
	}
}