/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 * 
 * Usage of this program and the accompanying materials in any form 
 * without prior permission from the owner is strictly prohibited. 
 * 
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

var editor = null;
var executable = null;

define(['bootstrap', 'ace', 'base64'], function() {
	
	load_css('/styles/scratchpad');
	load_css('/styles/loader');
	
	try {
		programs = eval('(' + Base64.decode(programs) + ')');
	} catch(err) {
		$('#error').modal('show');
		return;
	}
	
	$(document).ready(function() {
		stylize();
		bindHandlers();
		if(codeID != 0) initEditor();
		code = Base64.decode(code);
		editor.setValue(code);
		window.onresize = function() {
			$('#editor').css('min-height', (window.innerHeight - 90) + 'px');
			editor.resize();
		};
	});
});

function initEditor() {
	Range = ace.require("ace/range").Range;
	editor = ace.edit("editor");
	editor.setTheme("ace/theme/eclipse");
	editor.setFontSize(16);
	editor.getSession().setMode("ace/mode/c_cpp");
	editor.getSession().setUseWrapMode(true);
	editor.getSession().on('change', function(event) {
			executable = null;
			$('#btn-execute').attr('disabled', 'disabled');
			});
}

function stylize() {
	$('#editor').css('min-height', (window.innerHeight - 90) + 'px');
	$('.list-group-item').first().addClass('active');
}

function bindHandlers() {
	
	$('#btn-new').click(function(event) {
		event.preventDefault();
		var name = $(this).parent().prev().val().trim();
		if(name == '') {
			$('.form-group').addClass('has-error');
			return;
		}
		$('.form-group').removeClass('has-error');
		$(this).parent().prev().val('');
		newProgram(name);
	});
	
	$('#btn-save').click(function(event) {
		save();
	});
	
	$('.list-group-item').click(function() {
		if($(this).hasClass('active')) return;
		save();
		var id = $(this).attr('href');
		id = id.substring(1);
		$('.list-group-item').removeClass('active');
		$(this).addClass('active');
		var oldCode = code;
		var oldID = codeID;
		for(var i = 0; i < programs.length; i++) {
			if(programs[i].id == id) {
				codeID = id;
				code = Base64.decode(programs[i].code);
				editor.setValue(code);
			}
			if(programs[i].id == oldID) {
				programs[i].code = Base64.encode(oldCode);
			}
		}
	});
	
	$('#work-tab').click(function() {
		if($('#work-tab .icon').hasClass('glyphicon-collapse-up')) {
			$('#work-tab .icon').removeClass('glyphicon-collapse-up');
			$('#work-tab .icon').addClass('glyphicon-collapse-down');
			$('#workspace').slideUp(500, function() {
				//
			});
		} else {
			$('#work-tab .icon').removeClass('glyphicon-collapse-down');
			$('#work-tab .icon').addClass('glyphicon-collapse-up');
			$('#workspace').slideDown(500, function() {
				//
			});
		}
	});
	
	$('#btn-compile').click(function() {
		$(this).attr('disabled', 'disabled');
		code = editor.getValue();
		showWorkspace();
		$('#workspace .work').html(getLoader('compiling your code'));
		$.post('/compile', {code: Base64.encode(code), assignment_id: 0}, function(response) {
			console.log(response);
			var res = response;
			try {
				response = eval( '(' + response + ')' );
				showCompilationResult(response);
			} catch(err) {
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/compile',
					req_params: JSON.stringify({code: Base64.encode(code), assignment_id: 0}),
					response: res
				}, function(response) {});
				$('#error').modal('show');
			} finally {
				$('.loader').remove();
				$('#btn-compile').removeAttr('disabled');
			}
		});
	});
	
	$('#btn-execute').click(function() {
		if(executable == null) return;
		$(this).attr('disabled', 'disabled');
		var testCase = $('#testcase').val(); 
		showWorkspace();
		$('#workspace .work').prepend(getLoader('executing your code'));
		// TODO bug here. testcase may be null.
		$.post('/execute', {assignment_id: 0, testcase: testCase}, function(response) {
			console.log(response);
			var res = response;
			try {
				response = eval( '(' + response + ')' );
				showExecutionResult(response);
			} catch(err) {
				$.post('/bug', {
					location: window.location.pathname,
					req_type: 'POST',
					req_uri: '/execute',
					req_params: JSON.stringify({assignment_id: 0, testcase: testCase}),
					response: res
				}, function(response) {});
				$('#error').modal('show');
			} finally {
				$('.loader').remove();
				$('#btn-execute').removeAttr('disabled');
			}
		});
	});
}

function showWorkspace() {
	if($('#work-tab .icon').hasClass('glyphicon-collapse-up')) return;
	$('#work-tab .icon').removeClass('glyphicon-collapse-down');
	$('#work-tab .icon').addClass('glyphicon-collapse-up');
	$('#workspace').slideDown(500, function() {
		//
	});
}

function getLoader(text) {

	var child = $('<div>').addClass('gmb-loader');
	child.append($('<div>'));
	child.append($('<div>'));
	child.append($('<div>'));
	
	var loader = $('<h2>').attr('align', 'center').addClass('loader');
	loader.append(child);
	loader.append($('<br>'));
	loader.append($('<span>').text(text));
	
	return loader;
} 

function save() {
	var newCode = editor.getValue().trim();
	if(code.trim() === newCode) return;
	$('#status').removeClass('glyphicon-floppy-remove glyphicon-floppy-saved');
	$('#status').addClass('glyphicon-floppy-save');
	code = newCode;
	var toPost = Base64.encode(code);
	$.post('/s/save', {code_id: codeID, code: toPost}, function(response) {
		var res = response;
		try {
			response = eval('(' + response + ')');
		} catch(err) {
			$.post('/bug', {
				location: window.location.pathname,
				req_type: 'POST',
				req_uri: '/s/save',
				req_params: JSON.stringify({code_id: codeID, code: toPost}),
				response: res
			}, function(response) {});
			response = false;
		}
		if(response) {
			// saved
			$('#status').removeClass('glyphicon-floppy-save');
			$('#status').addClass('glyphicon-floppy-saved');
		} else {
			// error
			$('#status').removeClass('glyphicon-floppy-save');
			$('#status').addClass('glyphicon-floppy-remove');
		}
	});
}

function newProgram(name) {
	var code = Base64.encode('');
	$.post('/s/create', {name: name, code: code}, function(response) {
		var res = response;
		try {
			response = eval('(' + response + ')');
		} catch(err) {
			$.post('/bug', {
				location: window.location.pathname,
				req_type: 'POST',
				req_uri: '/s/create',
				req_params: JSON.stringify({name: name, code: code}),
				response: res
			}, function(response) {});
			response = false;
		}
		if(response) window.location.reload();
		else {
			$('#error').modal('show');
			console.log(response);
		}
	});
}

function showExecutionResult(result) {
	var status = result['status'];
	if(status == 'ER') {
		$('#workspace .work').html($('<div>').addClass('alert alert-danger').text('Something wicked happened. The execution went wrong. Try again after some time.'));
	} else if(status == 'OK') {
		$('.output').remove();
		$('#workspace .work').append($('<div>').addClass('header-title').text('Output').addClass('output'));
		$('#workspace .work').append($('<pre>').text(result['output']).addClass('output'));
	} else if(status == 'TL') {
		var element = $('<h1>').addClass('alert alert-danger');
		element.append($('<span>').addClass('glyphicon glyphicon-time'));
		element.append($('<span>').text(' Time limit exceeded.'));
		$('#workspace .work').html(element);
	} else if(status == 'RF') {
		var element = $('<h1>').addClass('alert alert-danger');
		element.append($('<span>').addClass('glyphicon glyphicon-eye-close'));
		element.append($('<span>').text(' Your program misbehaved by trying to execute some prohibited system calls. This incident will be reported.'));
		$('#workspace .work').html(element);
	} else if(status == 'ML') {
		var element = $('<h1>').addClass('alert alert-danger');
		element.append($('<span>').addClass('glyphicon glyphicon-credit-card'));
		element.append($('<span>').text(' Memory limit exceeded.'));
		$('#workspace .work').html(element);
	} else if(status == 'AT') {
		var element = $('<h1>').addClass('alert alert-warning');
		element.append($('<span>').addClass('glyphicon glyphicon-remove-circle'));
		element.append($('<span>').text(' Your program terminated abnormally. Check if your main program returns a 0 or not.'));
		$('#workspace .work').html(element);
	} else if(status == 'RT') {
		var element = $('<h1>').addClass('alert alert-danger');
		element.append($('<span>').addClass('glyphicon glyphicon-warning-sign'));
		element.append($('<span>').text(' Runtime error!'));
		$('#workspace .work').html(element);
		//showExecutionErrors(result['feedback']);
	} else {
		$('#workspace .work').html($('<div>').addClass('alert alert-danger').text('Something wicked happened. The execution went wrong. Try again after some time.'));
	}
	
	showWorkspace();
}

function showCompilationResult(result) {
	
	var status = result['result'];
	
	if(status === 'success') {
		// Compiled successfully
		executable = result['executable'];
		showCompilationErrors(result['messages']);
		$('#workspace .work').html($('<div>').addClass('alert alert-success').html($('<b>').text('Compiled Successfully')).css('margin-bottom', '0px'));
		if(result['messages'].length > 0) {
			$('#workspace .work').append($('<div>').addClass('header-title').html('Compiler Output'));
			$('#workspace .work').append($('<pre>').text(result['raw']).addClass('col-md-12').attr('readonly', 'readonly'));
		}
		$('#workspace .work').append($('<div>').addClass('header-title').text('Input'));
		$('#workspace .work').append($('<textarea>').addClass('col-md-12').attr('id', 'testcase'));
		$('#btn-execute').removeAttr('disabled');
	} else if(status === 'failure') {
		// Compilation error
		showCompilationErrors(result['messages']);
		$('#workspace .work').html($('<div>').addClass('alert alert-danger').html($('<b>').text('Compilation Failed!')).css('margin-bottom', '0px'));
		$('#workspace .work').append($('<div>').addClass('header-title').html('Compiler Output'));
		$('#workspace .work').append($('<pre>').text(result['raw']).addClass('col-md-12').attr('readonly', 'readonly'));
		executable = null;
		$('#btn-execute').attr('disabled', 'disabled');
	} else {
		// An error occurred.
		executable = null;
		$('#btn-execute').attr('disabled', 'disabled');
	}
	
	showWorkspace();
}

function showCompilationErrors(errors) {
	
	var map = {
		'HEADER_INCLUDE' : 'Seems that you have missed out some "#include <header_file.h>" statements at the top of your code. How about "string.h" or "math.h" perhaps?',
		'NO_RETURN' : 'Your method has a return type but does not return any value. Add a return statement at the end of the method or wherever necessary.',
		'UNCLOSED_BRACES' : 'Well this should not happen with this editor but it seems that you have not closed a few braces "{}". Check the braces in your code and make sure they are closed.',
		'MISSING_SEMICOLON' : 'Whoops! Missed a semicolon in the previous line(s) it seems. Add a semicolon after each statement.',
		'UNUSED_VARIABLE' : 'You have unused variables here. You might consider removing them.',
		'ARGUMENT_MISMATCH' : 'Your argument type and data type does not match here. Change either of them.'
	};
	
	var lines = [];
	
	for(var index = 0; index < errors.length; index++) {
		var error = errors[index];
		var line = parseInt(error['line']);
		var msg = error['feedback'].trim();
		
		if(typeof map[msg] !== 'undefined') {
			msg = arrange_lines(map[msg]);
		}
		
		lines.push({
			row: line-1,
			column: 1,
			text: msg,
			type: error['type']
		});
	}
	
	editor.getSession().setAnnotations(lines);
}

function arrange_lines(text) {
	
	var words = text.split(' ');
	var arranged = '';
	var line = '';
	
	for(var index = 0; index < words.length; index++) {
		line = line + words[index] + ' ';
		if(line.length >= 50) {
			arranged = arranged + line + '\n';
			line = '';
		}
	}
	
	arranged = arranged + line;
	
	return arranged;
}
