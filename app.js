/**
 * Main module of the application.
 * @module 		app
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 * @requires module:connect-timeout
 * @requires module:path
 * @requires module:static-favicon
 * @requires module:morgan
 * @requires module:cookie-parser
 * @requires module:body-parser
 * @requires module:http
 */

var express = require('express');
var timeout = require('connect-timeout'); // require use for add module

var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');
var acl = require('acl');

mongoose.connect('mongodb://' + process.env.NOSQL_USER + ':' + process.env.NOSQL_PASS + '@' + process.env.NOSQL_HOST + '/' + process.env.NOSQL_DB);

acl = new acl(new acl.mongodbBackend(mongoose.connection.db, 'acl_'));

require('./bootstrap/models')(mongoose);

var aclManager = require('./bootstrap/acl')(acl);

mongoose.connection.once('open', function() {
	aclManager.configure();
});

var app = express();

var http = require('http');
//http.globalAgent.maxSockets = 20;

var server = http.Server(app);

// view engine setup 
var viewEngine = require('express-dot-engine');
app.engine('dot', viewEngine.__express);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'dot');
app.set('view cache', true);

app.use(timeout('20s'));
app.use(favicon());
app.use(logger('dev')); //use for both get and post
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(require('./bootstrap/session'));

app.use(aclManager.controller);

app.use(function(req, res, next) {
	var database = require('./bootstrap/database');
    req.dbPool = database.pool;
    req.dbquery = database.query;
    req.cacheData = require('./bootstrap/cache')(req.dbPool);
    req.nosql = mongoose;
    // Make globally available.
    global.sqlpool = req.dbPool;
    global.sqlquery = req.dbquery;
    global.sqlcache = req.cacheData;
    global.nosql = req.nosql;
    next();
});

app.use(haltOnTimedout);
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

function haltOnTimedout(req, res, next){
	if (!req.timedout) next();
}

/////////////////////////// ROUTES CONFIGURATION ////////////////////////////

var index = require('./routes/index');
var pager = require('./routes/pager');
var accounts = require('./routes/accounts/index')(acl);
var control = require('./routes/control');
var tasks = require('./routes/tasks');
var admin = require('./routes/admin');
var editor = require('./routes/editor');
var problems = require('./routes/problems/index');
var evts = require('./routes/events');
var dataviz = require('./routes/dataviz/index');
var codebook = require('./routes/codebook');
var tests = require('./routes/tests/index');
var notifications = require('./routes/notifications');

app.use('/', index);
app.use('/pager', pager);
app.use('/accounts', accounts);
app.use('/control', control);
app.use('/tasks', tasks);
app.use('/admin', admin);
app.use('/editor', editor);
app.use('/problems', problems);
app.use('/events', evts);
app.use('/dataviz', dataviz);
app.use('/codebook', codebook);
app.use('/tests', tests);
app.use('/notifications', notifications);

/////////////// ERROR MANAGEMENT///////////////

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
app.use(function(err, req, res, next) {
	console.log(err);
	res.status(err.status || 500);
	res.render('error', {
        message: err.message,
        error: err
    });
});

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	console.log(err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = server;
