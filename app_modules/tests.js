var tests = {};

var moduleBase = '.';

function simulateRandomHomeFetch(callback) {
	var query = "SELECT id FROM account WHERE type='STUDENT'";
	tests.cache(query, [], function(err, rows) {
		if(err) callback(err, false);
		else {
			var index = Math.round( Math.random() * (rows.length - 1) );
			var uid = rows[index].id;
			// Include files.
			var events = require(moduleBase  + '/events')(tests.db, tests.cache);
			var pager = require(moduleBase  + '/pager')(tests.db, tests.cache);
			var statistics = require(moduleBase  + '/statistics')(tests.db, tests.cache);
			var grading = require(moduleBase  + '/grading')(tests.db, tests.cache);
			var async = require('async');
			// Run modules.
			async.parallel([
		        function(callback) {
		        	events.getOngoingEvent(uid, callback);
		        },
		        function(callback) {
		        	statistics.getCourseStatistics(uid, callback);
		        },
		        function(callback) {
		        	grading.getScoreCard(uid, callback);
		        },
		        function(callback) {
		        	pager.getMessageNotifications(uid, callback);
		        }
		    ], callback);
		}
	});
}

tests.simulateRandomHomeFetch = simulateRandomHomeFetch;

module.exports = function(db, cache) {
	
	tests.db = db;
	tests.cache = cache;
	
	return tests;
}