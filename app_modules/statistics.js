/**
 * Get statistical information.
 * @module 		app_modules/statistics
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var statistics = {};

/**
 * 
 * @param eventID
 * @param callback
 */
function getScoreStats(eventID, callback) {
	var query = "SELECT user_id,assignment_id,problem_id,relative,mean,sd FROM score WHERE event_id=? AND assignment_id IN " +
			"(SELECT id FROM assignment WHERE event_id=score.event_id AND is_submitted=1) ORDER BY relative";
	statistics.cache(query, [eventID], function(err, data) {
		if(err) callback(err, null);
		else {
			var scores = {};
			for(var i = 0; i < data.length; i++) {
				if(typeof scores[data[i].problem_id] === 'undefined')
					scores[data[i].problem_id] = [];
				scores[data[i].problem_id].push(data[i]);
			}
			callback(null, scores);
		}
	});
}

/**
 * 
 * @param eventID
 * @param callback
 */
function getPerformanceStatistics(eventID, callback) {
	var async = require('async');
	async.parallel([
        function(callback) {
        	var query = "SELECT assignment_id,user_id,problem_id,COUNT(*) AS failure FROM attempt INNER JOIN assignment ON assignment.id=assignment_id " +
        			"WHERE event_id=? AND success=0 GROUP BY assignment_id";
        	statistics.cache(query, [eventID], function(err, rows) {
        		callback(err, rows);
        	});
        },
        function(callback) {
        	var query = "SELECT assignment_id,user_id,problem_id,COUNT(*) AS success FROM attempt INNER JOIN assignment ON assignment.id=assignment_id " +
        			"WHERE event_id=? AND success=1 GROUP BY assignment_id";
        	statistics.cache(query, [eventID], function(err, rows) {
        		callback(err, rows);
        	});
        },
        function(callback) {
        	var query = "SELECT event_name,COUNT(*) AS submissions FROM assignment WHERE event_id=? AND is_submitted=1";
        	statistics.cache(query, [eventID], function(err, rows) {
        		if(err || rows.length == 0)
        			callback(err, null);
        		else 
        			callback(null, rows[0]);
        	});
        },
        function(callback) {
        	var query = "SELECT id,name FROM account";
        	statistics.cache(query, [], function(err, rows) {
        		callback(err, rows);
        	});
        }
    ], function(err, data) {
		if(err) callback(err, null);
		else callback(null, data);
	});
}

function getGradeStatistics(eventID, callback) {
	var query = "SELECT event_name,user_id,problem_id,name,question,score,max_marks " +
			"FROM assignment INNER JOIN account ON account.id=assignment.user_id " +
			"WHERE event_id=? AND account.type='STUDENT'";
	statistics.cache(query, [eventID], function(err, rows) {
		if(err) {
			callback(err, null);
		} else {
			if(rows.length == 0 || rows[0].event_name === null) {
				callback(null, null);
			} else {
				var eventName = rows[0].event_name;
				var data = {
					'eventId': eventID,
					'eventName': eventName,
					'numAssignments': rows.length,
					'performance': [],
					'maxQuestions': 0,
					'questions': []
				};
				var userPerf = {};
				for(var i = 0; i < rows.length; i++) {
					if(typeof userPerf[rows[i].user_id] === 'undefined')
						userPerf[rows[i].user_id] = { id: rows[i].user_id, name: rows[i].name, results: [] };
					userPerf[rows[i].user_id].results.push({
						question: rows[i].question,
						problem: rows[i].problem_id,
						score: rows[i].score,
						maxscore: rows[i].max_marks
					});
				}
				for(var uid in userPerf) {
					if(userPerf[uid].results.length > data.maxQuestions)
						data.maxQuestions = userPerf[uid].results.length; 
					data.performance.push(userPerf[uid]);
				}
				for(var i = 1; i <= data.maxQuestions; i++)
					data.questions.push('Q' + i);
				callback(err, data);
			}
		}
	});
}

/**
 * Fetches summary information of all submitted and non-submitted problems, for a specific student.
 * 
 * @param studentID
 * @param callback
 */
function getCourseStatistics(studentID, callback) {
	var query = "SELECT event_id,assignment.id,is_submitted,type FROM assignment INNER JOIN event " +
		"ON assignment.event_id=event.id " +
		"WHERE event_id IN " +
		"(SELECT event_id FROM schedule WHERE id IN (SELECT schedule_id FROM slot WHERE section=(SELECT section FROM account WHERE id=?)) AND " +
		"time_start < CURRENT_TIMESTAMP) AND user_id=?";
	statistics.cache(query, [studentID, studentID], function(err, rows) {
		if(err) {
			callback(err, null);
			return;
		} else {
			var counts = {
				remaining: 0,
				submitted: 0,
				labs: {},
				exams: {},
				quizzes: {}
			};
			for(var i = 0; i < rows.length; i++) {
				if(rows[i].is_submitted == 1) counts.submitted++;
				else counts.remaining++;
				if(rows[i].type === 'EXAM') counts.exams[rows[i].event_id] = 1;
				else if(rows[i].type === 'LAB') counts.labs[rows[i].event_id] = 1;
				else if(rows[i].type === 'QUIZ') counts.quizzes[rows[i].event_id] = 1;
			}
			counts.exams = Object.keys(counts.exams).length;
			counts.labs = Object.keys(counts.labs).length;
			counts.quizzes = Object.keys(counts.quizzes).length;
			callback(null, counts);
		}
	});
}

statistics.getScoreStats = getScoreStats;
statistics.getPerformanceStatistics = getPerformanceStatistics;
statistics.getCourseStatistics = getCourseStatistics;
statistics.getGradeStatistics = getGradeStatistics;

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 */
module.exports = function(db, cache) {
	
	statistics.db = db;
	statistics.cache = cache;
	
	return statistics;
}