/**
 * Engine control panel.
 * @module 		app_modules/engine
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var engine = {};

/**
 * Fetches the control configuration for the system.
 * 
 * @param callback
 */
function getControlConfig(callback) {
	sqlpool.getConnection(function(err, con) {
		var query = "SELECT section,setting,value FROM configuration";
		con.query(query, [], function(err, rows) {
			if(err || rows.length == 0) callback(err, null);
			else {
				var config = {
					quotas: {},
					flags: [],
					tools: {},
					delays: {},
					others: []
				};
				for(var i = 0; i < rows.length; i++) {
					if(rows[i].section === 'EXECUTION_QUOTAS') {
						config.quotas[rows[i].setting] = eval('(' + rows[i].value + ')');
					} else if(rows[i].section === 'COMPILER_FLAGS') {
						config.flags = eval('(' + rows[i].value + ')');
					} else if(rows[i].section === 'TOOLS') {
						config.tools[rows[i].setting] = eval('(' + rows[i].value + ')');
					} else if(rows[i].section === 'DELAYS') {
						config.delays[rows[i].setting] = eval('(' + rows[i].value + ')');
					} else {
						config.others.push(rows[i]);
					}
				}
				callback(null, config);
			}
		});
		con.release();
	});
}

function getProgrammingEnvironments(callback) {
	
	var Environment = nosql.model('Environments');
	
	Environment.find().exec(function(err, data) {
		if(err)
			console.log(err);
		callback(err, data);
	});
}

/**
 * Updates plugin state.
 * 
 * @param tool
 * @param state
 * @param callback
 */
function updateToolState(tool, state, callback) {
	sqlpool.getConnection(function(err, con) {
		var query = "UPDATE configuration SET value=? WHERE setting=? AND section='TOOLS'";
		con.query(query, [JSON.stringify(state), tool], function(err, result) {
			if(err) callback(err, false);
			else callback(null, true);
		});
		con.release();
	});
}

/**
 * Updates compiler flags for the currently used compiler.
 * 
 * @param action
 * @param flag
 * @param callback
 */
function updateCompilerFlags(action, flag, callback) {
	sqlpool.getConnection(function(err, con) {
		var async = require('async');
		async.waterfall([
		   function(callback) {
			   var query = "SELECT value FROM configuration WHERE setting='flag'";
        	   con.query(query, [], function(err, rows) {
        		   if(err || rows.length == 0) callback(err, null);
        		   else callback(null, rows[0].value);
        	   });
           },
           function(flags, callback) {
        	   flags = eval('(' + flags + ')');
        	   if(action === 'add') {
        		   flags.push(flag);
        	   } else if(action === 'remove') {
        		   var pos = flags.indexOf(flag);
        		   flags.splice(pos, 1);
        	   }
        	   var query = "UPDATE configuration SET value=? WHERE setting='flag'";
        	   con.query(query, [JSON.stringify(flags)], function(err, result) {
        		   if(err) callback(err, false);
        		   else callback(null, true);
        	   });
           }
        ], function(err, result) {
			if(err) {
				callback(err, false);
			} else {
				callback(null, true);
			}
		});
		con.release();
	});
}

/**
 * Updates engine quotas for compilation and execution.
 * 
 * @param time
 * @param memory
 * @param callback
 */
function updateEngineQuotas(time, memory, callback) {
	sqlpool.getConnection(function(err, con) {
		var async = require('async');
		async.parallel([
            function(callback) {
            	var query = "UPDATE configuration SET value=? WHERE setting='time' AND section='EXECUTION_QUOTAS'";
            	con.query(query, [JSON.stringify(time)], function(err, result) {
    				if(err) callback(err, false);
    				else callback(null, true);
    			});
            },
            function(callback) {
            	var query = "UPDATE configuration SET value=? WHERE setting='memory' AND section='EXECUTION_QUOTAS'";
            	con.query(query, [JSON.stringify(memory)], function(err, result) {
    				if(err) callback(err, false);
    				else callback(null, true);
    			});
            }
        ], function(err, result) {
			if(err) callback(err, false);
			else callback(null, result[0] && result[1]);
		});
		con.release();
	});
}

/**
 * Update delay settings for compilation, execution and evaluation.
 * 
 * @param compile
 * @param execute
 * @param evaluate
 * @param callback
 */
function updateEngineDelays(compile, execute, evaluate, callback) {
	sqlpool.getConnection(function(err, con) {
		var async = require('async');
		async.parallel([
            function(callback) {
            	var query = "UPDATE configuration SET value=? WHERE setting='compilation' AND section='DELAYS'";
            	con.query(query, [JSON.stringify(compile)], function(err, result) {
    				if(err) callback(err, false);
    				else callback(null, true);
    			});
            },
            function(callback) {
            	var query = "UPDATE configuration SET value=? WHERE setting='execution' AND section='DELAYS'";
            	con.query(query, [JSON.stringify(execute)], function(err, result) {
    				if(err) callback(err, false);
    				else callback(null, true);
    			});
            },
            function(callback) {
            	var query = "UPDATE configuration SET value=? WHERE setting='evaluation' AND section='DELAYS'";
            	con.query(query, [JSON.stringify(evaluate)], function(err, result) {
    				if(err) callback(err, false);
    				else callback(null, true);
    			});
            }
        ], function(err, result) {
			if(err) callback(err, false);
			else callback(null, result[0] && result[1] && result[2]);
		});
		con.release();
	});
}

function updateEnvironments(settings, callback) {
	
	var Environment = nosql.model('Environments');
	
	for(var i = 0; i < settings.length; i++) {
		var env = settings[i];
		env.settings.compile = (env.settings.compile === 'true');
		Environment.update({_id: env.id}, {$set: env.settings}, function(err, record) {
			if(err)
				console.log(err);
			callback(err, record);
		});
	}
}

function getDefaultEnvironment(callback) {
	var Environment = nosql.model('Environments');
	
	Environment.findOne({'default': true}).exec(function(err, data) {
		if(err)
			console.log(err);
		callback(err, data);
	});
}

engine.getControlConfig = getControlConfig;
engine.getProgrammingEnvironments = getProgrammingEnvironments;
engine.updateToolState = updateToolState;
engine.updateCompilerFlags = updateCompilerFlags;
engine.updateEngineQuotas = updateEngineQuotas;
engine.updateEngineDelays = updateEngineDelays;
engine.updateEnvironments = updateEnvironments;
engine.getDefaultEnvironment = getDefaultEnvironment;

module.exports = engine;