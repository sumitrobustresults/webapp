/**
 * Manages assignments for courses.
 * @module 		app_modules/assignments
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var assignments = {};

/**
 * Fetches codebook containing all attempted problems in practice and course events, along with their submission status.
 * 
 * @param userID
 * @param callback
 */
function getCodeBook(userID, callback) {
	var query = "SELECT env,assignment.id,problem_id,title,category,is_practice,event_id,event_name,question,is_submitted " +
		"FROM assignment INNER JOIN problem ON assignment.problem_id=problem.id WHERE user_id=? " +
		"AND (event_id IN (SELECT id FROM event WHERE NOT EXISTS (SELECT * FROM schedule WHERE schedule.event_id=event.id HAVING CURRENT_TIMESTAMP < MAX(time_stop))) OR event_id IS NULL)";
	sqlquery(query, [userID], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			var codeBook = {
				practice: {},
				events: {}
			};
			for(var i = 0; i < rows.length; i++) {
				if(rows[i].is_practice == 1) {
					if(typeof codeBook.practice[rows[i].category] === 'undefined')
						codeBook.practice[rows[i].category] = [];
					codeBook.practice[rows[i].category].push(rows[i]);
				} else {
					if(typeof codeBook.events[rows[i].event_id] === 'undefined')
						codeBook.events[rows[i].event_id] = {name: rows[i].event_name, questions: []};
					codeBook.events[rows[i].event_id].questions.push(rows[i]);
				}
			}
			callback(null, codeBook);
		}
	});
}

/**
 * Fetches the code save history of an assignment.
 * 
 * @param assignmentID
 * @param callback
 */
function getCodeHistory(assignmentID, callback) {
	var async = require('async');
	sqlpool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			async.parallel([
                function(callback) {
                	var query = "SELECT id,contents,save_type,save_time " +
    					"FROM code WHERE assignment_id=? " +
    					"ORDER BY id DESC";
                	con.query(query, [assignmentID], function(err, rows) {
                		if(err) callback(err, null);
                		else {
                			callback(null, rows);
                		}
                	});
                },
                function(callback) {
                	var query = "SELECT id,code_id,compiled " +
    					"FROM compilation WHERE assignment_id=?";
                	con.query(query, [assignmentID], function(err, rows) {
                		if(err) callback(err, null);
                		else callback(null, rows);
                	});
                },
                function(callback) {
                	var query = "SELECT execution.id,code_id,result,input,output,execute_time " +
    					"FROM execution INNER JOIN code ON execution.code_id=code.id WHERE assignment_id=?";
                	con.query(query, [assignmentID], function(err, rows) {
                		if(err) callback(err, null);
                		else callback(null, rows);
                	});
                },
                function(callback) {
                	var query = "SELECT id,category,statement,env FROM problem " +
    					"WHERE id=(SELECT problem_id FROM assignment WHERE id=?)";
                	con.query(query, [assignmentID], function(err, rows) {
                		if(err || rows.length == 0) {
                			callback(err, null);
                		} else {
                			var problem = rows[0];
                			var Environment = nosql.model('Environments');
                			Environment.findOne({name: problem.env}).exec(function(err, data) {
                				if(err){
                					console.log(err);
                					callback(err, null);
                				} else {
                					problem.env = data;
                					callback(null, problem);
                				}
                			});
                		}
                	});
                },
                function(callback) {
                	var query = "SELECT is_submitted,submission,question FROM assignment WHERE id=?";
                	con.query(query, [assignmentID], function(err, rows) {
                		if(err || rows.length == 0) callback(err, null);
                		else callback(null, rows[0]);
                	});
                }
            ], function(err, dataSet) {
    			if(err) {
    				callback(err, null);
    			} else {
    				callback(null, {
    					history: dataSet[0],
    					compilations: dataSet[1],
    					executions: dataSet[2],
    					problem: dataSet[3],
    					assignment: dataSet[4]
    				});
    			}
    		});
    		con.release();
		}
	});
}

/**
 * Fetches the last saved code corresponding to an assignment.
 * 
 * @param studentID
 * @param assignmentID
 * @param callback
 */
function getLastSavedCode(studentID, assignmentID, callback) {
	var query = "SELECT id,user_id,contents,save_time FROM code WHERE assignment_id=? ORDER BY save_time DESC LIMIT 1";
	sqlquery(query, [assignmentID], function(err, rows) {
		if(err) {
			callback(err, null);
		} else if(rows.length == 0) {
			callback(null, null);
		} else if(rows[0].user_id != studentID) {
			callback(null, null);
		} else {
			callback(null, rows[0]);
		}
	});
}

/**
 * Fetches the problem statement corresponding to an assignment.
 * 
 * @param assignmentID
 * @param callback
 */
function getAssignmentProblem(assignmentID, callback) {
	var query = "SELECT statement,env FROM problem WHERE id=(SELECT problem_id FROM assignment WHERE id=?)";
	sqlquery(query, [assignmentID], function(err, rows) {
		if(err) {
			callback(err, null);
			return;
		} else if(rows.length == 0) {
			callback(null, null);
		} else {
			var problem = rows[0];
			var Environment = nosql.model('Environments');
			Environment.findOne({ name: problem.env }).exec(function(err, data) {
				if(err)
					console.log(err);
				problem.env = data;
				callback(err, problem);
			});
		}
	});
}

/**
 * Fetches the event,question and maximum marks for an assignment.
 * 
 * @param assignmentID
 * @param callback
 */
function getAssignmentDetails(assignmentID, callback) {
	var query = "SELECT event_name,question,max_marks FROM assignment WHERE id=?";
	sqlquery(query, [assignmentID], function(err, rows) {
		if(err) {
			callback(err, null);
			return;
		} else if(rows.length == 0) {
			callback(null, null);
		} else {
			callback(null, rows[0]);
		}
	});
}

/**
 * Saves code for an assignment.
 * 
 * @param userID
 * @param assignmentID
 * @param source
 * @param trigger
 * @param callback
 */
function saveCode(userID, assignmentID, source, trigger, callback) {
	
	var triggerID =  0;
	
	if(trigger === 'auto') {
		triggerID = 0;
	} else if(trigger === 'manual') {
		triggerID = 1;
	} else if(trigger === 'submit') {
		triggerID = 2;
	} else if(trigger === 'compile') {
		triggerID = 3;
	} else if(trigger === 'paste') {
		triggerID = 4;
	} else if(trigger === 'revert') {
		triggerID = 6;
	} else {
		callback(null, null);
		return;
	}
	
	var query = "INSERT INTO code (user_id,assignment_id,contents,save_type) " +
		"VALUES (?,?,?,?)";
	sqlquery(query, [userID, assignmentID, source, triggerID], function(err, result) {
		if(err) {
			callback(err, null);
			return;
		} else {
			var codeID = result.insertId;
			if(triggerID == 2) {
				query = "UPDATE assignment SET is_submitted=1,submission=? WHERE id=?";
				sqlquery(query, [codeID, assignmentID], function(err, result) {
					if(err) console.log(err);
				});
			} 
			if(process.env.AUDIT) {
				var audit = require('./audit')(db);
				if(triggerID == 1) audit.logActivity(userID, 'CODE', 'SAVE', result.insertId);
				else if(triggerID == 2) audit.logActivity(userID, 'CODE', 'SUBMIT', result.insertId);
				else if(triggerID == 4) audit.logActivity(userID, 'CODE', 'PASTE', result.insertId);
				else if(triggerID == 6) audit.logActivity(userID, 'CODE', 'REVERT', result.insertId);
			}
			callback(null, codeID);
		}
	});
}

/**
 * Fetches/Creates a practice assignment corresponding to an user and problem. 
 * 
 * @param userID
 * @param problemID
 * @param callback
 */
function getPracticeAssignment(userID, problemID, callback) {
	var async = require('async');
	sqlpool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			async.waterfall([
                 function(callback) {
                 	var query = "SELECT id FROM assignment WHERE user_id=? AND problem_id=?";
                 	con.query(query, [userID, problemID], function(err, rows) {
         				if(err) {
         					console.log(err);
         					callback(err, null);
         				} else {
         					if(rows.length == 0) callback(null, null);
         					else callback(null, rows[0].id);
         				}
         			});
                 },
                 function(assignmentID, callback) {
                 	async.series([
         	             function(callback) {
         	            	 if(assignmentID == null) {
     	                		var query = "INSERT INTO assignment (user_id,problem_id) VALUES (?,?)";
     	            			con.query(query, [userID, problemID], function(err, result) {
     	            				if(err) {
     	            					console.log(err);
     	            					callback(err, null);
     	            				} else {
     	            					query = "INSERT INTO code (assignment_id,user_id,contents,save_type) " +
     	            							"SELECT ?,?,(SELECT template FROM problem WHERE id=(SELECT problem_id FROM assignment WHERE id=?)),0";
     	            					con.query(query, [result.insertId, userID, result.insertId]);
     	            					assignmentID = result.insertId;
     	            					callback(null, result.insertId);
     	            				}
     	            			});
     	                	} else {
     	                		callback(null, assignmentID);
     	                	}
         	             },
         	             function(callback) {
         	            	var query = "SELECT id,contents,save_time FROM code WHERE assignment_id=? ORDER BY save_time DESC LIMIT 1";
     	            		con.query(query, [assignmentID], function(err, rows) {
     	        				if(err) {
     	        					console.log(err);
     	        					callback(err, null);
     	        				} else {
     	        					if(rows.length == 0) callback(null, null);
     	        					else callback(null, rows[0]);
     	        				}
     	        			});
         	             }
     	              ], function(err, result) {
                 		if(err) {
                 			console.log(err);
                 			callback(err, null);
                 		} else {
                 			callback(null, {
                 				assignmentID: result[0],
                 				lastSave: result[1]
                 			});
                 		}
                 	});
                 }
              ], function(err, result) {
     			if(err) {
     				callback(err, null);
     			} else {
     				callback(null, result);
     			}
     		});
     		con.release();
		}
	});
}

/**
 * Fetches the submission and grading status for all submissions corresponding to a schedule.
 * 
 * @param scheduleID
 * @param offset
 * @param callback
 */
function getSubmissionsForSchedule(scheduleID, offset, callback) {
	if(isNaN(offset)) {
		callback(null, null);
	} else {
		var query = "SELECT id,is_submitted,question,grading_time FROM assignment " +
			"WHERE event_id=(select event_id FROM schedule where id=?) " +
			"AND user_id IN (SELECT id FROM account WHERE section IN " +
				"(SELECT section FROM slot WHERE schedule_id=?)) " +
			"ORDER BY id LIMIT " + offset + ",250";
		sqlcache(query, [scheduleID, scheduleID], callback);
	}
}

/**
 * Deletes a code save from an assignment.
 * 
 * @param codeID
 * @param callback
 */
function deleteCodeSave(codeID, callback) {
	var query = "DELETE FROM code WHERE id=?";
	sqlquery(query, [codeID], function(err, result) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else callback(null, true);
	});
}

/**
 * Get overview of assignment submission.
 * 
 * @param assignmentID
 * @param callback
 */
function getSubmission(assignmentID, callback) {
	var query = "SELECT env,assignment.id AS assignment_id,problem.id AS problem_id,title,category,statement,score,max_marks,comments,event_name,question,is_submitted,regrade_requested,regrade_reason,regrade_reqtime," +
		"(SELECT CASE WHEN is_submitted=1 THEN (SELECT contents FROM code WHERE id=submission) ELSE (SELECT contents FROM code WHERE assignment_id=assignment.id ORDER BY save_time DESC LIMIT 1) END) AS code " +
		"FROM assignment INNER JOIN problem ON assignment.problem_id=problem.id WHERE assignment.id=?";
	sqlquery(query, [assignmentID], function(err, rows) {
		if(err || rows.length == 0) {
			callback(err, null);
		} else {
			var submission = rows[0];
			var Environment = nosql.model('Environments');
			Environment.findOne({name: submission.env}).exec(function(err, data) {
				if(err){
					console.log(err);
					callback(err, null);
				} else {
					submission.env = data;
					callback(null, submission);
				}
			});
		}
	});
}

/**
 * Assign problems to students for a course event with respect to some algorithm.
 * 
 * @param algo
 * @param eventID
 * @param problems
 * @param callback
 */
function assignProblems(algo, eventID, problems, callback) {
	var async = require('async');
	sqlpool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else {
			if(algo == 1) {
				async.waterfall([
	                 function(callback) {
	                	 var query = "SELECT name FROM event WHERE id=?";
	                	 con.query(query, [eventID], function(err, data) {
	                		 callback(err, data);
	                	 });
	                 },
	                 function(eventName, callback) {
	                	 eventName = eventName[0]['name'];
	                	 async.each(problems, function(pid, callback) {
	                		 var query = "INSERT INTO assignment (user_id,event_id,event_name,question,problem_id,max_marks) " +
	                	 		"SELECT id,?,?,?,?,? FROM account";
	                		 con.query(query, [eventID, eventName, (problems.indexOf(pid) + 1), pid, 20], function(err, result) {
	                			 callback(err, (err == null));
	                		 });
	                	 }, function(err, data) {
	                		 if(err) callback(err, false);
	                		 else callback(null, true);
	                	 });
	                 },
	                 function(result, callback) {
	                	 var query = "INSERT INTO code (assignment_id,user_id,contents,save_type) " +
	            	 		"SELECT id,user_id,(SELECT template FROM problem WHERE id=problem_id),0 FROM assignment WHERE event_id=?";
	                	 con.query(query, [eventID], function(err, result) {
	                		 callback(err, (err == null));
	                	 });
	                 }
	             ], function(err, result) {
					if(err) callback(err, false);
					else callback(null, true);
				});
			} else if(algo == 2) {
				try {
					problems = eval('(' + problems + ')');
					async.waterfall([
		                 function(callback) {
		                	 var query = "SELECT name FROM event WHERE id=?";
		                	 con.query(query, [eventID], function(err, data) {
		                		 callback(err, data);
		                	 });
		                 },
		                 function(eventName, callback) {
		                	 eventName = eventName[0]['name'];
		                	 var schedules = [];
		                	 for(var sid in problems)
		                		 schedules.push(sid);
		                	 async.each(schedules, function(sid, callback) {
		                		 var set = problems[sid];
		                		 async.each(set, function(pid, callback) {
			                		 var query = "INSERT INTO assignment (user_id,event_id,event_name,question,problem_id,max_marks) " +
			                	 		"SELECT id,?,?,?,?,? FROM account WHERE section IN (SELECT section FROM slot WHERE schedule_id=?)";
			                		 con.query(query, [eventID, eventName, (set.indexOf(pid) + 1), pid, 20, sid], function(err, result) {
			                			 callback(err, (err == null));
			                		 });
			                	 }, function(err, data) {
			                		 if(err) callback(err, false);
			                		 else callback(null, true);
			                	 });
		                	 }, function(err, data) {
		                		 if(err) callback(err, false);
		                		 else callback(null, true);
		                	 });
		                 },
		                 function(result, callback) {
		                	 var query = "INSERT INTO code (assignment_id,user_id,contents,save_type) " +
		            	 		"SELECT id,user_id,(SELECT template FROM problem WHERE id=problem_id),0 FROM assignment WHERE event_id=?";
		                	 con.query(query, [eventID], function(err, result) {
		                		 callback(err, (err == null));
		                	 });
		                 }
		             ], function(err, result) {
						if(err) callback(err, false);
						else callback(null, true);
					});
				} catch(err) {
					callback(null, false);
				}
			} else {
				callback(null, false);
			}
			con.release();
		}
	});
}

assignments.getCodeBook = getCodeBook;
assignments.getCodeHistory = getCodeHistory;
assignments.getLastSavedCode = getLastSavedCode;
assignments.getAssignmentProblem = getAssignmentProblem;
assignments.getAssignmentDetails = getAssignmentDetails;
assignments.saveCode = saveCode;
assignments.getPracticeAssignment = getPracticeAssignment;
assignments.getSubmissionsForSchedule = getSubmissionsForSchedule;
assignments.deleteCodeSave = deleteCodeSave;
assignments.getSubmission = getSubmission;
assignments.assignProblems = assignProblems;

module.exports = assignments;
