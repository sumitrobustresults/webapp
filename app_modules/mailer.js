/**
 * Manages mailing for the course.
 * @module 		app_modules/mailer
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var mailer = {};

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 */
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

var NodeMailer = require('nodemailer');
var MandrillTransport = require('nodemailer-mandrill-transport');

var transporter = NodeMailer.createTransport(MandrillTransport({
	auth: {
		apiKey: ''
	}
}));

/**
 * Sends a mail.
 * sandbox1bcff145cafc4bc9b5880241d2710f1c.mailgun.org
 * @param recipient
 * @param subject
 * @param msgText
 * @param msgHTML
 * @param callback
 */
function send(recipient, subject, msgText, msgHTML, callback) {
	
	var emailID = process.env.MAIL_SENDER_EMAIL;
	var senderName = process.env.MAIL_SENDER_NAME;
	
	console.log(process.env.MAIL_USER);
	console.log(process.env.MAIL_PASS);
	
	var options = {
		from: senderName + ' ' + '<' + emailID + '>',
		to: recipient,
		subject: subject,
		text: msgText,
		html: msgHTML
	};
	
	transporter.sendMail(options, function(error, info) {
		if(error) {
			console.log(error);
			callback(false);
		} else {
			callback(true);
		}
	});
}

mailer.send = send;

module.exports = mailer;