/**
 * Community support system module.
 * @module 		app_modules/pager
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var pager = {};

/**
 * Creates a message thread.
 * 
 * @param userID
 * @param context
 * @param reference
 * @param visibility
 * @param message
 * @param callback
 */
function createThread(userID, context, reference, visibility, message, callback) {
	this.db.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			var query = "INSERT INTO thread (initiator,context,reference,visibility) VALUES (?,?,?,?)";
			con.query(query, [userID, context, reference, visibility], function(err, result) {
				if(err) {
					callback(err, null);
				} else {
					query = "INSERT INTO message (composer,thread_id,contents) VALUES (?,?,?)";
					con.query(query, [userID, result.insertId, message], function(err, result) {
						if(err) {
							callback(err, null);
						} else {
							// Notify
							con.query("INSERT INTO notification (recipient,event,reference) SELECT id,'CREATE_THREAD',? FROM account WHERE type='ADMIN' AND id <> ?", [userID, userID]);
							callback(null, result.insertId); 
						}
					});
				}
			});
			con.release();
		}
	});
}

/**
 * Responds to a message thread created by some student.
 * 
 * @param userID
 * @param threadID
 * @param message
 * @param callback
 */
function respond(userID, threadID, message, callback) {
	this.db.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			var query = "INSERT INTO message (composer,thread_id,contents) VALUES (?,?,?)";
			con.query(query, [userID, threadID, message], function(err, result) {
				if(err) {
					callback(err, null);
				} else {
					con.query("INSERT INTO notification (recipient,event,reference) SELECT composer,'REPLY_THREAD',? FROM message WHERE thread_id=? AND composer <> ?", [userID, threadID, userID]);
					callback(null, result.insertId);
				}
			});
			con.release();
		}
	});
}

/**
 * Deletes a message.
 * 
 * @param userID
 * @param messageID
 * @param callback
 */
function removeMessage(userID, messageID, callback) {
	var query = "DELETE FROM message WHERE id=? AND composer=?";
	pager.query(query, [messageID, userID], function(err, result) {
		if(err) {
			callback(err, false);
		} else {
			callback(null, true);
		}
	});
}

/**
 * Fetches all message threads in the database.
 * 
 * @param callback
 */
function getMessageThreads(callback) {
	var query = "SELECT id,(SELECT contents FROM message WHERE thread_id=thread.id ORDER BY create_time LIMIT 1) AS contents,create_time,initiator," +
		"(SELECT name FROM account WHERE id=initiator) AS name,context,reference,(SELECT COUNT(id) FROM message WHERE thread_id=thread.id) AS count " +
		"FROM thread ORDER BY create_time DESC";
	pager.query(query, [], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			callback(null, rows);
		}
	});
}

/**
 * Fetches all message threads initiated by a specific user.
 * 
 * @param userID
 * @param callback
 */
function getUserMessageThreads(userID, callback) {
	var query = "SELECT id,(SELECT contents FROM message WHERE thread_id=thread.id ORDER BY create_time LIMIT 1) AS contents,create_time,initiator," +
		"(SELECT name FROM account WHERE id=initiator) AS name,context,reference,(SELECT COUNT(id) FROM message WHERE thread_id=thread.id) AS count " +
		"FROM thread WHERE initiator=? ORDER BY create_time DESC";
	pager.query(query, [userID], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			callback(null, rows);
		}
	});
}

/**
 * Fetches all messages corresponding to a thread.
 * 
 * @param threadID
 * @param callback
 */
function getMessages(threadID, callback) {
	var query = "SELECT message.id,composer,name,contents,message.create_time " +
		"FROM message INNER JOIN account ON account.id=composer WHERE thread_id=? ORDER BY create_time DESC";
	pager.query(query, [threadID], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			rows.splice(rows.length - 1, 1);
			callback(null, rows);
		}
	});
}

/**
 * Fetches message notifications for a specific user.
 * 
 * @param userID
 * @param callback
 */
function getMessageNotifications(userID, callback) {
	var query = "SELECT event,trigger_time,name FROM notification INNER JOIN account ON notification.reference=account.id AND seen=0 " +
		"AND event IN ('CREATE_THREAD', 'REPLY_THREAD') AND recipient=?";
	pager.query(query, [userID], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			callback(null, rows);
		}
	});
}

/**
 * Marks a notification as seen by a user.
 * 
 * @param userID
 * @param callback
 */
function markSeen(userID, callback) {
	var query = "UPDATE notification SET seen=1 WHERE recipient=? AND event IN ('CREATE_THREAD', 'REPLY_THREAD')";
	pager.query(query, [userID], function(err, result) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else {
			callback(null, true);
		}
	});
}

pager.createThread = createThread;
pager.respond = respond;
pager.removeMessage = removeMessage;
pager.getMessageThreads = getMessageThreads;
pager.getUserMessageThreads = getUserMessageThreads;
pager.getMessages = getMessages;
pager.getMessageNotifications = getMessageNotifications;
pager.markSeen = markSeen;

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 */
module.exports = function(db, cache, query) {
	
	pager.db = db;
	pager.cache = cache;
	pager.query = query;
	
	return pager;
}