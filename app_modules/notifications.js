/**
 * Notifications module.
 * @module 		app_modules/notifications
 * @author 		Rajdeep Das
 * @copyright	(c) 2015 Rajdeep Das
 * @license		All rights reserved.
 */
var notifications = {};

var contexts = {
	'REGRADE_REQUEST': 'There is a regrading request for assignment #%s.'
};

var groups = {
	'ADMINS': "WHERE type='ADMIN'",
	'INSTRUCTORS': "WHERE type='ADMIN' AND admin_role=0",
	'TUTORS': "WHERE type='ADMIN' AND admin_role=1",
	'TAS': "WHERE type='ADMIN' AND admin_role=2",
	'STUDENTS': "WHERE type='STUDENT'"
};

/**
 * Send out notification to specific target.
 * @param context
 * @param source
 * @param target
 */
function notify(context, source, target) {
	
	if(typeof contexts[context] === 'undefined')
		return;
	
	var Notification = nosql.model('Notifications');
	
	var record = new Notification({
		context: context,
		source: source,
		target: target,
		message: contexts[context].replace('%s', source),
		seen: false,
		createTime: new Date()
	});
	record.save(function(err, record) {
		if(err)
			console.log(err);
	});
}

/**
 * Send out notifications to a group.
 * @param context
 * @param source
 * @param group
 */
function notifyGroup(context, source, group) {
	
	if(typeof contexts[context] === 'undefined' || typeof groups[group] === 'undefined')
		return;
	
	var Notification = nosql.model('Notifications');
	
	sqlquery("SELECT id FROM account " + groups[group], [], function(err, data) {
		if(err) {
			console.log(err);
		} else {
			var rows = [];
			for(var i = 0; i < data.length; i++)
				rows.push({
					context: context,
					source: source,
					target: data[i].id,
					message: contexts[context].replace('%s', source),
					seen: false,
					createTime: new Date()
				});
			Notification.collection.insert(rows, function(err, docs) {
				if(err)
					console.log(err);
			});
		}
	});
	
}

/**
 * Mark a notification as seen.
 * @param notificationID
 */
function markSeen(notificationID) {
	
	var Notification = nosql.model('Notifications');
	
	Notification.update({_id: notificationID}, {$set: {seen: true}}, function(err, record) {
		if(err)
			console.log(err);
	});
}

function markSeenAll(userID, callback) {
	
	var Notification = nosql.model('Notifications');
	
	Notification.update({target: userID, seen: false}, {$set: {seen: true}}, function(err, record) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else {
			callback(null, true);
		}
	});
}

/**
 * Fetch all notifications for a specific user.
 * @param userID
 * @param callback
 */
function getUnseenNotifications(userID, callback) {
	
	var Notification = nosql.model('Notifications');
	
	Notification.find({target: userID, seen: false})
		.sort({createTime: -1})
		.exec(function(err, data) {
			if(err)
				console.log(err);
			callback(err, data);
		});
}

function getAllNotifications(userID, callback) {
	
	var Notification = nosql.model('Notifications');
	
	Notification.find({target: userID})
		.sort({createTime: -1})
		.exec(function(err, data) {
			if(err)
				console.log(err);
			callback(err, data);
		});
}

notifications.notify = notify;
notifications.notifyGroup = notifyGroup;
notifications.markSeen = markSeen;
notifications.markSeenAll = markSeenAll;
notifications.getUnseenNotifications = getUnseenNotifications;
notifications.getAllNotifications = getAllNotifications;

module.exports = notifications;