/**
 * Manages user accounts for the course.
 * @module 		app_modules/accounts
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:bcrypt-nodejs
 */
var accounts = {};

var Bcrypt = require('bcrypt-nodejs');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

/**
 * @callback getDistinctSectionsCallback
 * @param {object} err The error object thrown (if any).
 * @param {string[]} sections An array of sections.
 */

/**
 * Fetch all the sections in the database.
 * @param {getDistinctSectionsCallback} callback
 */
function getDistinctSections(callback) {
	var query = "SELECT DISTINCT section FROM account WHERE section IS NOT NULL ORDER BY section";
	sqlcache(query, [], function(err, rows) {
		if(err) callback(err, null);
		else {
			var sections = [];
			for(var i = 0; i < rows.length; i++)
				sections.push(rows[i].section);
			callback(null, sections);
		}
	});
}

/**
 * @callback getUserTypeCallback
 * @param {object} err The error object thrown (if any).
 * @param {string} userType The type of user.
 */

/**
 * Fetch the type of user (student/admin) corresponding to an email ID.
 * @param {string} email
 * @param {getUserTypeCallback} callback
 */
function getUserType(email, callback) {
	var query = "SELECT type FROM account WHERE email=?";
	sqlcache(query, [email], function(err, rows) {
		if(err || rows.length == 0) callback(err, null);
		else callback(null, rows[0].type);
	});
}

/**
 * @callback studentLoginCallback
 * @param {object} profile {id,name,password,auth_type} | false
 */

/**
 * Authenticate a student user on the system.
 * @param {string} email
 * @param {string} password
 * @param {studentLoginCallback} callback
 */
function studentLogin(email, password, callback) {
	
	var async = require('async');
	
	async.waterfall([
       	function(callback) {
       		// Fetch authentication type.
       		var query = "SELECT id,name,password,auth_type,enabled FROM account WHERE email=?";
       		sqlquery(query, [email], function(err, rows) {
       			if(err) {
       				callback(err, null);
       			} else if(rows.length == 0) {
       				callback(null, null);
       			} else if(rows[0].enabled === 0) { 
       				callback(null, null);
       			} else {
       				callback(null, rows[0]);
       			}
       		});
       	},
       	function(student, callback) {
       		if(student === null) {
       			callback(null, false);
       		} else {
       			if(student.auth_type === 0) {
       				// CC Authentication.
       				ccAuthentication(email, password, function(authenticated) {
       					if(authenticated) {
       						callback(null, {
       							id: student.id,
     							name: student.name
     						});
       					} else callback(null, false);
       				});
       			} else {
       				// Password Authentication.
       				if(Bcrypt.compareSync(password, student.password)) {
 						callback(null, {
 							id: student.id,
 							name: student.name
 						});
 					} else callback(null, false);
       			}
       			// Audit login
       			if(process.env.AUDIT) require('./audit')(this.db).logActivity(student.id, 'AUTH', 'LOGIN', null);
       		}
       	}
   	], function(err, result) {
		if(err) {
			callback(false);
		} else {
			callback(result);
		}
	});
}

/**
 * @callback adminLoginCallback
 * @param {object} profile {id,name,type} | false
 */

/**
 * Authenticate an admin user on the system.
 * @param {string} email
 * @param {string} password
 * @param {adminLoginCallback} callback  
 * @example
 * accounts.adminLogin('admin@its.com', 'admin123', function(profile) {
 *     if(profile) {} // Authentication successful. Do something with information.
 *     else {} // Authentication failed.
 * });
 */
function adminLogin(email, password, callback) {
	var query = "SELECT id,name,admin_role AS type,password,enabled FROM account WHERE email=?";
	sqlquery(query, [email], function(err, rows) {
		if(err) callback(null);
		if(rows.length == 0) callback(false);
		else if(rows[0].enabled === 0) callback(false);
		else {
			if(Bcrypt.compareSync(password, rows[0].password)) {
				var user = {
					id: rows[0].id,
					name: rows[0].name,
					type: rows[0].type
				};
				callback(user);
			} else callback(false);
		}
	});
}

/**
 * @callback ccAuthenticationCallback
 * @param {bool} success Whether login succeeded or not.
 */

/**
 * Authenticate a student user using institute authentication mechanism at IIT Kanpur.
 * @param {string} email
 * @param {string} password
 * @param {ccAuthenticationCallback} callback
 */
function ccAuthentication(email, password, callback) {
	
	var Imap = require('imap');
	
	var id = email.substring(0, email.indexOf('@'));
	var imap = new Imap({
		user: id,
		password: password,
		host: 'newmailhost.cc.iitk.ac.in',
		port: '143',
		autotls: 'always'
	});
	
	imap.once('ready', function() {
		// authentication successful
		imap.destroy();
		callback(true);
	});
	
	imap.once('error', function(err) {
		if(typeof err.source != 'undefined' && err.source == 'authentication') {
			// authentication failure.
			imap.destroy();
			callback(false);
		}
	});
	
	imap.connect();
}

/**
 * @callback createAdminCallback
 * @param {uuid} userID A valid UUID or null (if an error occurred).
 */

/**
 * Create an admin user account.
 * @param {string} name
 * @param {string} email
 * @param {int}	type
 * @param {string} section
 * @param {createAdminCallback} callback
 */
function createAdmin(name, email, type, section, callback) {
	var hashed = createRandomHash();
	var query = "INSERT INTO account (id,name,email,type,section,admin_role,hash) " +
		"SELECT UUID(),?,?,'ADMIN',?,?,?";
	sqlquery(query, [name, email, section, type, hashed], function(err,result) {
		if(err) callback(null);
		else {
			//sqlquery("");
			/*var Mailer = require('./mailer')();
			var text = 'Dear ' + name + ',\n\nAn account has been created for you. '
				+ 'Your login credentials are:\n'
				+ 'Email: ' + email
				+ '\nPassword: ' + password
				+ '\n\nRegards,\nESC101-ITS Team';
			var html = '';
			Mailer.send(email, 'Account Created', text, html, function(sent) {
				if(sent) console.log('Email sent to: ' + email);
				else console.log('Email could not be sent to: ' + email);
			});*/
			callback(result.insertId);
		}
	});
}

/**
 * @callback deleteAdminCallback
 * @param {bool} success Whether deletion was successful or not.
 */

/**
 * Delete the account associated with an admin user.
 * @param {uuid} adminID
 * @param {deleteAdminCallback} callback
 */
function deleteAdmin(adminID, callback) {
	var query = "DELETE FROM account WHERE id=?"
	sqlquery(query, [adminID], function(err,result) {
		if(err) callback(false);
		else callback(true);
	});
}

/**
 * @callback modifyAdminCallback
 * @param {bool} success Whether role update was successful or not.
 */

/**
 * Update the role of an admin user.
 * @param {uuid} adminID
 * @param {int} role
 * @param {modifyAdminCallback} callback
 */
function modifyAdmin(adminID, role, callback) {
	var query = "UPDATE account SET admin_role=? WHERE id=?"
	sqlquery(query, [role, adminID], function(err,result) {
		if(err) callback(false);
		else callback(true);
	});
}

/**
 * @callback changeAdminPasswordCallback
 * @param {bool} success Whether password update was successful or not.
 */

/**
 * Change the password of an admin user.
 * @param {uuid} adminID
 * @param {string} oldpass
 * @param {string} newpass
 * @param {changeAdminPasswordCallback} callback
 */
function changeAdminPassword(adminID, oldpass, newpass, callback) {
	newpass = Bcrypt.hashSync(newpass);
	var query = "SELECT password FROM account WHERE id=?";
	sqlquery(query, [adminID], function(err, rows) {
		if(err || rows.length == 0) callback(false);
		else {
			if(Bcrypt.compareSync(oldpass, rows[0].password)) {
				query = "UPDATE account SET password=? WHERE id=?";
				sqlquery(query, [newpass, adminID], function(err,result) {
					if(err) callback(false);
					else callback(true);
				});
			} else callback(false);
		}
	});
}

/**
 * @callback changeAdminNameCallback
 * @param {bool} success Whether name update was successful or not.
 */

/**
 * Update the name of an admin user.
 * @param {uuid} adminID
 * @param {string} name
 * @param {changeAdminNameCallback} callback
 */
function changeAdminName(adminID, name, callback) {
	var query = "UPDATE account SET name=? WHERE id=?"
	sqlquery(query, [name, adminID], function(err,result) {
		if(err) callback(false);
		else callback(true);
	});
}

/**
 * @callback getAdminUsersCallback
 * @param {object} err The error object thrown (if any).
 * @param {array} adminList Array of {id,email,name,section,type} | null
 */

/**
 * Fetch a list of all admin users.
 * @param {getAdminUsersCallback} callback
 */
function getAdminUsers(callback) {
	var query = "SELECT id,enabled,email,name,section,admin_role AS type,hash FROM account WHERE type='ADMIN'";
	sqlquery(query, [], function(err, rows) {
		if(err) callback(err, null);
		else callback(null, rows);
	});
}

/**
 * @callback createStudentUserCallback
 * @param {object} err The error object thrown (if any).
 * @param {bool} success Whether accoutn creation was successful or not.
 */

/**
 * Create a student user account.
 * @param {string} emailID
 * @param {createStudentUserCallback} callback
 */
function createStudentUser(emailID, callback) {
	var hash = createRandomHash();
	var query = "INSERT INTO account (id,email,type,auth_type,hash) SELECT UUID(),?,'STUDENT',0,?";
	sqlquery(query, [emailID, hash], function(err, result) {
		if(err) callback(err, false);
		else callback(null, true);
	});
}

/**
 * @callback getStudentUsersCallback
 * @param {object} err The error object thrown (if any).
 * @param {array} studentList Array of {id,email,roll,name,auth_type,section} | null
 */

/**
 * Fetch a list of all student users in decreasing order of creation time.
 * @param {getStudentUsersCallback} callback 
 */
function getStudentUsers(callback) {
	var query = "SELECT id,enabled,email,roll,name,type,auth_type,section,hash FROM account ORDER BY create_time DESC";
	sqlquery(query, [], function(err, rows) {
		if(err) callback(err, null);
		else callback(null, rows);
	});
}

/**
 * @callback updateStudentInfoCallback
 * @param {object} err The error object thrown (if any).
 * @param {bool} success Whether profile update was successful or not.
 */

/**
 * Update user profile information of a student.
 * @param {uuid} userID
 * @param {string} email
 * @param {string} roll
 * @param {string} section
 * @param {int} authType
 * @param {updateStudentInfoCallback} callback
 */
function updateStudentInfo(userID, email, roll, name, section, authType, callback) {
	var hash = null;
	var enabled = 1;
	if(parseInt(authType) === 1) {
		hash = createRandomHash();
		enabled = 0;
	}
	var query = "UPDATE account SET email=?,roll=?,name=?,auth_type=?,section=?,hash=?,enabled=? WHERE id=?";
	sqlquery(query, [email,roll,name,authType,section,hash,enabled,userID], function(err, result) {
		if(err) callback(err, false);
		else {
			callback(null, true);
		}
	});
}

/**
 * @callback deleteStudentCallback
 * @param {object} err The error object thrown (if any).
 * @param {bool} success Whether account deletion was successful or not.
 */

/**
 * Delete the account associated with a student user.
 * @param {uuid} userID
 * @param {deleteStudentCallback} callback
 */
function deleteStudent(userID, callback) {
	var query = "DELETE FROM account WHERE id=?";
	sqlquery(query, [userID], function(err, result) {
		if(err) callback(err, false);
		else callback(null, true);
	});
}

function setNewPassword(hash, password, callback) {
	var newpass = Bcrypt.hashSync(password);
	var query = "UPDATE account SET password=?,enabled=1,hash=NULL WHERE hash=?";
	sqlquery(query, [newpass, hash], function(err, result) {
		if(err) callback(err, false);
		else callback(null, true);
	});
}

function resetPassword(userID, callback) {
	var hash = createRandomHash();
	var query = "UPDATE account SET enabled=0,hash=? WHERE id=?";
	sqlquery(query, [hash, userID], function(err, result) {
		if(err) callback(err, false);
		else callback(null, true);
	});
}

function createRandomHash() {
	
	var part1 = parseInt(Math.random() * 1000);
	var part2 = parseInt(Math.random() * 1000);
	var part3 = parseInt(Math.random() * 1000);
	
	var hash = new Buffer(part1 + "" + part2 + "" + part3).toString('base64');
	
	return hash;
}

accounts.getDistinctSections = getDistinctSections;
accounts.getUserType = getUserType;
accounts.studentLogin = studentLogin;
accounts.adminLogin = adminLogin;
accounts.ccAuthentication = ccAuthentication;
accounts.createAdmin = createAdmin;
accounts.deleteAdmin = deleteAdmin;
accounts.modifyAdmin = modifyAdmin;
accounts.changeAdminPassword = changeAdminPassword;
accounts.changeAdminName = changeAdminName;
accounts.getAdminUsers = getAdminUsers;
accounts.createStudentUser = createStudentUser;
accounts.getStudentUsers = getStudentUsers;
accounts.updateStudentInfo = updateStudentInfo;
accounts.deleteStudent = deleteStudent;
accounts.setNewPassword = setNewPassword;
accounts.resetPassword = resetPassword;

/**
 * Accounts export.
 */
module.exports = accounts;