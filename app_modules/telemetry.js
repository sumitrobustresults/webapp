var telemetry = {};

function logUserActivity(userId, context, description, reference, callback) {
	var Log = telemetry.nosql.model('Telemetry');
	var entry = new Log({
		userId: userId,
		context: context,
		details: description,
		reference: reference
	});
	entry.save(function(err, entry) {
		if(err)
			console.log(err);
		if(callback)
			callback(err, entry);
	});
}

telemetry.logUserActivity = logUserActivity;

module.exports = function(query, nosql) {
	
	telemetry.query = query;
	telemetry.nosql = nosql;
	
	return telemetry;
};