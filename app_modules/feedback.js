/**
 * Feedback module for editor.
 * @module 		app_modules/feedback
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var feedback = {};

function getRegisteredFeedbacks(callback) {
	var query = "SELECT hash,type FROM ext_feedback";
	this.db.getConnection(function(err, con) {
		con.query(query, [], function(err, rows) {
			if(err) {
				callback(err, null);
				return;
			} else {
				var reg = {};
				for(var i = 0; i < rows.length; i++) {
					reg[rows[i].hash] = rows[i].type;
				}
				callback(null, reg);
			}
		});
		con.release();
	});
}

function getFeedback(hash, callback) {
	var query = "SELECT type,feedback FROM ext_feedback WHERE hash=?";
	this.db.getConnection(function(err, con) {
		con.query(query, [hash], function(err, rows) {
			if(err || rows.length == 0) {
				callback(err, null);
				return;
			} else {
				callback(null, rows[0]);
			}
		});
		con.release();
	});
}

function updateFeedback(hash, feedback, callback) {
	var query = "REPLACE INTO ext_feedback (hash,feedback) VALUES (?,?)";
	this.db.getConnection(function(err, con) {
		con.query(query, [hash, feedback], function(err, result) {
			if(err) {
				callback(err, false);
			} else {
				callback(null, true);
			}
		});
		con.release();
	});
}

function addSpec(problemID, spec, inputs, callback) {
	this.db.getConnection(function(err, con) {
		var query = "INSERT INTO specification (problem_id,contents,inputs) VALUES (?,?,?)";
		con.query(query, [problemID, new Buffer(spec, 'base64').toString(), inputs], function(err, result) {
			if(err) callback(err, null);
			else callback(null, result.insertId);
		});
		con.release();
	});
}

function deleteSpec(specID, callback) {
	this.db.getConnection(function(err, con) {
		var query = "DELETE FROM specification WHERE id=?";
		con.query(query, [specID], function(err, result) {
			if(err) callback(err, false);
			else callback(null, true);
		});
		con.release();
	});
}

function updateSpecCode(specID, code, callback) {
	this.db.getConnection(function(err, con) {
		var query = "UPDATE specification SET contents=? WHERE id=?";
		con.query(query, [new Buffer(code, 'base64').toString(), specID], function(err, result) {
			if(err) callback(err, false);
			else callback(null, true);
		});
		con.release();
	});
}

function updateSpecInput(specID, input, callback) {
	this.db.getConnection(function(err, con) {
		var query = "UPDATE specification SET inputs=? WHERE id=?";
		con.query(query, [input, specID], function(err, result) {
			if(err) callback(err, false);
			else callback(null, true);
		});
		con.release();
	});
}

function getSpecs(problemID, callback) {
	this.db.getConnection(function(err, con) {
		var query = "SELECT id,contents,is_enabled,inputs FROM specification WHERE problem_id=?";
		con.query(query, [problemID], function(err, rows) {
			if(err) callback(err, null);
			else callback(null, rows);
		});
		con.release();
	});
}

function toggleSpec(specID, state, callback) {
	this.db.getConnection(function(err, con) {
		var query = "UPDATE specification SET is_enabled=? WHERE id=?";
		con.query(query, [state, specID], function(err, result) {
			if(err) callback(err, false);
			else callback(null, true);
		});
		con.release();
	});
}

feedback.getRegisteredFeedbacks = getRegisteredFeedbacks;
feedback.getFeedback = getFeedback;
feedback.updateFeedback = updateFeedback;
feedback.addSpec = addSpec;
feedback.deleteSpec = deleteSpec;
feedback.updateSpecCode = updateSpecCode;
feedback.updateSpecInput = updateSpecInput;
feedback.getSpecs = getSpecs;
feedback.toggleSpec = toggleSpec;

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 */
module.exports = function(db, cache) {
	
	feedback.db = db;
	feedback.cache = cache;
	
	return feedback;
}