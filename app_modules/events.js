/**
 * Manages events for the course.
 * @module 		app_modules/events
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var events = {};

/**
 * Get meta information regarding all lab events.
 * 
 * @param callback
 */
function getLabsOverview(callback) {
	var query = "SELECT id,name FROM event";
	sqlcache(query, [], function(err, data) {
		callback(err, data);
	}, 30);
}

function getSchedules(callback) {
	var query = "SELECT id,event_id,reference FROM schedule";
	sqlcache(query, [], function(err, data) {
		if(err) 
			callback(err, null);
		else {
			var grouped = {};
			for(var i = 0; i < data.length; i++) {
				if(typeof grouped[data[i].event_id] === 'undefined')
					grouped[data[i].event_id] = [];
				grouped[data[i].event_id].push(data[i]);
			}
			callback(null, grouped);
		}
	}, 30);
}

/**
 * Create a course event.
 * 
 * @param type
 * @param name
 * @param callback
 */
function createEvent(type, name, callback) {
	var query = "INSERT INTO event (type,name) " +
		"VALUES (?,?)";
	sqlquery(query, [type, name], function(err, result) {
		if(err) callback(null);
		else callback(result.insertId);
	});
}

/**
 * Create a schedule for a specific course event.
 * 
 * @param eventID
 * @param reference
 * @param startTime
 * @param endTime
 * @param callback
 */
function createSchedule(eventID, reference, startTime, endTime, callback) {
	var dateformat = require('dateformat');
	startTime = dateformat(new Date(startTime), "yyyy-mm-dd HH:MM:ss");
	endTime = dateformat(new Date(endTime), "yyyy-mm-dd HH:MM:ss");
	var query = "INSERT INTO schedule (event_id,reference,time_start,time_stop) " +
		"VALUES (?,?,?,?)";
	sqlquery(query, [eventID, reference, startTime, endTime], function(err, result) {
		if(err) callback(null);
		else callback(result.insertId);
	});
}

/**
 * Delete a schedule for a course event.
 * 
 * @param sid
 * @param callback
 */
function deleteSchedule(sid, callback) {
	var async = require('async');
	sqlpool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else {
			async.waterfall([
                 function(callback) {
               	  var query = "SELECT event_id,(SELECT COUNT(*) FROM schedule WHERE s.event_id=event_id) AS count FROM schedule s WHERE id=?";
               	  con.query(query, [sid], function(err, result) {
   	      				if(err) callback(err, null);
   	      				else callback(null, result);
   	      			});
                 },
                 function(data, callback) {
               	  if(data[0].count == 1) {
               		  var query = "DELETE FROM event WHERE id=?";
               		  con.query(query, [data[0].event_id], function(err, result) {
   		      			if(err) callback(err, false);
   		      			else callback(null, true);
   		      		  });
               	  } else {
               		  var query = "DELETE FROM schedule WHERE id=?";
   		      		  con.query(query, [sid], function(err, result) {
   		      			if(err) callback(err, false);
   		      			else callback(null, true);
   		      		  });
               	  }
                 }
             ], function(err, data) {
	   			if(err) callback(err, false);
	   			else callback(null, true);
	   		});
	   		con.release();
		}
	});
}

/**
 * Assign slots to student corresponding to a schedule.
 * 
 * @param schedule_id
 * @param sections
 * @param callback
 */
function assignSlots(schedule_id, sections, callback) {
	if(sections.length == 0) {
		callback(true);
		return;
	}
	var params = '';
	for(var i = 0; i < sections.length; i++)
		params = params + ',(?,?)';
	params = params.substring(1);
	var values = [];
	for(var i = 0; i < sections.length; i++) {
		values.push(schedule_id);
		values.push(sections[i]);
	}
	var query = "INSERT INTO slot (schedule_id,section) " +
			"VALUES " + params;
	sqlquery(query, values, function(err, result) {
		if(err) {
			callback(false);
		} else
			callback(true);
	});
}

/**
 * Fetches the ongoing event (if any).
 * 
 * @param userID
 * @param callback
 */
function getCurrentEvent(userID, callback) {
	sqlpool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(false, false);
		} else {
			var query = "SELECT `event`.id AS event_id,type,name,time_start,time_stop " + 
			"FROM `event` INNER JOIN schedule " +  
			"ON `event`.id=schedule.event_id " + 
			"WHERE ? BETWEEN time_start AND time_stop AND schedule.id IN " +
			"(SELECT schedule_id FROM slot WHERE section=(SELECT section FROM account WHERE id=?))";
			var date = new Date();
			var util = require('util');
			var now = util.format('%s-%s-%s %s:%s', date.getFullYear(),	date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes());
			con.query(query, [now, userID], function(err, rows) {
				if(err) {
					console.log(err);
					callback(false, false);
					return;
				}
				if(rows.length == 0) {
					callback(false, false);
					return;
				}
				var event = rows[0];
				query = "SELECT problem.id AS problem_id,assignment.id AS assignment_id,question_no,category,statement," +
						"(CASE WHEN EXISTS (SELECT * FROM code WHERE event=2 AND assignment_id=assignment.id) THEN 1 ELSE 0 END) AS solved " +  
					"FROM problem INNER JOIN assignment " + 
					"ON problem.id=assignment.problem_id " +   
					"WHERE assignment.user_id=? AND assignment.event_id=?";
				con.query(query, [userID, event.event_id], function(err, rows) {
					if(err) {
						console.log(err);
						callback(false, false);
					}
					callback(event, rows);
				});
			});
			con.release();
		}
	});
}

/**
 * Fetches meta information regarding all events along with their schedules.
 * 
 * @param callback
 */
function getEvents(callback) {
	var query = "SELECT schedule.id AS schedule_id,event_id,type,name,reference,time_start,time_stop FROM event INNER JOIN schedule " +
		"ON event.id=schedule.event_id ORDER BY time_start DESC";
	sqlquery(query, [], function(err, rows) {
		if(err) callback(null);
		else callback(rows);
	});
}

/**
 * Get the id,name,type of all events.
 * 
 * @param callback
 */
function getEventsMeta(callback) {
	var query = "SELECT id,name,type FROM event";
	sqlquery(query, [], function(err, rows) {
		if(err) callback(null);
		else callback(rows);
	});
}

/**
 * Checks if operations are permitted for a specific assignment at the current time.
 * 
 * @param event
 * @param assignmentID
 * @param callback
 */
function isAllowed(event, assignmentID, callback) {
	var query = "SELECT is_practice FROM problem WHERE id=(SELECT problem_id FROM assignment WHERE id=?)";
	sqlcache(query, [assignmentID], function(err, data) {
		if(err) {
			// Failure favours students?
			callback(null, false);
		} else if(data.length === 0) {
			// If such assignment does not exist.
			callback(null, false);
		} else {
			var isPractice = (data[0].is_practice == 1);
			if(isPractice) callback(null, true);
			else if(!event) callback(null, false);
			else {
				// Check if ongoing.
				var stopTime = new Date(event.time_stop);
				var rightNow = new Date();
				if(rightNow > stopTime) callback(null, false);
				else callback(null, true);
			}
		}
	});
}

/**
 * Fetches the ongoing event (if any).
 * 
 * @param studentID
 * @param callback
 */
function getOngoingEvent(studentID, callback) {
	sqlpool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			var query = "SELECT `event`.id AS event_id,type,name,time_start,time_stop " + 
				"FROM `event` INNER JOIN schedule " +  
				"ON `event`.id=schedule.event_id " + 
				"WHERE ? BETWEEN time_start AND time_stop AND schedule.id IN " +
				"(SELECT schedule_id FROM slot WHERE section=(SELECT section FROM account WHERE id=?))";
			
			var date = new Date();
			var util = require('util');
			var now = util.format('%s-%s-%s %s:%s', date.getFullYear(),	date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes());
			
			con.query(query, [now, studentID], function(err, rows) {
				if(err) {
					callback(err, null);
					return;
				}
				if(rows.length == 0) {
					callback(null, null);
					return;
				}
				var event = rows[0];
				query = "SELECT id,question,is_submitted,max_marks FROM assignment WHERE user_id=? AND event_id=? ORDER BY question";
				sqlcache(query, [studentID, event.event_id], function(err, rows) {
					if(err) {
						callback(err, null);
						return;
					}
					else callback(null, {
						event: event, 
						assignments: rows
					});
				});
			});
			con.release();
		}
	});
}

/**
 * Fetches the status of problem assignment to students for a specific course event.
 * 
 * @param eventID
 * @param callback
 */
function getAssignmentStatus(eventID, callback) {
	var query = "SELECT DISTINCT section FROM account WHERE id IN (SELECT DISTINCT user_id FROM assignment WHERE event_id=?)";
	sqlquery(query, [eventID], function(err, rows) {
		if(err) callback(err, null);
		else {
			var sections = [];
			for(var i = 0; i < rows.length; i++)
				sections.push(rows[i].section);
			callback(null, sections);
		}
	});
}

events.getLabsOverview = getLabsOverview;
events.getSchedules = getSchedules;
events.createEvent = createEvent;
events.createSchedule = createSchedule;
events.deleteSchedule = deleteSchedule;
events.assignSlots = assignSlots;
events.getCurrentEvent = getCurrentEvent;
events.getEvents = getEvents;
events.getEventsMeta = getEventsMeta;
events.isAllowed = isAllowed;
events.getOngoingEvent = getOngoingEvent;
events.getAssignmentStatus = getAssignmentStatus;

module.exports = events;