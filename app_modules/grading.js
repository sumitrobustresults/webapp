/**
 * Manages grading for the course.
 * @module 		app_modules/grading
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var grading = {};

/**
 * Fetches the score card for a student containing his/her grades for all assignments.
 * 
 * @param studentID
 * @param callback
 */
function getScoreCard(studentID, callback) {
	var query = "SELECT id,event_id,event_name,question,score,max_marks,is_submitted FROM assignment WHERE user_id=? AND event_id IS NOT NULL";
	sqlcache(query, [studentID], function(err, rows) {
		if(err) {
			callback(err, null);
			return;
		} else {
			var scores = {};
			for(var i = 0; i < rows.length; i++) {
				var assignment = rows[i];
				if(typeof scores[assignment.event_id] === 'undefined') scores[assignment.event_id] = {
					name: assignment.event_name,
					questions: {}
				};
				scores[assignment.event_id]['questions'][assignment.question] = {
					score: assignment.score,
					marks: assignment.max_marks,
					submitted: (assignment.is_submitted == 1)
				};
			}
			callback(null, scores);
		}
	});
}

/**
 * Grades an assignment.
 * 
 * @param assignmentID
 * @param marks
 * @param comments
 * @param evaluator
 * @param callback
 */
function setGrade(assignmentID, marks, comments, evaluator, callback) {
	var async = require('async');
	sqlpool.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			async.waterfall([
                 function(callback) {
                	 var query = "UPDATE assignment SET evaluator=?,score=?,comments=?,grading_time=CURRENT_TIMESTAMP,regrade_requested=0 WHERE id=?";
         			con.query(query, [evaluator, marks, comments, assignmentID], function(err, result) {
         				if(err) {
         					console.log(err);
         					callback(err, false);
         				} else {
         					callback(null, true);
         				}
         			});
                 },
                 function(result, callback) {
                	 if(result) {
                		 var query = "UPDATE task SET is_complete=1 WHERE reference=? AND type='GRADING'";
                    	 con.query(query, [assignmentID]);
                	 }
                	 callback(null, result);
                 }
             ], function(err, result) {
    			callback(null, result);
    		});
    		con.release();
		}
	});
}

/**
 * Fetches the grade awarded for an assignment.
 * 
 * @param assignmentID
 * @param callback
 */
function getGrade(assignmentID, callback) {
	var query = "SELECT score,comments,max_marks,evaluator,grading_time,regrade_requested,regrade_reason,regrade_reqtime," +
		"(SELECT name FROM account WHERE account.id=assignment.evaluator) AS name," +
		"(SELECT email FROM account WHERE account.id=assignment.evaluator) AS email " +
		"FROM assignment " +
		"WHERE id=?";
	sqlquery(query, [assignmentID], function(err, rows) {
		if(err || rows.length == 0) callback(err, null);
		else callback(null, rows[0]);
	});
}

function requestRegrade(assignmentID, reason, callback) {
	var query = "UPDATE assignment SET regrade_requested=1,regrade_reason=?,regrade_reqtime=NOW() " +
			"WHERE id=?";
	sqlquery(query, [reason, assignmentID], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else {
			var notifications = require('./notifications');
			notifications.notifyGroup('REGRADE_REQUEST', assignmentID, 'INSTRUCTORS');
			callback(null, true);
		}
	});
}

grading.getScoreCard = getScoreCard;
grading.setGrade = setGrade;
grading.getGrade = getGrade;
grading.requestRegrade = requestRegrade;

module.exports = grading;