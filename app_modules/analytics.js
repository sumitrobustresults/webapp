/**
 * Provides analytical information for the data.
 * @module 		app_modules/analytics
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var analytics = {};

/**
 * 
 * @param codeID
 * @param callback
 */
function getCodeClusters(codeID, callback) {
	var query = "SELECT cluster,description,line FROM ext_faulty INNER JOIN ext_cluster ON cluster=category WHERE code_id=?";
	analytics.cache(query, [codeID], function(err, rows) {
		if(err) callback(err, null);
		else callback(null, rows);
	});
}

/**
 * 
 * @param codeID
 * @param callback
 */
function getCompilerMessages(codeID, callback) {
	var query = "SELECT type,message,line FROM compilation_error WHERE code_id=?";
	analytics.cache(query, [codeID], function(err, data) {
		if(err) callback(err, null);
		else callback(null, data);
	});
}

/**
 * 
 * @param assignmentID
 * @param callback
 */
function getAssignmentAnalytics(assignmentID, callback) {
	var async = require('async');
	async.parallel([
        function(callback) {
        	var query = "SELECT id,contents AS code,save_type AS event,save_time AS timestamp FROM code WHERE assignment_id=? " +
        			"AND id > (SELECT MIN(id) FROM code WHERE assignment_id=?)";
    		analytics.cache(query, [assignmentID, assignmentID], function(err, rows) {
    			if(err) callback(err, null);
    			else callback(null, rows);
    		});
        },
        function(callback) {
        	var query = "SELECT code_id,type,error,message,line,hash,(SELECT compile_time FROM compilation WHERE id=(SELECT compilation_id FROM compilation_error WHERE id=error_id)) AS timestamp " +
        			"FROM ext_syntax WHERE code_id IN " +
        			"(SELECT id FROM code WHERE assignment_id=?)";
    		analytics.cache(query, [assignmentID], callback);
        },
        function(callback) {
        	var query = "SELECT code_id,result,execute_time FROM execution INNER JOIN code ON code.id=code_id WHERE assignment_id=?";
        	analytics.cache(query, [assignmentID], function(err, rows) {
        		callback(err, rows);
        	});
        },
        function(callback) {
        	var query = "SELECT code_id,evaluate_time,success AS passed " +
        		"FROM attempt WHERE assignment_id=?";
        	analytics.cache(query, [assignmentID], function(err, rows) {
        		callback(err, rows);
        	});
        }
    ], function(err, dataSet) {
		if(err) {
			callback(err, null);
		} else {
			callback(null, dataSet);
		}
	});
}

/**
 * 
 * @param callback
 */
function getSyntacticErrorTypes(callback) {
	var query = "SELECT hash,error,type,COUNT(id) AS frequency FROM ext_syntax GROUP BY hash ORDER BY frequency DESC";
	analytics.cache(query, [], callback);
}

/**
 * 
 * @param hash
 * @param callback
 */
function getInstancesHavingError(hash, callback) {
	var query = "SELECT assignment_id,code_id,message FROM ext_syntax INNER JOIN code ON code.id=code_id WHERE hash=?";
	analytics.cache(query, [hash], callback);
}

/**
 * 
 * @param codeID
 * @param callback
 */
function getCodeInstance(codeID, callback) {
	var query = "SELECT line,(SELECT contents FROM code WHERE id=code_id) AS code FROM ext_syntax WHERE code_id=?";
	analytics.cache(query, [codeID], function(err, data) {
		if(err) callback(err, null);
		else if(data.length > 0) callback(null, data[0]);
		else callback(null, null);
	});
}

/**
 * 
 * @param codeID
 * @param callback
 */
function getCodeWithErrors(codeID, callback) {
	var query = "SELECT contents,compiled,raw_output FROM code INNER JOIN compilation ON code.id=code_id WHERE code.id=?";
	analytics.cache(query, [codeID], function(err, rows) {
		if(err || rows.length == 0) callback(err, null);
		else callback(null, rows[0]);
	});
}

function addStar(aid, callback) {
	var query = "UPDATE ext_faulty SET starred=1 WHERE code_id IN (SELECT id FROM code WHERE assignment_id=?)";
	this.db.getConnection(function(err, con) {
		con.query(query, [aid], function(err, result) {
			if(err) {
				console.log(err);
				callback(err, false);
			} else {
				callback(null, true);
			}
		});
		con.release();
	});
}

function removeStar(aid, callback) {
	var query = "UPDATE ext_faulty SET starred=0 WHERE code_id IN (SELECT id FROM code WHERE assignment_id=?)";
	this.db.getConnection(function(err, con) {
		con.query(query, [aid], function(err, result) {
			if(err) {
				console.log(err);
				callback(err, false);
			} else {
				callback(null, true);
			}
		});
		con.release();
	});
}

function setSeen(codeID, callback) {
	var query = "UPDATE ext_faulty SET seen=1 WHERE code_id=?";
	this.db.getConnection(function(err, con) {
		con.query(query, [codeID], function(err, result) {
			if(err) {
				console.log(err);
				callback(err, false);
			} else {
				callback(null, true);
			}
		});
		con.release();
	});
}

analytics.getCodeClusters = getCodeClusters;
analytics.getCompilerMessages = getCompilerMessages;
analytics.getAssignmentAnalytics = getAssignmentAnalytics;
analytics.getSyntacticErrorTypes = getSyntacticErrorTypes;
analytics.getInstancesHavingError = getInstancesHavingError;
analytics.getCodeInstance = getCodeInstance;
analytics.getCodeWithErrors = getCodeWithErrors;
analytics.addStar = addStar;
analytics.removeStar = removeStar;
analytics.setSeen = setSeen;

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 */
module.exports = function(db, cache) {
	
	analytics.db = db;
	analytics.cache = cache;
	
	return analytics;
};