/**
 * Manages TA tasks for the course.
 * @module 		app_modules/tasks
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var tasks = {};

/**
 * Get all the events for which tasks have been assigned.
 * 
 * @param {getAssignedEventsCallback} callback
 */
function getAssignedEvents(callback) {
	var query = "SELECT id,name FROM event WHERE id IN (SELECT DISTINCT event_id FROM task)";
	tasks.query(query, [], function(err, rows) {
		if(err) {
			callback(err, null);
		} else callback(null, rows);
	});
}

/**
 * Fetches the tasks that have been assigned to a particular user.
 * 
 * @param userID
 * @param callback
 */
function getUserTasks(userID, callback) {
	var query = "SELECT name,task.type,reference,deadline,is_complete FROM task INNER JOIN event ON event.id=event_id " +
		"WHERE assignee=? ORDER BY is_complete";
	tasks.query(query, [userID], function(err, rows) {
		if(err) {
			callback(err, null);
		} else callback(null, rows);
	});
}

/**
 * Fetches the number of pending tasks for a particular user.
 * 
 * @param userID
 * @param callback
 */
function getNumPendingTasks(userID, callback) {
	var query = "SELECT id FROM task WHERE assignee=? AND is_complete=0";
	tasks.query(query, [userID], function(err, rows) {
		if(err) {
			callback(err, null);
		} else callback(null, rows.length);
	});
}

/**
 * Fetches summary information of the tasks that have been created for a particular course event.
 * 
 * @param eventID
 * @param callback
 */
function getTaskStatus(eventID, callback) {
	var query = "SELECT task.type,assignee,name,email,reference,is_complete " +
		"FROM task INNER JOIN account ON task.assignee=account.id WHERE event_id=?";
	tasks.query(query, [eventID], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			callback(null, rows);
		}
	});
}

tasks.getAssignedEvents = getAssignedEvents;
tasks.getUserTasks = getUserTasks;
tasks.getNumPendingTasks = getNumPendingTasks;
tasks.getTaskStatus = getTaskStatus;

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 * 
 * @return {object} tasks
 */
module.exports = function(db, cache, query) {
	
	tasks.db = db;
	tasks.cache = cache;
	tasks.query = query;
	
	return tasks;
}