/**
 * Scratchpad module.
 * @module 		app_modules/scratchpad
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var scratchpad = {};

/**
 * Fetches all the files and folders in the respective student's workspace.
 * 
 * @param studentID
 * @param callback
 */
function getWorkspaceFiles(studentID, callback) {
	var query = "SELECT id,is_dir,path,last_saved FROM source WHERE user_id=?";
	scratchpad.query(query, [studentID], function(err, rows) {
		if(err) {
			callback(err, null);
			return;
		} else {
			var files = [];
			for(var i = 0; i < rows.length; i++) {
				var path = rows[i].path.split("/");
				files.push({
					id: rows[i].id,
					path: path,
					loc: rows[i].path,
					dir: rows[i].is_dir,
					saved: rows[i].last_saved
				});
			}
			files.sort(function(a, b) {
				return a.path.length - b.path.length;
			});
			callback(null, files);
		}
	});
}

/**
 * Creates a new file in the student's workspace.
 * 
 * @param studentID
 * @param path
 * @param directory
 * @param callback
 */
function createFile(studentID, path, directory, callback) {
	var query = "INSERT INTO source (user_id,is_dir,path) VALUES (?,?,?)";
	scratchpad.query(query, [studentID, directory, path], function(err, result) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			callback(null, result.insertId);
		}
	});
}

/**
 * Deletes a file from a student's workspace.
 * 
 * @param studentID
 * @param fileID
 * @param callback
 */
function deleteFile(studentID, fileID, callback) {
	var query = "DELETE FROM source WHERE id=? AND user_id=?";
	scratchpad.query(query, [fileID, studentID], function(err, result) {
		if(err) {
			callback(err, false);
			return;
		} else {
			callback(null, true);
		}
	});
}

/**
 * Fetches a file along with its contents and meta information.
 * 
 * @param studentID
 * @param fileID
 * @param callback
 */
function getFile(studentID, fileID, callback) {
	var query = "SELECT contents,last_saved FROM source WHERE user_id=? AND id=?";
	scratchpad.query(query, [studentID, fileID], function(err, rows) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else if(rows.length == 0) {
			callback(null, null);
		} else {
			if(rows[0].contents === null) rows[0].contents = '';
			callback(null, rows[0]);
		}
	});
}

/**
 * Saves the contents of a file.
 * 
 * @param fileID
 * @param contents
 * @param callback
 */
function saveFile(fileID, contents, callback) {
	var query = "UPDATE source SET contents=?,last_saved=CURRENT_TIMESTAMP WHERE id=?";
	scratchpad.query(query, [contents, fileID], function(err, result) {
		if(err) {
			callback(err, false);
			return;
		} else {
			callback(null, {savetime: new Date().toUTCString()});
		}
	});
}

scratchpad.getWorkspaceFiles = getWorkspaceFiles;
scratchpad.createFile = createFile;
scratchpad.deleteFile = deleteFile;
scratchpad.getFile = getFile;
scratchpad.saveFile = saveFile;

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 */
module.exports = function(db, cache, query) {
	
	scratchpad.db = db;
	scratchpad.cache = cache;
	scratchpad.query = query;
	
	return scratchpad;
}