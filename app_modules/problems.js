/**
 * Manages problems for the course.
 * @module 		app_modules/problems
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 */
var problems = {};

/**
 * Fetches the PVD ids and the categories of all problems present in the database.
 * 
 * @param callback
 */
function getPVDCategory(callback) {
	var query = "SELECT id,id_p,id_v,id_d,category FROM problem";
	problems.cache(query, [], function(err, rows) {
		if(err) callback(err, null);
		else {
			var categories = {};
			for(var i = 0; i < rows.length; i++) {
				if(typeof categories[rows[i].category] === 'undefined')
					categories[rows[i].category] = [];
				categories[rows[i].category].push(rows[i]);
			}
			callback(null, categories);
		}
	});
}

/**
 * Creates a new problem.
 * 
 * @param visibility
 * @param statement
 * @param solution
 * @param template
 * @param category
 * @param localID
 * @param callback
 */
function putProblem(visibility, statement, solution, template, category, localID, callback) {
	var ids = new RegExp(/p([0-9]+)v([0-9]+)d([0-9]+)/).exec(localID);
	solution = new Buffer(solution).toString('base64');
	template = new Buffer(template).toString('base64');
	var query = "INSERT INTO problem (visibility,category,statement,solution,template,id_p,id_v,id_d) " +
			"VALUES (?,?,?,?,?,?,?,?)";
	problems.query(query, [visibility, category, statement, solution, template, ids[1], ids[2], ids[3]], function(err, result) {
		if(err) {
			console.log(err);
			callback(false);
		}
		else callback(result.insertId);
	});
}

/**
 * Deletes a problem from the database.
 * 
 * @param problemID
 * @param callback
 */
function removeProblem(problemID, callback) {
	var query = "DELETE FROM problem WHERE id=?";
	problems.query(query, [problemID], function(err, result) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else callback(null, true);
	});
}

/**
 * Marks a specific problem as a practice problem.
 * 
 * @param problemID
 * @param practice
 * @param callback
 */
function markPracticeProblem(problemID, practice, callback) {
	var query = "UPDATE problem SET is_practice=? WHERE id=?";
	problems.query(query, [practice, problemID], function(err, result) {
		if(err) {
			console.log(err);
			callback(err, false);
		} else callback(null, true);
	});
}

/**
 * Fetches problem statement,difficulty and category for a specific problem.
 * 
 * @param assignmentID
 * @param callback
 */
function getProblem(assignmentID, callback) {
	var query = "SELECT id_difficulty,statement,category FROM problem WHERE id=" +
		"(SELECT problem_id FROM assignment WHERE id=?)";
	problems.query(query, [assignmentID], function(err, rows) {
		if(err) callback(null);
		else callback(rows[0]);
	});
}

/**
 * Fetches the solution for a particular problem.
 * 
 * @param id
 * @param callback
 */
function getPartialSolution(id, callback) {
	var query = "SELECT code FROM solution WHERE problem_id=?";
	problems.query(query, [id], function(err, rows) {
		if(err) callback(null);
		callback(rows[0].code);
	});
}

/**
 * Fetches all the category names for all problems present in the database, restricted to the visibility levels for the requesting user.
 * 
 * @param adminType
 * @param callback
 */
function getCategories(adminType, callback) {
	var visibility = 2 - adminType;
	var query = "SELECT DISTINCT category FROM problem WHERE visibility<=?";
	problems.query(query, [visibility], function(err, rows) {
		if(err) callback(null);
		else {
			var categories = [];
			for(var i = 0; i < rows.length; i++) categories.push(rows[i].category);
			callback(categories);
		}
	});
}

/**
 * Fetches meta information of all the problems for a particular category, restricted to the visibility levels for the requesting user.
 * 
 * @param adminType
 * @param category
 * @param callback
 */
function getProblemsByCategory(adminType, category, callback) {
	var visibility = 2 - adminType;
	var query = "SELECT id,local_id,id_problem,id_version,id_difficulty,visibility," +
		"(SELECT COUNT(*) FROM testcase WHERE problem_id=problem.id) AS testcases " +
		"FROM problem " +
		"WHERE category=? AND visibility<=? ORDER BY local_id";
	problems.query(query, [category, visibility], function(err, rows) {
		if(err) callback(null);
		else callback(rows);
	});
}

/**
 * Fetches meta information of all the problems in the database, restricted to the visibility levels for the requesting user.
 * 
 * @param adminType
 * @param callback
 */
function getAllProblems(adminType, callback) {
	var visibility = 2 - adminType;
	var query = "SELECT id,title,category,id_p,id_v,id_d,visibility," +
		"(SELECT COUNT(*) FROM test_case WHERE problem_id=problem.id) AS testcases " +
		"FROM problem " +
		"WHERE visibility<=? ORDER BY id_p";
	problems.query(query, [visibility], function(err, rows) {
		if(err) callback(err, null);
		else callback(null, rows);
	});
}

/**
 * Fetches meta information for all practice problems.
 * @param callback
 */
function getPracticeProblems(callback) {
	var query = "SELECT id,title,category FROM problem WHERE is_practice=1";
	problems.query(query, [], function(err, rows) {
		if(err) callback(err, null);
		else {
			var problems = {};
			for(var i = 0; i < rows.length; i++) {
				if(typeof problems[rows[i].category] === 'undefined') problems[rows[i].category] = [];
				problems[rows[i].category].push(rows[i]);
			}
			callback(null, problems);
		}
	});
}

/**
 * Fetches the problem statement corresponding to a particular problem.
 * 
 * @param problemID
 * @param callback
 */
function getProblemStatement(problemID, callback) {
	var query = "SELECT id,title,category,statement,env FROM problem WHERE id=?";
	problems.query(query, [problemID], function(err, rows) {
		if(err || rows.length == 0) callback(err, null);
		else {
			var problem = rows[0];
			var Environment = nosql.model('Environments');
			Environment.findOne({name: problem.env}).exec(function(err, data) {
				if(err){
					console.log(err);
					callback(err, null);
				} else {
					problem.env = data;
					callback(null, problem);
				}
			});
		}
	});
}

/**
 * Fetches a complete problem along with its test cases.
 * 
 * @param adminType
 * @param problemID
 * @param callback
 */
function getCompleteProblem(adminType, problemID, callback) {
	var visibility = 2 - adminType;
	this.db.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			var query = "SELECT env,id,title,statement,solution,template,id_p,id_v,id_d,is_practice,visibility FROM problem WHERE id=? AND visibility<=?";
			con.query(query, [problemID, visibility], function(err, rows) {
				if(err || rows.length == 0) callback(null);
				else {
					query = "SELECT * FROM test_case WHERE problem_id=? AND type=1";
					con.query(query, [problemID], function(err, tests) {
						if(err) callback(err, null);
						else {
							var problem = rows[0];
							problem.test_cases = tests;
                			var Environment = nosql.model('Environments');
                			Environment.findOne({name: problem.env}).exec(function(err, data) {
                				if(err){
                					console.log(err);
                					callback(err, null);
                				} else {
                					problem.env = data;
                					callback(null, problem);
                				}
                			});
						}
					});
				}
			});
			con.release();
		}
	});
}

/**
 * Fetches all automated test cases for a particular problem.
 * 
 * @param problemID
 * @param callback
 */
function getAutomatedTestCases(problemID, callback) {
	var query = "SELECT * FROM test_case WHERE problem_id=? AND type=0";
	problems.cache(query, [problemID], callback);
}

/**
 * Adds a new test case for a particular problem.
 * 
 * @param userID
 * @param problemID
 * @param input
 * @param output
 * @param visibility
 * @param callback
 */
function addTestCase(userID, problemID, input, output, visibility, callback) {
	var query = "INSERT INTO test_case (problem_id,input,output,visibility,type) " +
		"VALUES (?,?,?,?,1)";
	problems.query(query, [problemID, input, output, visibility], function(err, result) {
		if(err) callback(null);
		else {
			/*require('./audit')(db).logTestCase(userID, result.insertId, function() {
				callback(result.insertId);
			});*/
			callback(result.insertId);
		}
	});
}

/**
 * Adds automated test cases in batch for a particular problem, from a CSV file.
 * 
 * @param userID
 * @param problemID
 * @param data
 * @param callback
 */
function addAutomatedTests(userID, problemID, data, callback) {
	var csv = require('fast-csv');
	var failed = 0;
	csv
		.fromString(data, {headers: false})
		.on('data', function(data) {
			var input = new Buffer(data[0], 'base64').toString();
			var output = new Buffer(data[1], 'base64').toString();
			var query = "INSERT INTO testcase (problem_id,input,output,visibility,type) " +
				"VALUES (?,?,?,0,0)";
			problems.query(query, [problemID, input, output], function(err, result) {
				if(err) failed++;
			});
		})
		.on('end', function() {
			if(failed > 0) callback(false);
			else callback(true);
		});
}

/**
 * Deletes a test case.
 * 
 * @param testID
 * @param callback
 */
function removeTestCase(testID, callback) {
	var query = "DELETE FROM test_case WHERE id=?";
	problems.query(query, [testID], function(err, result) {
		if(err) callback(false);
		else callback(true);
	});
}

function updateEnv(pid, env, callback) {
	var query = "UPDATE problem SET env=? WHERE id=?";
	problems.query(query, [env, pid], function(err, result) {
		if(err) callback(err, false);
		else callback(null, true);
	});
}

/**
 * Updates the title for a particular problem.
 * 
 * @param pid
 * @param title
 * @param callback
 */
function updateTitle(pid, title, callback) {
	var query = "UPDATE problem SET title=? WHERE id=?";
	problems.query(query, [title, pid], function(err, result) {
		if(err) callback(false);
		else callback(true);
	});
}

/**
 * Updates the problem statement for a particular problem.
 * 
 * @param pid
 * @param statement
 * @param callback
 */
function updateStatement(pid, statement, callback) {
	var query = "UPDATE problem SET statement=? WHERE id=?";
	problems.query(query, [statement, pid], function(err, result) {
		if(err) callback(false);
		else callback(true);
	});
}

/**
 * Updates the template for a particular problem.
 * 
 * @param pid
 * @param template
 * @param callback
 */
function updateTemplate(pid, template, callback) {
	var query = "UPDATE problem SET template=? WHERE id=?";
	problems.query(query, [template, pid], function(err, result) {
		if(err) callback(false);
		else callback(true);
	});
}

/**
 * Updates the solution code for a particular problem.
 * 
 * @param cookie
 * @param pid
 * @param solution
 * @param host
 * @param callback
 */
function updateSolution(cookie, pid, solution, host, callback) {
	
	var async = require('async');
	var engineURL = 'http://' + host + '/';
	var request = require('request');
	var jar = request.jar();
	
	jar.setCookie(request.cookie('its=' + cookie), engineURL);
	
	this.db.getConnection(function(err, con) {
		if(err) {
			console.log(err);
			callback(err, null);
		} else {
			async.waterfall([
                 function(callback) {
                	 var query = "UPDATE problem SET solution=? WHERE id=?";
                	 con.query(query, [solution, pid], function(err, result) {
                		 callback(err, result);
                	 });
                 },
                 function(result, callback) {
                	 var query = "SELECT id,input FROM test_case WHERE problem_id=?";
                	 con.query(query, [pid], function(err, rows) {
                		 callback(err, rows);
                	 });
                 },
                 function(rows, callback) {
                	var request = require('request');
    				request({
    					uri: engineURL + 'compile',
    					method: 'POST',
    					jar: jar,
    					form: {
    						code: solution
    					}
    				}, function(err, res, body) {
    					callback(err, body, rows);
    				});
                 }, 
                 function(body, rows, callback) {
    				var response = JSON.parse(body);
    				if(response.result !== 'success') {
    					callback(null, null);
    				} else {
    					async.each(rows, function(row, callback) {
    						var request = require('request');
    						request({
    							uri: engineURL + 'execute',
    							method: 'POST',
    							jar: jar,
    							form: {
    								code: solution,
    								executable: response.executable,
    								testcase: row.input
    							}
    						}, function(errors, resp, exec) {
    							// TODO parse fails handle situation
    							exec = JSON.parse(exec);
    							if(exec.status === 'OK') {
    								var output = exec.output;
    								query = "UPDATE test_case SET output=? WHERE id=?";
    								con.query(query, [output, row.id], function(err, result) {
    									callback(err, result);
    								});
    							} else {
    								// handle error
    								callback(null, null);
    							}
    						});
    					}, function(err) {
    						callback(err, true);
    					});
    				}
                 }
             ], function(err, data) {
    			con.release();
    			callback(data);
    		});
		}
	});
}

/*function getDowngrade(aid, callback) {
	this.db.getConnection(function(err, con) {
		var query = "SELECT id,category,local_id,statement " +  
			"FROM problem AS p " +
			"WHERE EXISTS " + 
			"(" +
			    "SELECT * FROM problem WHERE id=(SELECT problem_id FROM assignment WHERE id=?) " +
			    "AND category=p.category AND id_problem=p.id_problem AND id_version=p.id_version AND p.id_difficulty=id_difficulty+1" +
		    ")";
		con.query(query, [aid], function(err, rows) {
			if(err || rows.length == 0) callback(false);
			else {
				var problem = rows[0];
				query = "UPDATE assignment SET problem_id=? WHERE id=?";
				con.query(query, [problem.id, aid], function(err) {
					if(err) callback(null);
					else callback(problem);
				});
			}
		});
		con.release();
	});
}

function getUpgrade(aid, callback) {
	this.db.getConnection(function(err, con) {
		var query = "SELECT id,category,local_id,statement " +  
			"FROM problem AS p " +
			"WHERE EXISTS " + 
			"(" +
			    "SELECT * FROM problem WHERE id=(SELECT problem_id FROM assignment WHERE id=?) " +
			    "AND category=p.category AND id_problem=p.id_problem AND id_version=p.id_version AND p.id_difficulty=id_difficulty-1" +
		    ")";
		con.query(query, [aid], function(err, rows) {
			if(err || rows.length == 0) callback(false);
			else {
				var problem = rows[0];
				query = "UPDATE assignment SET problem_id=? WHERE id=?";
				con.query(query, [problem.id, aid], function(err) {
					if(err) callback(null);
					else callback(problem);
				});
			}
		});
		con.release();
	});
}*/

problems.getPVDCategory = getPVDCategory;
problems.putProblem = putProblem;
problems.removeProblem = removeProblem;
problems.markPracticeProblem = markPracticeProblem;
problems.getProblem = getProblem;
problems.getPartialSolution = getPartialSolution;
problems.getCategories = getCategories;
problems.getProblemsByCategory = getProblemsByCategory;
problems.getAllProblems = getAllProblems;
problems.getPracticeProblems = getPracticeProblems;
problems.getProblemStatement = getProblemStatement;
problems.getCompleteProblem = getCompleteProblem;
problems.getAutomatedTestCases = getAutomatedTestCases;
problems.addTestCase = addTestCase;
problems.addAutomatedTests = addAutomatedTests;
problems.removeTestCase = removeTestCase;
problems.updateEnv = updateEnv;
problems.updateTitle = updateTitle;
problems.updateStatement = updateStatement;
problems.updateTemplate = updateTemplate;
problems.updateSolution = updateSolution;
/*problems.getDowngrade = getDowngrade;
problems.getUpgrade = getUpgrade;*/

/**
 * @param {object} db Database pool.
 * @param {function} cache Caching method.
 */
module.exports = function(db, cache, query) {
	
	problems.db = db;
	problems.cache = cache;
	problems.query = query;
	
	return problems;
};