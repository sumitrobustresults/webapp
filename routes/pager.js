/**
 * Pager router.
 * @module 		routes/pager
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../app_modules/pager';

router.get('/', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'PAGER', 'LAND', null);
	var pager = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	pager.getUserMessageThreads(req.session.user.id, function(err, threads) {
		pager.markSeen(req.session.user.id, function(result) {
			res.render('pager', {
				title: 'Prutor', 
				username: req.session.user.name,
				role: req.session.role,
				userid: req.session.user.id,
				enableScratch: req.session.scratchpad,
				threads: threads
			});
		});
	});
});

router.post('/create', function(req, res) {
	var pager = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	var reference = req.body.reference ? req.body.reference : null;
	pager.createThread(req.session.user.id, req.body.context, reference, req.body.visibility, req.body.message, function(err, result) {
		if(err) console.log(err);
		res.json(result);
	});
});

router.post('/respond', function(req, res) {
	var pager = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	pager.respond(req.session.user.id, req.body.id, req.body.message, function(err, result) {
		if(err) console.log(err);
		res.json(result);
	});
});

router.post('/remove', function(req, res) {
	var pager = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	pager.removeMessage(req.session.user.id, req.body.id, function(err, result) {
		if(err) console.log(err);
		res.json(result);
	});
});

router.get('/messages', function(req, res) {
	var pager = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	pager.getMessages(req.query.thread_id, function(err, result) {
		if(err) console.log(err);
		res.json(result);
	});
});

module.exports = router;