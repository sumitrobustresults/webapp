/**
 * Problems update router.
 * @module 		routes/problems/update
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../../app_modules/problems';

router.post('/environment', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.updateEnv(req.body.id, req.body.env, function(err, result) {
		if(err)
			console.log(err);
		res.json(result);
	});
});

router.post('/statement', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.updateStatement(req.body.pid, req.body.statement, function(result) {
		if(result !== null) res.json(true);
		else res.json(false);
	});
});

router.post('/template', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.updateTemplate(req.body.pid, req.body.template, function(result) {
		if(result !== null) res.json(true);
		else res.json(false);
	});
});

router.post('/solution', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.updateSolution(req.cookies['its'], req.body.pid, req.body.solution, req.body.host, function(result) {
		if(result !== null) res.json(true);
		else res.json(false);
	});
});

router.post('/title', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.updateTitle(req.body.pid, req.body.title, function(result) {
		if(result !== null) res.json(true);
		else res.json(false);
	});
});

module.exports = router;