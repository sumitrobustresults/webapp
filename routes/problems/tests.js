/**
 * Problems tests router.
 * @module 		routes/problems/tests
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../../app_modules/problems';

router.post('/add/manual', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.addTestCase(req.session.user.id, req.body.problem_id, req.body.input, req.body.output, req.body.visibility, function(result) {
		if(result !== null) res.json(true);
		else res.json(false);
	});
});

router.post('/add/auto', function(req, res) {
	problems.addAutomatedTests(req.session.user.id, req.body.pid, req.body.data, function(result) {
		res.json(result);
	});
});

router.post('/remove', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.removeTestCase(req.body.test_id, function(result) {
		res.json(result);
	});
});

module.exports = router;