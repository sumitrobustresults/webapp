/**
 * Problems main router.
 * @module 		routes/problems/index
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

router.use('/update', require('./update'));
router.use('/tests', require('./tests'));

var modulePath = '../../app_modules/problems';

router.get('/statement', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.getProblemStatement(req.query.id, function(err, problem) {
		res.json(problem);
	});
});

router.get('/manage', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.getAllProblems(req.session.user.type, function(err, problems) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Could not continue. Please contact the system administrator.');
			return;
		}
		res.render('_admin/problems/manage', {
			title: 'Prutor Admin',
			username: req.session.user.name,
			notifications: req.session.notifications,
			tasks: req.session.tasks,
			problems: problems,
			adminType: req.session.user.type
		});
	});
});

router.get('/upload', function(req, res) {
	res.render('_admin/problems/upload', {
		title: 'Prutor Admin',
		username: req.session.user.name,
		notifications: req.session.notifications
	});
});

router.get('/:id', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	var engine = require('../../app_modules/engine');
	var async = require('async');
	async.parallel([
        function(callback) {
        	problems.getAllProblems(req.session.user.type, callback);
        },
        function(callback) {
        	problems.getCompleteProblem(req.session.user.type, req.params.id, callback);
        },
        function(callback) {
        	problems.getAutomatedTestCases(req.params.id, callback);
        },
        function(callback) {
        	engine.getProgrammingEnvironments(callback);
        }
    ], function(err, dataSet) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Could not continue. Please contact the system administrator.');
		} else if(typeof dataSet[1] === 'undefined') {
			res.redirect('/problems/manage');
		} else {
			res.render('_admin/problems/problem', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				problems: dataSet[0],
				problem: dataSet[1],
				automated: dataSet[2],
				environments: dataSet[3],
				adminType: req.session.user.type
			});
		}
	});
});

router.get('/categories', function(req, res) {
	problems.getCategories(req.session.user.type, function(categories) {
		res.json(categories);
	});
});

router.get('/list', function(req, res) {
	if(typeof req.query.category === 'undefined') {
		problems.getAllProblems(req.session.user.type, function(problems) {
			res.json(problems);
		});
	} else {
		problems.getProblemsByCategory(req.session.user.type, req.query.category, function(problems) {
			res.json(problems);
		});
	}
});

router.post('/', function(req, res) {
	var p = req.body;
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.putProblem(p.visibility, p.statement, p.solution, p.template, p.category, p.local_id, function(result) {
		res.json(result);
	});
});

router.post('/practice', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.markPracticeProblem(req.body.pid, req.body.practice, function(err, result) {
		res.json(result);
	});
});

router.post('/remove', function(req, res) {
	var problems = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	problems.removeProblem(req.body.id, function(err, result) {
		res.json(result);
	});
});

module.exports = router;