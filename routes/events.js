/**
 * Events router.
 * @module 		routes/events
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../app_modules/events';

router.get('/', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'EVENTS', 'LAND', null);
	var async = require('async');
	var events = require('../app_modules/events');
	var accounts = require('../app_modules/accounts');
	var problems = require('../app_modules/problems')(req.dbPool,   req.cacheData, req.dbquery);
	async.parallel([
        function(callback) {
        	accounts.getDistinctSections(callback);
        },
        function(callback) {
        	events.getLabsOverview(callback);
        },
        function(callback) {
        	problems.getPVDCategory(callback);
        },
        function(callback) {
        	events.getSchedules(callback);
        }
    ], function(err, data) {
		if(err) res.send('An error occurred! Could not continue.');
		else {
			res.render('_admin/events/main', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				meta: {
					sections: data[0],
					events: data[1],
					problems: data[2],
					schedules: data[3]
				}
			});
		}
	});
});

router.get('/assign', function(req, res) {
	var async = require('async');
	var events = require('../app_modules/events');
	var accounts = require('../app_modules/accounts');
	var problems = require('../app_modules/problems')(req.dbPool,   req.cacheData, req.dbquery);
	async.parallel([
        function(callback) {
        	accounts.getDistinctSections(callback);
        },
        function(callback) {
        	events.getLabsOverview(callback);
        },
        function(callback) {
        	problems.getPVDCategory(callback);
        }
    ], function(err, data) {
		if(err) res.send('An error occurred! Could not continue.');
		else {
			res.render('_admin/events/assignment', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				meta: {
					sections: data[0],
					events: data[1],
					problems: data[2]
				}
			});
		}
	});
});

router.get('/all', function(req, res) {
	var events = require(modulePath);
	events.getEvents(function(data) {
		res.json(data);
	});
});

router.get('/status', function(req, res) {
	var events = require(modulePath);
	events.getAssignmentStatus(req.query.id, function(err, data) {
		if(err) console.log(err);
		res.json(data);
	});
});

router.post('/assign', function(req, res) {
	var assignments = require('../app_modules/assignments');
	assignments.assignProblems(req.body.algo, req.body.event_id, req.body.problems, function(err, result) {
		if(err) console.log(err);
		res.json(result);
	});
});

router.post('/create', function(req, res) {
	var events = require(modulePath);
	events.createEvent(req.body.type, req.body.name, function(result) {
		res.json(result);
	});
});

router.post('/schedule', function(req, res) {
	var events = require(modulePath);
	events.createSchedule(req.body.eid, req.body.reference, req.body.start_time, req.body.end_time, function(result) {
		res.json(result);
	});
});

router.post('/deleteschedule', function(req, res) {
	var events = require(modulePath);
	events.deleteSchedule(req.body.id, function(err, result) {
		if(err) console.log(err);
		res.json(result);
	});
});

router.post('/slot', function(req, res) {
	var events = require(modulePath);
	events.assignSlots(req.body.sid, req.body.sections, function(result) {
		res.json(result);
	});
});

module.exports = router;