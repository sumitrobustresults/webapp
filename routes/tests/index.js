/**
 * Tests main router.
 * @module 		routes/tests/index
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../../app_modules/tests';

router.get('/db', function(req, res) {
	var tests = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	tests.simulateRandomHomeFetch(function(err, data) {
		if(err) {
			console.log(err);
			res.send('An error occurred!');
		} else
			res.json(data);
	});
});

module.exports = router;