/**
 * Accounts main router.
 * @module 		routes/accounts/index
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

router.use('/student', require('./student'));

var modulePath = '../../app_modules/accounts';
var acl = null;
var adminRoles = ['instructor', 'tutor', 'ta'];

var init = function(aclObj) {
	acl = aclObj;
	return router;
};

/**
 * Attempts to log in a user.
 */
router.post('/login', function(req, res) {
	
	var username = req.body.username;
	var password = req.body.password;
	
	var async = require('async');
	var accounts = require(modulePath);
	
	accounts.getUserType(username, function(err, userType) {
		if(err) {
			console.log(err);
			res.json(false);
		} else if(userType === 'STUDENT') {
			accounts.studentLogin(username, password, function(user) {
				if(user === false) {
					// invalid credentials
					res.json(false);
				} else {
					// valid credentials
					req.session.cookie.expires = new Date(Date.now() + 3600000);
					req.session.cookie.maxAge = 36000000;
					req.session.user = user;
					req.session.role = 'regular';
					acl.addUserRoles(user.id, 'student');
					res.json(true);
				}
			});
		} else if(userType === 'ADMIN') {
			accounts.adminLogin(username, password, function(user) {
				if(user === false) {
					// invalid credentials
					res.json(false);
				} else {
					// valid credentials
					req.session.cookie.expires = new Date(Date.now() + 36000000);
					req.session.cookie.maxAge = 36000000;
					req.session.user = user;
					req.session.role = 'admin';
					acl.addUserRoles(user.id, adminRoles[user.type]);
					res.json(true);
				}
			});
		} else {
			res.json(false);
		}
	});
});

router.get('/logout', function(req, res) {
	
	req.session.destroy();
	
	res.redirect('/');
});

router.get('/', function(req, res) {
	var async = require('async');
	var accounts = require(modulePath);
	async.parallel([
        function(callback) {
        	accounts.getAdminUsers(callback);
        }
    ], function(err, dataSet) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Please contact the administrator.');
		} else {
			res.render('_admin/accounts/main', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				admins: dataSet[0]
			});
		}
	});
});

router.post('/create', function(req, res) {
	var accounts = require(modulePath);
	accounts.createAdmin(req.body.name, req.body.email, req.body.type, req.body.section, function(result) {
		if(result !== null) res.json(true);
		else res.json(false);
	});
});

router.post('/delete', function(req, res) {
	var accounts = require(modulePath);
	accounts.deleteAdmin(req.body.id, function(result) {
		res.json(result);
	});
});

router.post('/modify', function(req, res) {
	var accounts = require(modulePath);
	accounts.modifyAdmin(req.body.id, req.body.role, function(result) {
		if(result) {
			acl.removeUserRoles(req.body.id, adminRoles);
			acl.addUserRoles(req.body.id, adminRoles[parseInt(req.body.role)]);
		}
		res.json(result);
	});
});

router.post('/password', function(req, res) {
	var accounts = require(modulePath);
	accounts.changeAdminPassword(req.session.user.id, req.body.oldpass, req.body.newpass, function(result) {
		res.json(result);
	});
});

router.post('/name', function(req, res) {
	var accounts = require(modulePath);
	accounts.changeAdminName(req.session.user.id, req.body.name, function(result) {
		if(result) req.session.user.name = req.body.name;
		res.json(result);
	});
});

router.post('/setnewpassword', function(req, res) {
	var accounts = require(modulePath);
	accounts.setNewPassword(req.body.hash, req.body.password, function(err, result) {
		if(err)
			console.log(err);
		res.json(result);
	});
});

router.post('/initresetpwd', function(req, res) {
	var accounts = require(modulePath);
	accounts.resetPassword(req.body.id, function(err, result) {
		if(err)
			console.log(err);
		res.json(result);
	});
});

module.exports = init;