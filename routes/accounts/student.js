/**
 * Student accounts router.
 * @module 		routes/accounts/student
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../../app_modules/accounts';

router.get('/', function(req, res) {
	var async = require('async');
	var accounts = require(modulePath);
	async.parallel([
        function(callback) {
        	accounts.getStudentUsers(callback);
        }
    ], function(err, dataSet) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Please contact the administrator.');
		} else {
			res.render('_admin/accounts/students', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				students: dataSet[0]
			});
		}
	});
});

router.post('/update', function(req, res) {
	var accounts = require(modulePath);
	accounts.updateStudentInfo(req.body.id, req.body.email, req.body.roll, req.body.name, req.body.section, req.body.auth, function(err, result) {
		if(err) {
			console.log(err);
		}
		res.json(result);
	});
});

router.post('/create', function(req, res) {
	var accounts = require(modulePath);
	accounts.createStudentUser(req.body.email, function(err, result) {
		if(err) {
			console.log(err);
		}
		res.json(result);
	});
});

router.post('/delete', function(req, res) {
	var accounts = require(modulePath);
	accounts.deleteStudent(req.body.id, function(err, result) {
		if(err) {
			console.log(err);
		}
		res.json(result);
	});
});

module.exports = router;