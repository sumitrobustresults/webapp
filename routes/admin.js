/**
 * Admin router.
 * @module 		routes/admin
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	// Render admin home page
	var async = require('async');
	var pager = require('../app_modules/pager')(req.dbPool, req.cacheData, req.dbquery);
	var tasks = require('../app_modules/tasks')(req.dbPool, req.cacheData, req.dbquery);
	var notifications = require('../app_modules/notifications');
	async.parallel([
        function(callback) {
        	tasks.getNumPendingTasks(req.session.user.id, callback);
        },
        function(callback) {
        	pager.getMessageNotifications(req.session.user.id, callback);
        },
        function(callback) {
        	notifications.getUnseenNotifications(req.session.user.id, callback);
        }
    ], function(err, data) {
		if(err)
			console.log(err);
		else {
			req.session.notifications = data[2];
			req.session.tasks = data[0];
			console.log(req.session.notifications);
			res.render('_admin/home', {
				title: 'Prutor', 
				username: req.session.user.name,
				type: req.session.user.type,
				notifications: req.session.notifications,
				tasks: req.session.tasks
			});
		}
	});
});

router.get('/editor', function(req, res) {
	res.render('_admin/editor', {
		title: 'Prutor Admin',
		username: req.session.user.name,
		notifications: req.session.notifications,
		tasks: req.session.tasks
	});
});

router.get('/submissions', function(req, res) {
	var events = require('../app_modules/events');
	events.getEvents(function(rows) {
		var events = {};
		for(var i = 0; i < rows.length; i++) {
			if(typeof events[rows[i].event_id] === 'undefined')
				events[rows[i].event_id] = {type: rows[i].type, name: rows[i].name, schedules: []};
		 	events[rows[i].event_id].schedules.push({id: rows[i].schedule_id, reference: rows[i].reference});
		}
		res.render('_admin/submissions', {
			title: 'Prutor Admin',
			username: req.session.user.name,
			notifications: req.session.notifications,
			tasks: req.session.tasks,
			events: events
		});
	});
});

router.get('/viewer/:id', function(req, res) {
	var async = require('async');
	var assignments = require('../app_modules/assignments');
	var grading = require('../app_modules/grading');
	async.parallel([
    	function(callback) {
    		assignments.getCodeHistory(req.params.id, callback);
    	},
    	function(callback) {
    		grading.getGrade(req.params.id, callback);
    	}
    ], function(err, dataSet) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Please contact the system administrator.');
		} else if(dataSet[0].assignment === null) {
			res.send('Could not find such an assignment.');
		} else {
			res.render('_admin/viewer/main', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				userid: req.session.user.id,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				adminType: req.session.user.type,
				assignmentID: req.params.id,
				assignment: dataSet[0].assignment,
				history: dataSet[0].history,
				compilations: new Buffer(JSON.stringify(dataSet[0].compilations)).toString('base64'),
				executions: new Buffer(JSON.stringify(dataSet[0].executions)).toString('base64'),
				problem: dataSet[0].problem,
				grade: dataSet[1]
			});
		}
	});
});

router.get('/pager', function(req, res) {
	var pager = require('../app_modules/pager')(req.dbPool,   req.cacheData, req.dbquery);
	pager.getMessageThreads(function(err, threads) {
		req.session.notifications = null;
		pager.markSeen(req.session.user.id, function(result) {
			res.render('_admin/tickets', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				userid: req.session.user.id,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				threads: threads
			});
		});
	});
});

module.exports = router;