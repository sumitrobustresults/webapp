/**
 * Student editor router.
 * @module 		routes/editor
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

router.get('/rules', function(req, res) {
	var analytics = require('../app_modules/analytics')(req.dbPool, req.cacheData, req.dbquery);
	analytics.getErrorRules(function(err, rules) {
		if(err) {
			console.log(err);
			res.json(null);
		} else
			res.json(rules);
	});
});

// Scratchpad.
router.get('/', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'SCRATCHPAD', 'LAND', null);
	var async = require('async');
	var scratchpad = require('../app_modules/scratchpad')(req.dbPool, req.cacheData, req.dbquery);
	var engine = require('../app_modules/engine');
	async.parallel([
            function(callback) {
            	scratchpad.getWorkspaceFiles(req.session.user.id, callback);
            },
            function(callback) {
            	engine.getProgrammingEnvironments(callback);
            },
            function(callback) {
            	engine.getDefaultEnvironment(callback);
            }
        ], function(err, data) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Could not continue. Please contact the system administrator.');
		} else {
			res.render('editor/main', {
				title: 'Prutor', 
				username: req.session.user.name,
				role: req.session.role,
				files: data[0],
				environments: data[1],
				env: data[2],
				enableScratch: req.session.scratchpad
			});
		}
	});
});

// Assignment problem.
router.get('/:id', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'ASSIGNMENT', 'LAND', null);
	
	var assignmentID = req.params.id;
	
	var assignments = require('../app_modules/assignments');
	var events = require('../app_modules/events');
	var async = require('async');
	
	events.isAllowed(req.session.now, assignmentID, function(err, allowed) {
		if(allowed) {
			async.parallel([
                function(callback) {
                	assignments.getLastSavedCode(req.session.user.id, assignmentID, callback);
                },
                function(callback) {
                	assignments.getAssignmentProblem(assignmentID, callback);
                },
                function(callback) {
                	assignments.getAssignmentDetails(assignmentID, callback);
                }
            ], function(err, dataSet) {
        		if(err) {
        			console.log(err);
        			res.send('An error occurred! Could not continue. Please contact the system administrator.');
        			return;
        		}
        		if(dataSet[0] === null || dataSet[1] === null || dataSet[2] === null) {
        			res.redirect('/');
        			return;
        		}
        		res.render('editor/main', {
        			title: 'Prutor', 
        			username: req.session.user.name,
        			role: req.session.role,
        			assignmentID: req.params.id,
        			enableScratch: req.session.scratchpad,
        			now: req.session.now,
        			lastSave: dataSet[0],
        			problem: dataSet[1],
        			assignment: dataSet[2]
        		});
        	});
		} else res.redirect('/');
	});
});

router.get('/practice/:id', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'PRACTICE', 'PROBLEM', null);
	var async = require('async');
	var problems = require('../app_modules/problems')(req.dbPool,     req.cacheData, req.dbquery);
	var assignments = require('../app_modules/assignments');
	async.parallel([
     	function(callback) {
     		problems.getProblemStatement(req.params.id, callback);
     	},
     	function(callback) {
     		assignments.getPracticeAssignment(req.session.user.id, req.params.id, callback);
     	}
     ], function(err, result) {
		if(err) {
			console.log(err);
			res.redirect('/practice');
		} else if(result[0] === null) {
			res.redirect('/practice');
		} else {
			if(process.env.AUDIT) require('../app/audit')(req.dbPool,     req.cacheData, req.dbquery).logActivity(req.session.user.id, 'PRACTICE', 'PROBLEM', req.params.id);
			res.render('editor/main', {
				title: 'Prutor', 
				username: req.session.user.name,
				role: req.session.role,
				enableScratch: req.session.scratchpad,
				notifications: req.session.notifications,
				problem: result[0],
				practice: true,
				assignmentID: result[1].assignmentID,
				lastSave: result[1].lastSave
			});
		}
	});
});

router.post('/save', function(req, res) {
	
	var assignments = require('../app_modules/assignments');
	var events = require('../app_modules/events');
	
	var assignmentID = req.body.assignment_id;
	var source = req.body.code;
	var trigger = req.body.trigger;
	
	events.isAllowed(req.session.now, assignmentID, function(err, allowed) {
		if(allowed) {
			assignments.saveCode(req.session.user.id, assignmentID, source, trigger, function(err, insertID) {
				if(err) res.json(null);
				else res.json(insertID);
			});
		} else res.redirect('/');
	});
});

router.post('/create', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'SCRATCHPAD', 'CREATE_FILE', null);
	var scratchpad = require('../app_modules/scratchpad')(req.dbPool,     req.cacheData, req.dbquery);
	scratchpad.createFile(req.session.user.id, req.body.path, req.body.dir, function(err, result) {
		if(err) res.json(false);
		else {
			if(process.env.AUDIT) require('../app/audit')(req.dbPool,     req.cacheData, req.dbquery).logActivity(req.session.user.id, 'SCRATCHPAD', 'NEWFILE', result);
			res.json(result);
		}
	});
});

router.post('/delete', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'SCRATCHPAD', 'DELETE_FILE', null);
	var scratchpad = require('../app_modules/scratchpad')(req.dbPool,     req.cacheData, req.dbquery);
	scratchpad.deleteFile(req.session.user.id, req.body.id, function(err, result) {
		if(err) res.json(false);
		else {
			if(process.env.AUDIT) require('../app/audit')(req.dbPool,     req.cacheData, req.dbquery).logActivity(req.session.user.id, 'SCRATCHPAD', 'DELETEFILE', req.body.id);
			res.json(result);
		}
	});
});

router.post('/file', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'SCRATCHPAD', 'LOAD_FILE', null);
	var scratchpad = require('../app_modules/scratchpad')(req.dbPool,     req.cacheData, req.dbquery);
	scratchpad.getFile(req.session.user.id, req.body.id, function(err, file) {
		if(err) res.json(false);
		else {
			if(process.env.AUDIT) require('../app/audit')(req.dbPool,     req.cacheData, req.dbquery).logActivity(req.session.user.id, 'SCRATCHPAD', 'OPENFILE', req.body.id);
			res.json(file);
		}
	});
});

router.post('/savefile', function(req, res) {
	
	var scratchpad = require('../app_modules/scratchpad')(req.dbPool,     req.cacheData, req.dbquery);
	scratchpad.saveFile(req.body.id, req.body.contents, function(err, result) {
		res.json(result);
	});
});

router.get('/download/:id', function(req, res) {
	var assignments = require('../app_modules/assignments');
	assignments.getLastSavedCode(req.session.user.id, req.params.id, function(err, data) {
		var filename = req.params.id + ".txt";
		res.setHeader('Content-disposition', 'attachment; filename=' + filename);
		res.setHeader('Content-type', 'text/plain');
		res.charset = 'UTF-8';
		res.write(new Buffer(data.contents, 'base64').toString());
		res.end();
	});
});

router.get('/download/scratch/:id', function(req, res) {
	var scratchpad = require('../app_modules/scratchpad')(req.dbPool, req.cacheData, req.dbquery);
	scratchpad.getFile(req.session.user.id, req.params.id, function(err, file) {
		var filename = req.query.name;
		res.setHeader('Content-disposition', 'attachment; filename=' + filename);
		res.setHeader('Content-type', 'text/plain');
		res.charset = 'UTF-8';
		res.write(new Buffer(file.contents, 'base64').toString());
		res.end();
	});
});

router.post('/upload', function(req, res) {
	var scratchpad = require('../app_modules/scratchpad')(req.dbPool, req.cacheData, req.dbquery);
	scratchpad.createFile(req.session.user.id, req.body.path, req.body.dir, function(err, result) {
		if(err) {
			res.json(false);
		} else {
			scratchpad.saveFile(result, req.body.contents, function(err, data) {
				res.json(result);
			});
		}
	});
});

/*router.get('/downgrade', function(req, res) {
	if(typeof req.session.user === 'undefined' || req.session.now == false) {
		res.redirect('/');
		return;
	}
	var stopTime = new Date(req.session.now.time_stop);
	var rightNow = new Date();
	if(rightNow > stopTime) {
		res.redirect('/');
		return;
	}
	var problems = require('../app/problems')(req.dbPool,     req.cacheData, req.dbquery);
	problems.getDowngrade(req.query.assignment_id, function(problem) {
		var audit = require('../app/audit')(req.dbPool,     req.cacheData, req.dbquery);
		if(problem) audit.logActivity(req.session.user.id, 'CODE', 'DOWNGRADE', problem.id, function(id) {});
		res.json(problem);
	});
});

router.get('/upgrade', function(req, res) {
	if(typeof req.session.user === 'undefined' || req.session.now == false) {
		res.redirect('/');
		return;
	}
	var stopTime = new Date(req.session.now.time_stop);
	var rightNow = new Date();
	if(rightNow > stopTime) {
		res.redirect('/');
		return;
	}
	var problems = require('../app/problems')(req.dbPool,     req.cacheData, req.dbquery);
	problems.getUpgrade(req.query.assignment_id, function(problem) {
		var audit = require('../app/audit')(req.dbPool,     req.cacheData, req.dbquery);
		if(problem) audit.logActivity(req.session.user.id, 'CODE', 'UPGRADE', problem.id, function(id) {});
		res.json(problem);
	});
});*/

module.exports = router;