/**
 * Top level router.
 * @module 		routes/index
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

// Default View
router.get('/', function(req, res) {
	
	if(typeof req.session.user === 'undefined') {
		// Render student login page
		res.render('index', { title: 'Prutor' });
	} else if(req.session.role === 'regular') {
		// Redirect to student home page
		res.redirect('/home');
	} else {
		// Redirect to admin home
		res.redirect('/admin');
	}
});

router.get('/home', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'HOME', 'LAND', null);
	// Render student home page
	var events = require('../app_modules/events');
	var pager = require('../app_modules/pager')(req.dbPool, req.cacheData, req.dbquery);
	var statistics = require('../app_modules/statistics')(req.dbPool, req.cacheData, req.dbquery);
	var grading = require('../app_modules/grading');
	var async = require('async');
	async.parallel([
        function(callback) {
        	events.getOngoingEvent(req.session.user.id, callback);
        },
        function(callback) {
        	statistics.getCourseStatistics(req.session.user.id, callback);
        },
        function(callback) {
        	grading.getScoreCard(req.session.user.id, callback);
        },
        function(callback) {
        	pager.getMessageNotifications(req.session.user.id, callback);
        }
    ], function(err, dataSet) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Could not continue. Please contact the system administrator.');
			return;
		}
		var ongoing = (dataSet[0] === null) ? false : true;
		var enableScratch = true;
		if(ongoing) {
			if(dataSet[0].event.type === 'EXAM' || dataSet[0].event.type === 'QUIZ') enableScratch = false;
			req.session.now = dataSet[0].event;
		}
		req.session.scratchpad = enableScratch;
		req.session.ongoing = ongoing;
		req.session.notifications = dataSet[3];
		res.render('home/main', {
			title: 'Prutor', 
			username: req.session.user.name,
			role: req.session.role,
			notifications: req.session.notifications,
			ongoingEvent: dataSet[0],
			courseStatistics: dataSet[1],
			scoreCard: dataSet[2],
			enableScratch: req.session.scratchpad
		});
	});
});

router.get('/practice', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'PRACTICE', 'LAND', null);
	var problems = require('../app_modules/problems')(req.dbPool, req.cacheData, req.dbquery);
	problems.getPracticeProblems(function(err, problems) {
		if(process.env.AUDIT) require('../app/audit')(req.dbPool, req.cacheData, req.dbquery).logActivity(req.session.user.id, 'PRACTICE', 'ARENA', null);
		res.render('practice', {
			title: 'Prutor', 
			username: req.session.user.name,
			role: req.session.role,
			enableScratch: req.session.scratchpad,
			notifications: req.session.notifications,
			problems: problems
		});
	});
});

router.get('/code/:id', function(req, res) {
	req.dbPool.getConnection(function(err, con) {
		var query = "SELECT assignment_id FROM code WHERE id=?";
		con.query(query, [req.params.id], function(err, rows) {
			if(err || rows.length === 0) {
				res.redirect('/');
			} else {
				res.redirect('/admin/viewer/' + rows[0].assignment_id + '#' + req.params.id);
			}
		});
		con.release();
	});
});

router.post('/bug', function(req, res) {
	res.json(true);
	/*var userID = (req.session) ? req.session.user.id : null;
	require('../app/audit')(req.dbPool, req.cacheData, req.dbquery).logBug(userID, req.body.location, req.body.req_type, req.body.req_uri, req.body.req_params, req.body.response, function(result) {
		res.json(result);
	});*/
});

router.get('/ping', function(req, res) {
	res.send('pong');
});

router.get('/settings', function(req, res) {
	res.render('_admin/settings', {
		title: 'Prutor', 
		username: req.session.user.name,
		notifications: req.session.notifications
	});
});

router.get('/setnewpassword/:id', function(req, res) {
	var hash = req.params.id;
	req.dbquery("SELECT id FROM account WHERE hash=?", [hash], function(err, data) {
		if(err || data.length === 0) {
			res.redirect('/');
		} else {
			res.render('setpassword', {
				title: 'Prutor'
			});
		}
	});
});

module.exports = router;
