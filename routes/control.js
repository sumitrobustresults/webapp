/**
 * Control panel router.
 * @module 		routes/control
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../app_modules/engine';

router.get('/', function(req, res) {
	var async = require('async');
	var engine = require(modulePath);
	async.parallel([
        function(callback) {
        	engine.getProgrammingEnvironments(callback);
        },
        function(callback) {
        	engine.getControlConfig(callback);
        }
    ], function(err, data) {
		if(err) {
			res.send(error);
		} else {
			res.render('_admin/control', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				config: data[1],
				environments: data[0]
			});
		}
	});
});

router.post('/', function(req, res) {
	var engine = require(modulePath);
	if(req.body.type === 'quota') {
		engine.updateEngineQuotas(req.body.time, req.body.memory, function(err, result) {
			res.json(result);
		});
	} else if(req.body.type === 'flags') {
		engine.updateCompilerFlags(req.body.action, req.body.flag, function(err, result) {
			res.json(result);
		});
	} else if(req.body.type === 'tools') {
		engine.updateToolState(req.body.name, req.body.value, function(err, result) {
			res.json(result);
		});
	} else if(req.body.type === 'delays') { 
		engine.updateEngineDelays(req.body.compile, req.body.execute, req.body.evaluate, function(err, result) {
			res.json(result);
		});
	} else if(req.body.type === 'env') {
		engine.updateEnvironments(req.body.settings, function(err, data) {
			if(err)
				res.json(false);
			else
				res.json(true);
		});
	} else res.send('invalid query');
});

module.exports = router;