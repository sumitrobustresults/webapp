/**
 * Tasks router.
 * @module 		routes/tasks
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../app_modules/tasks';

router.get('/', function(req, res) {
	var async = require('async');
	var tasks = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	
	async.parallel([
            function(callback) {
            	tasks.getUserTasks(req.session.user.id, callback);
            },
            function(callback) {
            	tasks.getAssignedEvents(callback);
            }
        ], function(err, dataSet) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Could not continue.');
		} else {
			res.render('_admin/tasks', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				role: req.session.user.type,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				myTasks: dataSet[0],
				events: dataSet[1]
			});
		}
	});
});

router.get('/status', function(req, res) {
	var eventID = req.query.id;
	var tasks = require(modulePath)(req.dbPool, req.cacheData, req.dbquery);
	tasks.getTaskStatus(eventID, function(err, data) {
		res.json(data);
	});
});

module.exports = router;