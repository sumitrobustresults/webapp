/**
 * Codebook router.
 * @module 		routes/codebook
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'CODEBOOK', 'LAND', null);
	var assignments = require('../app_modules/assignments');
	assignments.getCodeBook(req.session.user.id, function(err, codeBook) {
		if(process.env.AUDIT) require('../app/audit')(req.dbPool,    req.cacheData, req.dbquery).logActivity(req.session.user.id, 'CODEBOOK', 'CONTENTS', null);
		res.render('codebook', {
			title: 'Prutor', 
			username: req.session.user.name,
			role: req.session.role,
			enableScratch: req.session.scratchpad,
			notifications: req.session.notifications,
			codeBook: codeBook
		});
	});
});

router.get('/page', function(req, res) {
	// TELEMETRY
	require('../app_modules/telemetry')(req.dbquery, req.nosql)
		.logUserActivity(req.session.user.id, 'CODEBOOK', 'LOAD_ASSIGNMENT', null);
	var assignments = require('../app_modules/assignments');
	assignments.getSubmission(req.query.id, function(err, page) {
		if(err) {
			console.log(err);
		} else {
			if(process.env.AUDIT) require('../app/audit')(req.dbPool,    req.cacheData, req.dbquery).logActivity(req.session.user.id, 'CODEBOOK', 'PAGE'.id);
			res.json(page);
		}
	});
});

router.post('/regrade', function(req, res) {
	var grading = require('../app_modules/grading');
	grading.requestRegrade(req.body.id, req.body.reason, function(err, response) {
		res.json(response);
	});
});

module.exports = router;