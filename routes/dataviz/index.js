/**
 * Dataviz main router.
 * @module 		routes/dataviz/index
 * @author 		Rajdeep Das
 * @copyright	(c) 2014 Rajdeep Das
 * @license		All rights reserved.
 * 
 * @requires module:express
 */

var express = require('express');
var router = express.Router();

var modulePath = '../../app_modules/analytics';

router.get('/errors', function(req, res) {
	var analytics = require(modulePath)(req.dbPool,     req.cacheData, req.dbquery);
	analytics.getCompilerMessages(req.query.id, function(err, data) {
		if(err) console.log(err);
		res.json(data);
	});
});

router.get('/events', function(req, res) {
	var events = require('../../app_modules/events');
	events.getEvents(function(events) {
		res.json(events);
	});
});

router.get('/submissions', function(req, res) {
	var assignments = require('../../app_modules/assignments');
	assignments.getSubmissionsForSchedule(req.query.schedule_id, req.query.offset, function(err, submissions) {
		res.json(submissions);
	});
});

router.post('/grade', function(req, res) {
	var grading = require('../../app_modules/grading');
	var tasks = require('../../app_modules/tasks')(req.dbPool,     req.cacheData, req.dbquery);
	grading.setGrade(req.body.assignment_id, req.body.marks, req.body.comments, req.session.user.id, function(result) {
		tasks.getNumPendingTasks(req.session.user.id, function(err, numTasks) {
			req.session.tasks = numTasks;
			res.json(result);
		});
	});
});

router.post('/code/delete', function(req, res) {
	var assignments = require('../../app_modules/assignments');
	assignments.deleteCodeSave(req.body.id, function(err, result) {
		res.json(result);
	});
});

router.get('/assignments', function(req, res) {
	var assignments = require('../../app_modules/assignments');
	assignments.getAssignments(function(assignments) {
		res.json(assignments);
	});
});

router.get('/analytics/:id', function(req, res) {
	var analytics = require(modulePath)(req.dbPool,     req.cacheData, req.dbquery);
	analytics.getAssignmentAnalytics(req.params.id, function(err, dataSet) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Could not continue.');
		} else if(dataSet[0].length === 0) {
			res.send('Could not find such an assignment to generate analytics for.');
		} else {
			res.render('_admin/analytics/assignment', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				history: dataSet[0],
				compilations: dataSet[1],
				executions: dataSet[2],
				evaluations: dataSet[3],
				assignmentID: req.params.id
			});
		}
	});
});

////////////////////////////////// MERGED ///////////////////////////////////

router.get('/dash/:id', function(req, res) {
	var statistics = require('../../app_modules/statistics')(req.dbPool,     req.cacheData, req.dbquery);
	statistics.getGradeStatistics(req.params.id, function(err, data) {
		if(err) {
			console.log(err);
			res.send('An error occurred! Could not continue.');
		} else if(data === null) {
			res.send('No data available for event.');
		} else {
			res.render('_admin/dataviz/progress', {
				title: 'Prutor Admin',
				username: req.session.user.name,
				notifications: req.session.notifications,
				tasks: req.session.tasks,
				performance: data
			});
		}
	});
});

router.get('/progress/stats', function(req, res) {
	var statistics = require('../../app_modules/statistics')(req.dbPool,     req.cacheData, req.dbquery);
	statistics.getScoreStats(req.query.id, function(err, stats) {
		if(err) {
			console.log(err);
		} else {
			res.json(stats);
		}
	});
});

router.get('/compiler_messages', function(req, res) {
	var analytics = require(modulePath)(req.dbPool,     req.cacheData, req.dbquery);
	analytics.getCompilerMessages(req.query.id, function(err, data) {
		if(err)
			console.log(err);
		res.json(data);
	});
});

module.exports = router;