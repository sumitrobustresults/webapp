var express = require('express');
var router = express.Router();

var modulePath = '../app_modules/notifications';

router.get('/', function(req, res) {
	var notifications = require(modulePath);
	notifications.getAllNotifications(req.session.user.id, function(err, data) {
		res.render('_admin/notifications', {
			title: 'Prutor', 
			username: req.session.user.name,
			type: req.session.user.type,
			notifications: req.session.notifications,
			tasks: req.session.tasks,
			notificationData: data
		});
	});
});

router.get('/read', function(req, res) {
	var notifications = require(modulePath);
	notifications.markSeenAll(req.session.user.id, function(err, success) {
		res.json(success);
	});
});

module.exports = router;