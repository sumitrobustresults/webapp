# README #

This is the web application for the Intelligent Tutoring System. You must install the necessary components for this application to run. Refer to https://rajd33pd@bitbucket.org/rajd33pd/its-deploy.git for the deployment scripts.

1. Add the updated repository for NodeJS.    
    ```sudo add-apt-repository ppa:chris-lea/node.js```

2. Install NodeJS.   
    ```sudo apt-get install nodejs```

3. Clone the minimal branch of this repository till depth 1.

4. Go to the cloned directory. Modify app.js to suit your database configuration.

5. Run ```npm start``` from a terminal.

6. Browse to http:///localhost:3000/
